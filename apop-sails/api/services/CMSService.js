const axios = require('axios');

// Reusable preconfigured axios instance
var api = axios.create({
    baseURL: sails.config.CMSApiEndpoint,
});

// Inject API Access Token into all requests
api.interceptors.request.use(config =>
    (config.headers['Authorization'] = ['Bearer', sails.config.CMSApiAccessToken].join(' '), config)
);

module.exports = {

    get: function (req, next) {

        let url = req.url || '';
        let parameters = req.parameters || {};

        api.get(url, {
          params: {
            ...parameters
          }
        })
            .then(function (response) {
                return next(null, response.data);
            })
            .catch(function (error) {
                if (error.response) {
                    // The request was made and the server responded with a status code
                    // that falls out of the range of 2xx
                    // sails.log.error(error.response.data);
                    // sails.log.error(error.response.status);
                    // sails.log.error(error.response.headers);
                    sails.log.error(`${error.response.status} ${error.response.data}`);
                } else if (error.request) {
                    // The request was made but no response was received
                    // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
                    // http.ClientRequest in node.js
                    sails.log.error(error.request);
                } else {
                    // Something happened in setting up the request that triggered an Error
                    sails.log.error('Error', error.message);
                }
                // sails.log.error(error.config);

                return next(error);
            });
    }

}