
module.exports = {

	saveMetric: function(req, next) {
		Metric.create({
			user: req.user,
			userclient: req.userclient,
			timestamp: req.timestamp,
			version: req.version,
			useragent: req.useragent,
			duration: req.duration,
			page: req.page,
			type: req.type,
			category: req.category,
			reference: req.reference,
			document: req.document,
			resource: req.resource,
			tag: req.tag,
			table: req.table,
			figure: req.figure,
			search: req.search,
			submessage: req.submessage
		}).exec(function(err, metric) {
			if(err) throw err;
			next(null, metric);
		});
	},

};
