
module.exports = {
	
	getFavorites: function(user, done) {

		Favorite.find({user, favorited: true}).exec(function(err, favorites) {

			if(err) {

				return done(err);

			} else {

				return done(null, favorites);

			}

		});

	},

	saveFavorite: function(criteria, done) {

		const { user, reference } = criteria;

		sails.log.info(`Favoriting Reference: ${reference}`);

		Favorite.findOrCreate({user, reference}, {user, reference, favorited: true}).exec(function(err, favorite) {

			if(err) {

				return done(err);

			} else if (favorite.favorited) {

				return done(err, favorite);

			} else {

				sails.log.info('Updating favorite...')

				Favorite.update(criteria, { favorited: true }, function(err, favorite){

					if(err) {
						return done(err);
					}

					sails.log.info('Favorite updated');

					return done(err, Array.isArray(favorite) ? favorite[0] : favorite);

				})

			}

		});
	},

	archiveFavorite: function(criteria, done) {
		Favorite.findOne(criteria).exec(function(err, favorite) {

			if(err) {

				return done(err);

			} else if(favorite) {

				sails.log.info('Updating favorite...')

				Favorite.update(criteria, { favorited: false }, function(err, favorite){

					if(err) {
						return done(err);
					}

					sails.log.info('Favorite updated');

					return done(err, Array.isArray(favorite) ? favorite[0] : favorite);

				});

			} else {

				sails.log.info('No favorite found');

				return done();

			}
		});
	}

};