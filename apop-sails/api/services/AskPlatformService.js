module.exports = {

	saveAskPlatformComment: function(req, next) {
		AskPlatformComment.create({user: req.user, comment: req.comment}).exec(function(err, comment) {
			if(err) throw err;
			next(comment);
		});
	},

	getSubmittedSurveys: function(req, next) {
		AskPlatformSurvey.find({user: req.session.user.username}).exec(function(err, surveys) {
			if(err) throw err;
			next(surveys);
		});
	},

	saveAskPlatformSurvey: function(req, next) {
		AskPlatformSurvey.create({user: req.user, survey_id: req.survey_id, survey_q1: req.survey_q1, survey_q2: req.survey_q2, survey_q3: req.survey_q3, survey_q4: req.survey_q4, survey_q5: req.survey_q5, survey_q6:  req.survey_q6, survey_q7: req.survey_q7, survey_q8: req.survey_q8, survey_q9: req.survey_q9 }).exec(function(err, survey) {
			if(err) throw err;
			next(survey);
		});
	}

};