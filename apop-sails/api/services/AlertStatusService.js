
module.exports = {
	
	getAlertStatus: function(req, next) {
		AlertStatus.find({user: req.session.user.username, read: true}).exec(function(err, status) {
			if(err) throw err;
			next(status);
		});
	},
	saveReadAlertStatus: function(req, next) {
		console.log(req);
		AlertStatus.findOne({user: req.user, alert: req.alert}).exec(function(err, status) {
			if(err) throw err;

			if(!status) {
				AlertStatus.create({user: req.user, alert: req.alert, read: true}).exec(function(err, response) {
					if(err) throw err;
					next(response);
				});
			} else {
				status.read = true;
				status.save();
				next(status);
			}
		});
	},
	saveAlertedAlertStatus: function(req, next) {
		console.log(req);
		AlertStatus.findOne({user: req.user, alert: req.alert}).exec(function(err, status) {
			if(err) throw err;

			if(!status) {
				AlertStatus.create({user: req.user, alert: req.alert, alerted: true}).exec(function(err, response) {
					if(err) throw err;
					next(response);
				});
			} else {
				status.alerted = true;
				status.save();
				next(status);
			}
		});
	}


};