
module.exports = {
	
	getAlerts: function(req, next) {
		Alert.find().populate('alertstatus', {user: req.session.user.username}).exec(function(err, alerts) {
			if(err) throw err;
			next(alerts);
		});
	}

};