const { extname } = require('path');
const axios = require('axios');

// Reusable preconfigured axios instance -- S3
const s3 = axios.create({
    method: 'get'
});

const MIMETypes = {
    '.pdf': 'application/pdf',
    '.pptx': 'application/vnd.openxmlformats-officedocument.presentationml.presentation',
    '.ppt': 'application/vnd.ms-powerpoint',
    '.docx': 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
    '.doc': 'application/msword',
    '.xlsx': 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
    '.xls': 'application/vnd.ms-excel',
    '.png': 'image/png',
    '.jpg': 'image/jpeg',
    '.jpeg': 'image/jpeg',
    '.gif': 'image/gif'
};

module.exports = {

    get: function (filename, url, next) {

        // [extension]#[uuid]
        // const [extension] = extname(filename).split('#');
        const extension = extname(filename);
        // sails.log('Extension', extension);

        const mimeType = MIMETypes[extension];
        // sails.log('MIME Type', mimeType);

        sails.log.info(`Requesting object data for: ${filename} ...`);

        const config = {             
            headers: {
            'Content-Type': mimeType
            },
            responseType: 'arraybuffer'
        }

        s3.get(url, config)
            .then(function (response) {
                return next(null, response.data);
            })
            .catch(function (error) {
                if (error.response) {
                    // The request was made and the server responded with a status code
                    // that falls out of the range of 2xx
                    sails.log.error(error.response.data);
                    sails.log.error(error.response.status);
                    // sails.log.error(error.response.headers);
                } else if (error.request) {
                    // The request was made but no response was received
                    // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
                    // http.ClientRequest in node.js
                    // sails.log.error(error.request);
                } else {
                    // Something happened in setting up the request that triggered an Error
                    sails.log.error('Error', error.message);
                }
                // sails.log.error(error.config);

                return next(error);
            });

    }

}