var nodemailer = require('nodemailer');

var options	= sails.config.mailerOptions;

var transporter = nodemailer.createTransport(options);

module.exports 	= {

	sendMail: function(options) {

		var mailOptions = {
			from: options.from,
			sender: options.from,
			to: options.to,
			subject: options.subject,
			text: options.text,
			html: options.html
		}

		transporter.sendMail(mailOptions, function(error){
			if(error)
				return
		})


	}


};