
module.exports = {

	getUser: function(req, next) {
    try {
      var user = (((req || {}).session || {}).user || {}).nameID;

      if (!user) {
        throw new Error("No valid session or user was found.");
      }

      User.findOne({username: req.session.user.nameID})
          .populate('favorites')
          .exec(function(err, user) {
            if(err) throw err;
            next(null, user);
      });
    } catch (err) {
      next(err)
    }
	},

	saveUser: function(req, next) {
    User.findOne({username: req.nameID}).exec(function(err, user) {
      if(err) {
        next(err);
      } else {
        if(user){
          user.email = req.email;
          user.first_name = req.firstName;
          user.last_name = req.lastName;
          user.full_name = req.fullName;
          user.groups = req.groups;
          user.save();
          next(null, user);
        }

        if(!user){
          User.create({
            username: req.nameID,
            email: req.email,
            first_name: req.firstName,
            last_name: req.lastName,
            full_name: req.fullName,
            groups: req.groups
          }).exec(function(err, user) {
            if(err){
              next(err);
            } else {
              next(null, user);
            }
          });
        }
      }
    });
	}

};
