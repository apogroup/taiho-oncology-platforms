const axios = require('axios');
const { stringify } = require('querystring');
const { setFlagsFromString } = require('v8');

const { AWSS3Bucket, AWSApiObjectPath } = sails.config;

// Reusable preconfigured axios instance -- API Gateway
const gateway = axios.create({
    baseURL: sails.config.AWSApiRoot,
});

// Inject AWS Access Token into all requests
gateway.interceptors.request.use(config =>
    (config.headers['x-api-key'] = sails.config.AWSApiAccessToken, config)
);

module.exports = {

    createPresignedUrl: async function(filePath, next){

        sails.log.info(`Creating presigned url for: ${filePath} implementing GET...`);

        const key = decodeURIComponent(`${AWSApiObjectPath}${filePath}`) 

        const query = {
            bucket: AWSS3Bucket,
            key,
            operation: 'GET'
        }
        
        const queryString = `?${stringify(query)}`; // <baseURL><queryString>

        //---------------------------------------
        // [AWSApiRoot]?[query]
        //---------------------------------------

        gateway.get(queryString)
                .then(function (response) {
                    return next(null, response.data.body.signed_url);
                })
                .catch(function (error) {
                    if (error.response) {
                        // The request was made and the server responded with a status code
                        // that falls out of the range of 2xx
                        sails.log.error(error.response.data);
                        sails.log.error(error.response.status);
                        // sails.log.error(error.response.headers);
                    } else if (error.request) {
                        // The request was made but no response was received
                        // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
                        // http.ClientRequest in node.js
                        sails.log.error(error.request);
                    } else {
                        // Something happened in setting up the request that triggered an Error
                        sails.log.error('Error', error.message);
                    }
                    sails.log.error(error.config);

                    return next(error);
                });    

    }

}