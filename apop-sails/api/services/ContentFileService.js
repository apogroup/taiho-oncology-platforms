const { createReadStream } = require('fs');
const { resolve } = require('path');

module.exports = {

    local: function (file, next) {

        let stream;
        try {
            let path = resolve(sails.config.appPath, `.${file}`);
            stream = createReadStream(path);
        } catch (error) {
            return next(error);
        }

        return next(null, stream);
    },


    s3: function (key, next) {
        
        GatewayService.createPresignedUrl(key, function(error, signed_url) {

            if (error) {
                sails.log.error(`Failed to create presigned url`);
                return next(error);
            }

            S3Service.get(key, signed_url, function (error, buffer) {
    
                if (error) {
                    sails.log.error(`Failed to get ${key} from S3`);
                    sails.log.error(error);
                    return next(error);
                }
    
                return next(null, buffer);
    
            });

        });

    }

}