/**
 * AskPlatformSurvey.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

	attributes: {
		id: {
			type: 'integer',
			primaryKey: true,
			unique: true
		},
		user: {
			model: 'user',
		},
		survey_id: {
			type: 'text'
		},
		survey_q1: {
			type: 'text'
		},
		survey_q2: {
			type: 'text'
		},
		survey_q3: {
			type: 'text'
		},
		survey_q4: {
			type: 'text'
		},
		survey_q5: {
			type: 'text'
		},
		survey_q6: {
			type: 'text'
		},
		survey_q7: {
			type: 'text'
		},
		survey_q8: {
			type: 'text'
		},
		survey_q9: {
			type: 'text'
		}

	}

};
