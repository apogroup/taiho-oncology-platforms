/**
 * Metric.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

	attributes: {
		id: {
			type: 'integer',
			primaryKey: true,
			unique: true
		},
		user: {
			model: 'user'
		},
		userclient: {
			type: 'text'
		},
		timestamp: {
			type: 'datetime'
		},
		version: {
			type: 'text'
		},
		useragent: {
			type: 'text'
		},
		duration: {
			type: 'text'
		},
		page: {
			type: 'text'
		},
		type: {
			type: 'text'
		},
		category: {
			type: 'text'
		},
		reference: {
			type: 'text'
		},
		document: {
			type: 'text'
		},
		resource: {
			type: 'text'
		},
		tag: {
			type: 'text'
		},
		table: {
			type: 'text'
		},
		figure: {
			type: 'text'
		},
		search: {
			type: 'text'
		},
		submessage: {
			type: 'text'
		}
	}

};
