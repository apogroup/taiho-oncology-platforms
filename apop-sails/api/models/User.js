/**
 * User.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models


 NOTE: UNREGISTERED TYPE EMAIL ISSUE ON STARTUP WHEN TYPE IS SET TO EMAIL
 */


module.exports = {

    attributes: {
        username: {
            type: 'string',
            primaryKey: true,
            required: true,
            unique: true
        },
        email: {
            type: 'string'
        },
        first_name: {
            type: 'string'
        },
        last_name: {
            type: 'string'
        },
        full_name: {
            type: 'string'
        },
        groups: {
            type: 'array'
        },

        favorites: {
            collection: 'favorite',
            via: 'user',
            dominant: true
        },

        toJSON: function() {
            var obj = this.toObject();
            return obj;
        }

    }
    
};

