module.exports = function(req, res, next) {

	if (req.session.authenticated) {
		return next();
	}

	req.session.destroy(function (err) {
        return res.redirect('/login-sso');
    });

	// return res.redirect('/logout');

};