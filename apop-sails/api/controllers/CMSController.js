/**
 * CMSController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {

  fetch: function (req, res) {

    const { path, query } = req;

    /////////////////////////////////////////////////////////////////////////
    //  Transform request path into CMS request URL
    /////////////////////////////////////////////////////////////////////////
    //  /cms/[model]/[id]
    //  /[model]/[id]
    /////////////////////////////////////////////////////////////////////////
    let url;
    try {
      url = path.split('/').filter(part => part !== 'cms').join('/');
    } catch (error) {
      sails.log.error(`Could not parse request path: ${error}`);
      return res.badRequest();
    }

    let parameters = query || {};

    CMSService.get({url, parameters}, function (error, response) {
      if (error) {
        sails.log.error(error);
        return res.badRequest();
      }

      return res.json(response);
    });

  }

};

