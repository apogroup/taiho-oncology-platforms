/**
 * ContentFileController
 *
 * @description :: Server-side logic for serving static content
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

const fs = require('fs');
const { parse, resolve } = require('path');
const { PassThrough } = require('stream');

module.exports = {

    serve: function (req, res) {

        var file = req.path;
        file = file.substring(1);

        // sails.log('Serving: ', file);
        var filePath = resolve(sails.config.appPath, file);

        fs.createReadStream(filePath).pipe(res);

    },

    s3: function (req, res) {

        var key = req.path;
        key = key.split('/');

        key = key[2] + '/' + key[3];

        ContentFileService.s3(key, function (error, buffer) {

            if (error) {
                sails.log.error(`Could not fetch content from S3: ${key}`);
                sails.log(error);
                return res.notFound();
            }

            sails.log.info('Streaming from S3...');

            /////////////////////////////////////////////////////////////////////////
            // Rename file if downloading
            /////////////////////////////////////////////////////////////////////////
            const { query } = req;
            if (query.download) {

                /////////////////////////////////////////////////////////////////////////
                // Parse path and grab filename
                /////////////////////////////////////////////////////////////////////////
                //
                //
                //  ┌─────────────────────┬────────────┐
                //  │          dir        │    base    │
                //  ├──────┬              ├──────┬─────┤
                //  │ root │              │ name │ ext │
                //  "  /    home/user/dir / file  .txt "
                //  └──────┴──────────────┴──────┴─────┘
                //
                //  base: [filename]#[uuid]
                //
                /////////////////////////////////////////////////////////////////////////
                const [filename] = parse(key).base.split('#');
                res.set('Content-Disposition', `attachment; filename="${filename}"`);

                sails.log.info(`Downloading file...`);
            }

            const stream = new PassThrough();

            stream.end(buffer);

            stream.on('error', (error) => {
                sails.log.error(`Streaming failed: ${error}`);
                res.serverError();
                res.end();
            })

            stream.on('finish', () => {
                sails.log.info(`File served successfully: ${key}`);
            })

            stream.pipe(res);


            // if (response.status == 200) {

            //     // sails.log('Fetching from s3: ', req.path)
            //     var bufferStream = new stream.PassThrough();
            //     var resourceBuffer = Buffer.from(response.data, 'base64');

            //     bufferStream.end(resourceBuffer);

            //     bufferStream.pipe(res);

            // } else {
            //     module.exports.serve(req, res);
            // }

        })

    }





    // serve: function (req, res) {

    //     const { path } = req;

    //     sails.log.info(`Content Requested: ${path}`);

    //     /////////////////////////////////////////////////////////////////////////
    //     //  Transform request path into CMS request URL
    //     /////////////////////////////////////////////////////////////////////////
    //     //  /content/[model]/[id]
    //     //  /[model]/[id]
    //     /////////////////////////////////////////////////////////////////////////
    //     let s3Object;
    //     try {
    //         /////////////////////////////////////////////////////////////////////////
    //         // Parse path and grab S3Object ID
    //         /////////////////////////////////////////////////////////////////////////
    //         //
    //         //
    //         //  ┌─────────────────────┬────────────┐
    //         //  │          dir        │    base    │
    //         //  ├──────┬              ├──────┬─────┤
    //         //  │ root │              │ name │ ext │
    //         //  "  /    home/user/dir / file  .txt "
    //         //  └──────┴──────────────┴──────┴─────┘
    //         //
    //         //  base: 
    //         //
    //         /////////////////////////////////////////////////////////////////////////
    //         // url = path.split('/').filter(part => part !== 'content').join('/');
    //         var id = parse(path).base;
    //         s3Object = `/s3object/${id}`
    //     } catch (error) {
    //         sails.log.error(`Could not parse request path: ${error}`);
    //         return res.badRequest();
    //     }

    //     CMSService.get({url: s3Object}, function (error, s3Object) {

    //         if (error) {

    //             sails.log.info(`Attempting to serve local file: ${path}`);

    //             /////////////////////////////////////////////////////////////////////////
    //             //  Try serving file locally
    //             /////////////////////////////////////////////////////////////////////////

    //             ContentFileService.local(path, function (error, stream) {

    //                 if (error) {
    //                     sails.log.error(`Could not serve local file: ${path}`);
    //                     sails.log.error(error);
    //                     return res.notFound();
    //                 }

    //                 stream.on('error', (error) => {
    //                     sails.log('Local file stream error');
    //                     sails.log.error(error);
    //                     res.notFound();
    //                     res.end();
    //                 });

    //                 stream.on('end', () => {
    //                     sails.log.info(`File served successfully: ${path}`);
    //                 });

    //                 stream.pipe(res);

    //             });

    //         } else {

    //             let { key } = s3Object;
    
    //             sails.log.info(`Attempting to fetch content from S3...`);
    
    //             ContentFileService.s3(key, (error, buffer) => {
    
    //                 if (error) {
    //                     sails.log.error(`Could not fetch content from S3: ${key}`);
    //                     sails.log(error);
    //                     return res.notFound();
    //                 }
    
    //                 sails.log.info('Streaming from S3...');
    
    //                 /////////////////////////////////////////////////////////////////////////
    //                 // Rename file if downloading
    //                 /////////////////////////////////////////////////////////////////////////
    //                 const { query } = req;
    //                 if (query.download) {
    
    //                     /////////////////////////////////////////////////////////////////////////
    //                     // Parse path and grab filename
    //                     /////////////////////////////////////////////////////////////////////////
    //                     //
    //                     //
    //                     //  ┌─────────────────────┬────────────┐
    //                     //  │          dir        │    base    │
    //                     //  ├──────┬              ├──────┬─────┤
    //                     //  │ root │              │ name │ ext │
    //                     //  "  /    home/user/dir / file  .txt "
    //                     //  └──────┴──────────────┴──────┴─────┘
    //                     //
    //                     //  base: [filename]#[uuid]
    //                     //
    //                     /////////////////////////////////////////////////////////////////////////
    //                     const [filename] = parse(key).base.split('#');
    //                     res.set('Content-Disposition', `attachment; filename="${filename}"`);
    
    //                     sails.log.info(`Downloading file...`);
    //                 }
    
    //                 const stream = new PassThrough();
    
    //                 stream.end(buffer);
    
    //                 stream.on('error', (error) => {
    //                     sails.log.error(`Streaming failed: ${error}`);
    //                     res.serverError();
    //                     res.end();
    //                 })
    
    //                 stream.on('finish', () => {
    //                     sails.log.info(`File served successfully: ${key}`);
    //                 })
    
    //                 stream.pipe(res);
    
    //             });

    //         }

    //     });

    // }

};