/**
 * FavoriteController
 *
 * @description :: Server-side logic for managing favorites
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

    saveMetric: function(req, res) {
        try {
          var user = (((req || {}).session || {}).user || {}).nameID;

          if (!user) {
            throw new Error("No valid session or user was found.");
          }
        } catch (err) {
          return res.serverError(err);
        }

        try {
          var metric = (req.body) ? req.body : undefined;

          if(!metric) {
            throw new Error("Not a valid metric request.");
          }

          metric.user = user;

          MetricService.saveMetric(metric, function (err, success) {
            if (err) throw err;
            res.json(success);
          });
        } catch (err) {
          return res.serverError(err);
        }
    }

};
