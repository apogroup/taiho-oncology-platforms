/**
 * BootstrapController
 *
 * @description :: Server-side logic for managing bootstraps
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

	index: function(req, res) {

		res.view('bootstrap', {view: 'dashboard'});
		
	},

	login_sso: function(req, res) {

		req.session.destroy(function (err) { });
		
		res.view('auth/login-sso', {view: 'login-sso'});
		
	},

	"pdf-viewer": function(req, res) {

		res.view('pages/pdfjs-viewer');
		
	}	
	
	
};

