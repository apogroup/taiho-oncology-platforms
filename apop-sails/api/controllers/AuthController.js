/**
 * AuthController
 *
 * @description :: Server-side logic for managing auths
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

var passport = require('passport');

module.exports = {

    _config: {
        actions: false,
        shortcuts: false,
        rest: false
    },

    //--------------------------------
    // SAML OKTA Strategy API
    //--------------------------------

    auth: function(req, res, next){

        const config = req.params.config;

        passport.authenticate(config, function(err, user){
        
            if(err){
                sails.log("here is the error in auth: ", err);
                return res.redirect('/login-sso');
            }
            if(user){
                
                sails.log('User, ', user);

                // Store use in database

                UserService.saveUser(user, function(error, success) {
                    if(error){
                        sails.log(error);
                        return res.redirect('/login-sso');
                    } 
                    
                    sails.log(success);
                    // Store user in session

                    req.session.user = user;
                    req.session.authenticated = true;                        
                    return res.redirect('/');                    
                    // res.json(success);
                });

            }
        
        })(req, res, next);

    },

    login: function(req, res, next){

        const config = req.params.config;

        passport.authenticate(config, function(err, user){
            sails.log.info(user);
            if(err){
                sails.log("here is the error in login: ", err);
            }

            if(user){
                sails.log(user);
            }
            
        })(req, res, next);

    },

    

    //--------------------------------
    // Local OKTA Strategy API
    //--------------------------------

    // auth: function(req, res) {

    //     passport.authenticate('okta', function(err, user){

    //         if(err || (!user)) {
    //             req.addFlash('error', 'Service unavailable. Please try again later.');
    //             return res.redirect('/login'); 
    //         }

    //         // E0000004 Authentication Failed
    //         if(user.errorCode == 'E0000004') {
    //             req.addFlash('error', 'The username or password you provided is not correct. Please try again.'); 
    //             return res.redirect('/login'); 
    //         }

    //         // Successfull login
    //         if(user.status == "SUCCESS") {
    //             // return res.json(user);
    //             // store user in database
    //             // set req.session.user
    //             req.session.authenticated = true;
    //             return res.redirect('/');
    //         }

    //         // All other OKTA errors 
    //         else {
    //             req.addFlash('error', 'An error occured validating your account. Please try again later.');
    //             return res.redirect('/login');
    //         }

    //     })(req, res);
        
    // },

    // login: function(req, res) {

    //     res.view('auth/login');

    // },





    logout: function(req, res) {

        req.session.destroy(function (err) {
            res.redirect('/login-sso');
        });
        
    }
};