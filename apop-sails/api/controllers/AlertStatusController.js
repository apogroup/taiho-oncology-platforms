/**
 * AlertStatusController
 *
 * @description :: Server-side logic for managing alerts
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

	getAlertStatus: function(req, res) {
        AlertStatusService.getAlertStatus(req, function(alerts) {
            res.json(alerts);
        });
    },
    saveReadAlertStatus: function(req, res) {
        var status = (req.body) ? req.body : undefined;
        status.user = req.session.user.username;
        AlertStatusService.saveReadAlertStatus(status, function(success) {
            res.json(success);
        });
    },
    saveAlertedAlertStatus: function(req, res) {
        var status = (req.body) ? req.body : undefined;
        status.user = req.session.user.username;
        AlertStatusService.saveAlertedAlertStatus(status, function(success) {
            res.json(success);
        });
    },

};

