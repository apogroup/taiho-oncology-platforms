/**
 * AskPlatformController
 *
 * @description :: Server-side logic for managing references
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

    saveAskPlatformComment: function(req, res) {
        var comment = (req.body) ? req.body : undefined;
        comment.user = req.session.user.username;
        AskPlatformService.saveAskPlatformComment(comment, function(success) {
            res.json(success);
        });

        var mailOptions = {
            from: sails.config.messageOptions.from,
            to: sails.config.messageOptions.to,
            subject: sails.config.messageOptions.subject, 
            text: sails.config.messageOptions.text,
            html: sails.config.messageOptions.html + "<p>From: "+comment.user+" | "+comment.comment+"</p>"
        }

        sails.log.info(mailOptions);
        MailService.sendMail(mailOptions);
    },

    getSubmittedSurveys: function(req, res) {
        AskPlatformService.getSubmittedSurveys(req, function(surveys) {
            res.json(surveys);
        });
    },

    saveAskPlatformSurvey: function(req, res) {
        var survey = (req.body) ? req.body : undefined;
        survey.user = req.session.user.username;
        AskPlatformService.saveAskPlatformSurvey(survey, function(success) {
            res.json(success);
        });
    }

};