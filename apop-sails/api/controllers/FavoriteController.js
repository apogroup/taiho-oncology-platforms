/**
 * FavoriteController
 *
 * @description :: Server-side logic for managing favorites
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

    getFavorites: function (req, res) {
        const { session } = req;

        try {

            const { email: user } = session.user;

            sails.log.info(`Fetching user favorites: ${user}`);

            FavoriteService.getFavorites(user, function (err, favorites) {

                if (err) {

                    sails.log.error(err);

                    return res.badRequest();
                }

                return res.json(favorites);

            });

        } catch (error) {

            sails.log.error(error);

            return res.serverError();

        }

    },

    saveFavorite: function (req, res) {

        const { body, session } = req;

        try {

            const { reference } = body;
            const { user } = session;

            FavoriteService.saveFavorite({
                user: user.username,
                reference
            }, function (err, favorite) {

                if (err) {

                    sails.log.error(err);

                    return res.badRequest();

                }

                sails.log.info('Reference favorited successfully');

                return res.json(favorite);

            });

        } catch (error) {

            sails.log.error(error);

            return res.badRequest();

        }
    },

    archiveFavorite: function (req, res) {

        const { body, session } = req;

        try {

            const { reference } = body;
            const { user } = session;

            FavoriteService.archiveFavorite({
                user: user.username,
                reference
            }, function (err, favorite) {

                if (err) {

                    sails.log.error(err);

                    return res.badRequest();

                }

                sails.log.info('Reference unfavorited successfully');

                return res.json(favorite);

            });

        } catch (error) {

            sails.log.error(error);

            return res.badRequest();

        }
    }

};

