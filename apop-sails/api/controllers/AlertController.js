/**
 * AlertController
 *
 * @description :: Server-side logic for managing alerts
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

	getAlerts: function(req, res) {
        AlertService.getAlerts(req, function(alerts) {
            res.json(alerts);
        });
    }

};

