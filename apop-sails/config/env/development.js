/**
 * Development environment settings
 *
 * This file can include shared settings for a development team,
 * such as API keys or remote database passwords.  If you're using
 * a version control solution for your Sails app, this file will
 * be committed to your repository unless you add it to your .gitignore
 * file.  If your repository will be publicly viewable, don't add
 * any private information to this file!
 *
 */

module.exports = {
  /***************************************************************************
   * Set the default database connection for models in the development       *
   * environment (see config/connections.js and config/models.js )           *
   ***************************************************************************/

  // models: {
  // 	connection: ''
  // },

  /***************************************************************************
   * Set the local environment
   ***************************************************************************/

  local: {
    port: 1337,
    environment: 'development',
  },

  /***************************************************************************
   * Connections
   ***************************************************************************/

  connections: {
    MySQLServer: {
      adapter: 'sails-mysql',
      host: 'localhost',
      user: 'root',
      password: 'Apothecom2016',
      database: 'apo_platform_taiho',
    },
  },

  /***************************************************************************
   * Mail Service
   ***************************************************************************/

  mailerOptions: {
    service: 'Mailgun',
    auth: {
      user: 'postmaster@sandbox2c5124cbba5d441d8cf3e76c970c563a.mailgun.org',
      pass: 'e13f16471da17d98b3639c8863ede87a-f7d0b107-435f8cee'
    }
  },

  messageOptions: {
    from: "taiho.scientific.platform@gmail.com",
    to: "apotaiho@apothecom.com",
    subject: "New Comment | Taiho DEC-C Scientific Platform",
    text: "",
    html: "<p>You have received a new comment on Taiho DEC-C Scientific Platform</p>"
  },
  
  /***************************************************************************
	 * AWS API Gateway
	 ***************************************************************************/

    AWSS3Bucket: 'taiho-cms-development',
    AWSApiRoot: 'https://b25r52aaei.execute-api.us-east-1.amazonaws.com/beta',
    AWSApiObjectPath: 'private/taiho-platform-files/',
    AWSApiAccessToken: 'vKRwra7gXg7NZWUPKandBqG1NJVvMfO8hbGxJXO7',
  
      /***************************************************************************
      * CMS API Config
      ***************************************************************************/
  
      CMSApiEndpoint: 'http://cms.ixazomibmmmaintenancesciplatformstaging.com:8080/',
      CMSApiAccessToken: 'eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE1NzAxOTg4NTR9.HNdlKjVMjxNLc0I3r5uA_8ztlR_LQ1qWGZkENV7OELzcCyj4oJoXxdErTZaM8fwnNzSMoxmR75d7CdFUyKL-6foBILjm7gjmQ1CjxjatcmTmgRsvKPuar9y9JWNscVoy9ljb9VE-eqW5akMGvCoAQ3PwrgI74KYA3JxRbpZ4qp2dtiQj63_uQ0R4aivj6DMerA1SlnAkFC1BtPKAkzKnkfnx3co9Yw_I3IFxff28oiALlQGyyxwpGPVURZ1fc4tnxRn9-7gaBfhrkJHZ6MDV-DTUyu71w21RbxCC_P2rcQUFLFhSqYW8IKiaJCiVB9hEWva_EY2cVxb2VFFfzw4WNQ',
  
    // CMSApiEndpoint: 'http://localhost:8080/',
    // CMSApiAccessToken: 'eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE1NzQxMDU2OTh9.tAa-KxdC0kI0Re-rHwI0lj0xsAekN3bj0Ydsx3ih3ICJBYbbw1Ok0F-T4eAhYRYhGkmGOWvbalih35RkJpAdi0OFApkujvFPjeR3tjV0cOVMmESEh_GoEgmtIzIqkDuW45VGZ7cJzL47d5J6CcUM15B1ioF-bX-6xdrTj8qjj3KRxzyDd3JSCnRoFHiSZEbDfoB8hxLS5WOFqO0KVKST-XSNFeDK1wnWAav9MgqU4CDkBL5D0aSMljNuImigTYvPfkH6FfbE05-U5rCLmvD_iBKowNVVUJ6bz6JAMqoFtU1MSWdPs4hZbngoGPWSKEA6BOaVNhfwOn8tC5ssLsdAWA',

  /***************************************************************************
   * SAML Assertion
   ***************************************************************************/

  OKTA: {
    saml: {
      path: '/login/okta-saml',
      entryPoint: 'https://dev-429449.oktapreview.com/app/apothecom_taiholocalhostdevelopment_1/exkurqrlm8XBFOdI10h7/sso/saml',
      issuer: 'http://www.okta.com/exkurqrlm8XBFOdI10h7',
      callbackUrl: '',
      protocol: 'https://',
      cert: 'MIIDpDCCAoygAwIBAgIGAV2Tx75sMA0GCSqGSIb3DQEBCwUAMIGSMQswCQYDVQQGEwJVUzETMBEGA1UECAwKQ2FsaWZvcm5pYTEWMBQGA1UEBwwNU2FuIEZyYW5jaXNjbzENMAsGA1UECgwET2t0YTEUMBIGA1UECwwLU1NPUHJvdmlkZXIxEzARBgNVBAMMCmRldi00Mjk0NDkxHDAaBgkqhkiG9w0BCQEWDWluZm9Ab2t0YS5jb20wHhcNMTcwNzMwMTM1NDI3WhcNMjcwNzMwMTM1NTI3WjCBkjELMAkGA1UEBhMCVVMxEzARBgNVBAgMCkNhbGlmb3JuaWExFjAUBgNVBAcMDVNhbiBGcmFuY2lzY28xDTALBgNVBAoMBE9rdGExFDASBgNVBAsMC1NTT1Byb3ZpZGVyMRMwEQYDVQQDDApkZXYtNDI5NDQ5MRwwGgYJKoZIhvcNAQkBFg1pbmZvQG9rdGEuY29tMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA1SxbVBF3VWnDtnBbylxSd+0hIhXGmJrNfvV+FQXlR2uQqu2wqy4xLB12lF+fO3HLki8nb5xXqGvhfXn5trSk7ENt+qOWjdiYNJm7/mwhdyPZxtzJxJF5cizw+59FYLpxSVLfsGSMeFLgBKAWTrp0D1Z9lXMDUMhGbgX+/R5CmzpCea9pTM2UGtX2T/SJa4h/ohyqM06H5OREcziN5OhHbmqLxDFxzvZ3ytPE23thoQ4G9gSXG2b2XrYVLk9u9/7yhscOgsvxnNgqNIYATD4cN0keXoicm2DdpVEsz+aoP4ZlLbYto/WhdlMMgRG+RwuB4BlIxBusL6BGPqWGz03fcwIDAQABMA0GCSqGSIb3DQEBCwUAA4IBAQAYGBJ+5z5E+p6bvqiW/VIPQ68BFmTnyRnSAi80WXIX7xUS5/fHZ76rR13qtuhQ5V9D1TYYwXgOmrgHirPe3y1xr1wWQOUBec05rPMFsM4QBbUcBgl20EM//1jrG1sSMawG/N1BdJNQepdV0uSt3JX/jSnuVDbw6HJtRwbwlmm5q1ImmJYHdyDDX9WoF1UMwV5Hc2fFCQOA4RPZuf+Hz8hMtSFG5VhiKiUeLu+SHky7GU2iFKRTgBPONIbJmchiHDCsa80QNapHYchNjWqV32iInjYfT+ecgrE4BW0EQY0mxM8UwboMej1gYF7wGvVz4xPhUE0RvYV81Ii3DLSgOqWf'
    }
  },

  Astex: {
    saml: {
      path: '/login/okta-saml',
      entryPoint: 'https://dev-429449.oktapreview.com/app/apothecom_taiholocalhostdevelopment_1/exkurqrlm8XBFOdI10h7/sso/saml',
      issuer: 'http://www.okta.com/exkurqrlm8XBFOdI10h7',
      callbackUrl: '',
      protocol: 'https://',
      cert: 'MIIDpDCCAoygAwIBAgIGAV2Tx75sMA0GCSqGSIb3DQEBCwUAMIGSMQswCQYDVQQGEwJVUzETMBEGA1UECAwKQ2FsaWZvcm5pYTEWMBQGA1UEBwwNU2FuIEZyYW5jaXNjbzENMAsGA1UECgwET2t0YTEUMBIGA1UECwwLU1NPUHJvdmlkZXIxEzARBgNVBAMMCmRldi00Mjk0NDkxHDAaBgkqhkiG9w0BCQEWDWluZm9Ab2t0YS5jb20wHhcNMTcwNzMwMTM1NDI3WhcNMjcwNzMwMTM1NTI3WjCBkjELMAkGA1UEBhMCVVMxEzARBgNVBAgMCkNhbGlmb3JuaWExFjAUBgNVBAcMDVNhbiBGcmFuY2lzY28xDTALBgNVBAoMBE9rdGExFDASBgNVBAsMC1NTT1Byb3ZpZGVyMRMwEQYDVQQDDApkZXYtNDI5NDQ5MRwwGgYJKoZIhvcNAQkBFg1pbmZvQG9rdGEuY29tMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA1SxbVBF3VWnDtnBbylxSd+0hIhXGmJrNfvV+FQXlR2uQqu2wqy4xLB12lF+fO3HLki8nb5xXqGvhfXn5trSk7ENt+qOWjdiYNJm7/mwhdyPZxtzJxJF5cizw+59FYLpxSVLfsGSMeFLgBKAWTrp0D1Z9lXMDUMhGbgX+/R5CmzpCea9pTM2UGtX2T/SJa4h/ohyqM06H5OREcziN5OhHbmqLxDFxzvZ3ytPE23thoQ4G9gSXG2b2XrYVLk9u9/7yhscOgsvxnNgqNIYATD4cN0keXoicm2DdpVEsz+aoP4ZlLbYto/WhdlMMgRG+RwuB4BlIxBusL6BGPqWGz03fcwIDAQABMA0GCSqGSIb3DQEBCwUAA4IBAQAYGBJ+5z5E+p6bvqiW/VIPQ68BFmTnyRnSAi80WXIX7xUS5/fHZ76rR13qtuhQ5V9D1TYYwXgOmrgHirPe3y1xr1wWQOUBec05rPMFsM4QBbUcBgl20EM//1jrG1sSMawG/N1BdJNQepdV0uSt3JX/jSnuVDbw6HJtRwbwlmm5q1ImmJYHdyDDX9WoF1UMwV5Hc2fFCQOA4RPZuf+Hz8hMtSFG5VhiKiUeLu+SHky7GU2iFKRTgBPONIbJmchiHDCsa80QNapHYchNjWqV32iInjYfT+ecgrE4BW0EQY0mxM8UwboMej1gYF7wGvVz4xPhUE0RvYV81Ii3DLSgOqWf'
    }
  },  

  TPC: {
    saml: {
      path: '/login/okta-saml',
      entryPoint: 'https://dev-429449.oktapreview.com/app/apothecom_taiholocalhostdevelopment_1/exkurqrlm8XBFOdI10h7/sso/saml',
      issuer: 'http://www.okta.com/exkurqrlm8XBFOdI10h7',
      callbackUrl: '',
      protocol: 'https://',
      cert: 'MIIDpDCCAoygAwIBAgIGAV2Tx75sMA0GCSqGSIb3DQEBCwUAMIGSMQswCQYDVQQGEwJVUzETMBEGA1UECAwKQ2FsaWZvcm5pYTEWMBQGA1UEBwwNU2FuIEZyYW5jaXNjbzENMAsGA1UECgwET2t0YTEUMBIGA1UECwwLU1NPUHJvdmlkZXIxEzARBgNVBAMMCmRldi00Mjk0NDkxHDAaBgkqhkiG9w0BCQEWDWluZm9Ab2t0YS5jb20wHhcNMTcwNzMwMTM1NDI3WhcNMjcwNzMwMTM1NTI3WjCBkjELMAkGA1UEBhMCVVMxEzARBgNVBAgMCkNhbGlmb3JuaWExFjAUBgNVBAcMDVNhbiBGcmFuY2lzY28xDTALBgNVBAoMBE9rdGExFDASBgNVBAsMC1NTT1Byb3ZpZGVyMRMwEQYDVQQDDApkZXYtNDI5NDQ5MRwwGgYJKoZIhvcNAQkBFg1pbmZvQG9rdGEuY29tMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA1SxbVBF3VWnDtnBbylxSd+0hIhXGmJrNfvV+FQXlR2uQqu2wqy4xLB12lF+fO3HLki8nb5xXqGvhfXn5trSk7ENt+qOWjdiYNJm7/mwhdyPZxtzJxJF5cizw+59FYLpxSVLfsGSMeFLgBKAWTrp0D1Z9lXMDUMhGbgX+/R5CmzpCea9pTM2UGtX2T/SJa4h/ohyqM06H5OREcziN5OhHbmqLxDFxzvZ3ytPE23thoQ4G9gSXG2b2XrYVLk9u9/7yhscOgsvxnNgqNIYATD4cN0keXoicm2DdpVEsz+aoP4ZlLbYto/WhdlMMgRG+RwuB4BlIxBusL6BGPqWGz03fcwIDAQABMA0GCSqGSIb3DQEBCwUAA4IBAQAYGBJ+5z5E+p6bvqiW/VIPQ68BFmTnyRnSAi80WXIX7xUS5/fHZ76rR13qtuhQ5V9D1TYYwXgOmrgHirPe3y1xr1wWQOUBec05rPMFsM4QBbUcBgl20EM//1jrG1sSMawG/N1BdJNQepdV0uSt3JX/jSnuVDbw6HJtRwbwlmm5q1ImmJYHdyDDX9WoF1UMwV5Hc2fFCQOA4RPZuf+Hz8hMtSFG5VhiKiUeLu+SHky7GU2iFKRTgBPONIbJmchiHDCsa80QNapHYchNjWqV32iInjYfT+ecgrE4BW0EQY0mxM8UwboMej1gYF7wGvVz4xPhUE0RvYV81Ii3DLSgOqWf'
    }
  },  

  };
