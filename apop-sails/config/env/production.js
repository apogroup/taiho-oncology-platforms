/**
 * Production environment settings
 *
 * This file can include shared settings for a production environment,
 * such as API keys or remote database passwords.  If you're using
 * a version control solution for your Sails app, this file will
 * be committed to your repository unless you add it to your .gitignore
 * file.  If your repository will be publicly viewable, don't add
 * any private information to this file!
 *
 */

module.exports = {
  /***************************************************************************
   * Set the default database connection for models in the production        *
   * environment (see config/connections.js and config/models.js )           *
   ***************************************************************************/

  // models: {
  // 	connection: ''
  // },

  /***************************************************************************
   * Set the local environment
   ***************************************************************************/

  local: {
    port: process.env.PORT || 3000,
    environment: 'production',
  },

  /***************************************************************************
   * Connections
   ***************************************************************************/

  connections: {
    MySQLServer: {
      adapter: 'sails-mysql',
      // Takeda Platform Database (Production)
      host: 'taihodecproduction.cb7gmzg6z9em.us-east-1.rds.amazonaws.com',
			user: 'taihodbadmin', //optional
			password: '21yPEKW73cwW', //optional
			database: 'taihodecproduction' //optional
    },
  },

  /***************************************************************************
   * Mail Service
   ***************************************************************************/

  mailerOptions: {
    service: 'Mailgun',
    auth: {
      user: 'postmaster@sandbox2c5124cbba5d441d8cf3e76c970c563a.mailgun.org',
      pass: 'e13f16471da17d98b3639c8863ede87a-f7d0b107-435f8cee'
    }
  },

  messageOptions: {
    from: "taiho.scientific.platform@gmail.com",
    to: "apotaiho@apothecom.com",
    subject: "New Comment | Taiho DEC-C Scientific Platform",
    text: "",
    html: "<p>You have received a new comment on Taiho DEC-C Scientific Platform</p>"
  },

  /***************************************************************************
   * AWS API Gateway
   ***************************************************************************/

  AWSS3Bucket: 'taiho-dec-c-repository-production',
  AWSApiRoot: 'https://b25r52aaei.execute-api.us-east-1.amazonaws.com/beta',
  AWSApiObjectPath: 'private/taiho-platform-files/',
  AWSApiAccessToken: '64UYlFkmJ47OjeUhhH8CD6tKrCjyEHuV7xPPK9LP',

  /***************************************************************************
   * CMS API Config
   ***************************************************************************/

  CMSApiEndpoint: '',
  CMSApiAccessToken: '',

  /***************************************************************************
   * SAML Assertion
   ***************************************************************************/

  OKTA: {
    saml: {
      path: '/login/okta-saml',
      entryPoint: 'https://taihooncology.okta.com/app/taihooncology_toidataexchangecenter_1/exkeylv37jvpFyJV41t7/sso/saml',
      issuer: 'http://www.okta.com/exkeylv37jvpFyJV41t7',
      callbackUrl: '',
      protocol: 'https://',
      cert: 'MIIDqjCCApKgAwIBAgIGAV2msq8LMA0GCSqGSIb3DQEBCwUAMIGVMQswCQYDVQQGEwJVUzETMBEGA1UECAwKQ2FsaWZvcm5pYTEWMBQGA1UEBwwNU2FuIEZyYW5jaXNjbzENMAsGA1UECgwET2t0YTEUMBIGA1UECwwLU1NPUHJvdmlkZXIxFjAUBgNVBAMMDXRhaWhvb25jb2xvZ3kxHDAaBgkqhkiG9w0BCQEWDWluZm9Ab2t0YS5jb20wHhcNMTcwODAzMDYwNDE0WhcNMjcwODAzMDYwNTEzWjCBlTELMAkGA1UEBhMCVVMxEzARBgNVBAgMCkNhbGlmb3JuaWExFjAUBgNVBAcMDVNhbiBGcmFuY2lzY28xDTALBgNVBAoMBE9rdGExFDASBgNVBAsMC1NTT1Byb3ZpZGVyMRYwFAYDVQQDDA10YWlob29uY29sb2d5MRwwGgYJKoZIhvcNAQkBFg1pbmZvQG9rdGEuY29tMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAhZeGNhI/90mJiZJ4VORTuStW5256bAodtoBHv7QKjs3K2wUv2SdzXkfub9VUGzP5BzeTrcHRFDScr39X3FhnJ2EvX9CHgZ+vFXX74paH5nm38ijKdZtVHTJOiSUJyhgSeLZOday4Dh3i0Vv4a6CYkxVu0yTpqVYoZUIvXYXsr4ODu1ISQLaPUFCLGwHM0upOfIVUXEwrI5YLCuFaskaNWQPx6S0Rxn51nyneh7F9orRkEdpeBQflW+4B0/K+0nCBdM9TfCrExhONyYZlpwFuF8nYXDFdU3TTFD0vl59g4LtnMX7THXwpNLq8TLX4HUi8vrSpHim6GjxC0oEMmDBsawIDAQABMA0GCSqGSIb3DQEBCwUAA4IBAQALf2jH4v5o0ueq8SUhGG6/WG7qsHs0aA8aICCFb9l/bdcbJuhRxce8aoHH+FSvOQ4Vk+CvjPFb/8bx9kWfiOAx+Ynsgt3ijuGFwsFBooZ/39BLBkHNGUFcype+Jmbh5jjfO2AyHz7/qrYTknplTtbTf72XBpbofiq0NwiTaL3LIzJ9vKN7XoY6LKHUBPyHPcxJl+1R5rCLH02nT3JBcGQzILo/opReM2mg9RKwT3J+qtM8WnpWieS2Idb6tusiaK734FGIuLRCTrNGUVrvJM/GECXmgNN6h1ISx/vJw6NhywQ6msgitqvgSg5M+t3nYB53jTe84rNSTTCzkt56125V'
    }
  },

	TPC: {
		saml: {
		  path: '/login/tpc',
		  entryPoint: 'https://login.microsoftonline.com/34ddb339-7fd0-4f00-9041-c2e47fbbc9f4/saml2',
		  issuer: 'https://taihodataexchangecenter.com/',
		  callbackUrl: 'https://taihodataexchangecenter.com/login/tpc',
		  protocol: 'https://',
		  cert: 'MIIC8DCCAdigAwIBAgIQZZyPYl/eyptHvBV9UvbPrzANBgkqhkiG9w0BAQsFADA0MTIwMAYDVQQDEylNaWNyb3NvZnQgQXp1cmUgRmVkZXJhdGVkIFNTTyBDZXJ0aWZpY2F0ZTAeFw0yMDExMTAwMjExNDFaFw0yMzExMTAwMjExNDFaMDQxMjAwBgNVBAMTKU1pY3Jvc29mdCBBenVyZSBGZWRlcmF0ZWQgU1NPIENlcnRpZmljYXRlMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA+tAjxAHDWPH0wdJ/vqaSHZfmxG/YJDUHrN2jReLoRroHoWoaHVBbJHvduP0zR0pdSA4fx/GD9+WdJq/onVEMmntjbwjHKUqjzpDIhZ7/juRvNBaizryNgiwz7yAb6T2ufdSKi5kOCC1iHrrwGbGWNcbr33J6OcAEzcaAIfjMYTn1Zk6mg0RcuT/Ahc1qRFmlgYmIJh7Aws0OK1Wc0vwOBq+AaekaTCPV5/9M/1GIrhcWsJqm5NIKTfga3Zm2PJWC7uLcUIlrJFpaI2bwBkH2rwzTMxUhLloGqV6rIKpFfGBtlXQwT7ASbYul/ZLSZO3l5QD/+c+39254b6B8LWJnCQIDAQABMA0GCSqGSIb3DQEBCwUAA4IBAQCNlhrOax8N2eASG5GNFds0nrpameHG745DVgKtfr/DRtGuQDutfQHyeVQq9YFtp9jgeq/Y1P1Wr1Gcj9x5mq8oQj7nxu3/Q6R2zzliFIgACTn5ooyIuWJwAyOakGKlDmsUTqKg79rEzc3Yq4GYKeJDNUer1Vqi9bj/qCD3bKkDsxvj5grTevIfjZCq3TPxXmoK/DMu12AaYlEM4R9pb8U/PzlRivS4WnLm4Cgl594SLUMkZrloGdeQXDcv1Ei4vSva9EwBXkWJT2G8QxvNyScGpyhuOmKwPmZBgh97SouwTdKYHZ5oeSiCgaYIY/HnPU2KiJCLafrlhANP3/yktLHC'
		}
  },

	Astex: {
		saml: {
		  path: '/login/astex',
		  entryPoint: 'https://login.microsoftonline.com/952cf131-fad4-4a29-876e-01cf059a407d/saml2',
		  issuer: 'https://taihodataexchangecenter.com/',
		  callbackUrl: 'https://taihodataexchangecenter.com/login/astex',
		  protocol: 'https://',
		  cert: 'MIIC8DCCAdigAwIBAgIQGYFl7FPc7IpEXKAo7uKz2zANBgkqhkiG9w0BAQsFADA0MTIwMAYDVQQDEylNaWNyb3NvZnQgQXp1cmUgRmVkZXJhdGVkIFNTTyBDZXJ0aWZpY2F0ZTAeFw0yMDExMTAyMjA2MTRaFw0yMzExMTAyMjA2MTRaMDQxMjAwBgNVBAMTKU1pY3Jvc29mdCBBenVyZSBGZWRlcmF0ZWQgU1NPIENlcnRpZmljYXRlMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAujNgNXN/bei7v1bG0Z/OtmzYaq2V4gITB9J8DAjqA8DRqgRSkz0aces/nX/H7b2CMB8rQQdxI+a7XFNeFbSa8QFtfpKfmLhViV0sVLOon0vjApN+uX+rB+NNHieX2ddOurojX+zFgsnsDFIz9r58yNhOHShzg67eMsNQC9xgp9QaDEbZNoQcfqLGxTXuQtLn4UtiiBVtOzW5lGFYhsef7Q5EBg1A9e/g4k4gtE2gcR/BglSgjY9P+pUmh5QgT5ZVPlqM5QwLDOPanAZZ3PXkXaQqyRm9x/hlrhQfHz63jl0Is47wY9EPJYRj7fhB6cg0v+svgzhAswSCdy/T5ItvMQIDAQABMA0GCSqGSIb3DQEBCwUAA4IBAQBKPmoQKKYOCkZi4Xd/0cyEpZvY8xxcDr3Uaxr8smtujR0pcouHMTMA/qesz3PV9k5uBHHzbBE1lqiMy7NxY+9rQK/aLU6J/fwgzhZ43v3YDhFYdb0kjX2BU9AE6IyIf7+VhcyEXLxAfuFP7WcBxIRPTCX0WN3USgxNrlA4mHTE0JAd0neVtkS35B+f/GTRLApuclhgL/oghQxeOTPSlFFk2Kc6fA8B+OCWgXySl1ro7T7qQBvnDPBw7pq1hyAxk+a4fINVYFZtHRxAbHZo1NLlHPhMcu5T9J+pOKV3In7tsVoYK1eDg1VEieDMO+s7Gomw2F3FE5iV+ZQ8pMMxDqCn'
		}
  }



  /***************************************************************************
   * Set the log level in production environment to "silent"                 *
   ***************************************************************************/

  // log: {
  //   level: "silent"
  // }
};

