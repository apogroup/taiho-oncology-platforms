/**
 * Development environment settings
 *
 * This file can include shared settings for a development team,
 * such as API keys or remote database passwords.  If you're using
 * a version control solution for your Sails app, this file will
 * be committed to your repository unless you add it to your .gitignore
 * file.  If your repository will be publicly viewable, don't add
 * any private information to this file!
 *
 */

module.exports = {

	/***************************************************************************
	 * Set the default database connection for models in the development       *
	 * environment (see config/connections.js and config/models.js )           *
	 ***************************************************************************/

	// models: {
	// 	connection: ''
	// },

	/***************************************************************************
	 * Set local evnironment
	 ***************************************************************************/

	local: {
		port: process.env.PORT || 3001,
		environment: 'staging'
	},

	/***************************************************************************
	 * Connections
	 ***************************************************************************/

	connections: {
		MySQLServer: {
			adapter: 'sails-mysql',
			// Takeda Platform Database (Staging)
			host: 'taihodecstaging.cb7gmzg6z9em.us-east-1.rds.amazonaws.com',
			user: 'taihodbadmin', //optional
			password: '21yPEKW73cwW', //optional
			database: 'taihodecstaging' //optional
		}
	},

  /***************************************************************************
   * Mail Service
   ***************************************************************************/

  mailerOptions: {
    service: 'Mailgun',
    auth: {
      user: 'postmaster@sandbox2c5124cbba5d441d8cf3e76c970c563a.mailgun.org',
      pass: 'e13f16471da17d98b3639c8863ede87a-f7d0b107-435f8cee'
    }
  },

  messageOptions: {
    from: "taiho.scientific.platform@gmail.com",
    to: "apotaiho@apothecom.com",
    subject: "New Comment | Taiho DEC-C Scientific Platform",
    text: "",
    html: "<p>You have received a new comment on Taiho DEC-C Scientific Platform</p>"
  },
  
	/***************************************************************************
	 * AWS API Gateway
	 ***************************************************************************/

    AWSS3Bucket: 'taiho-dec-c-repository',
    AWSApiRoot: 'https://b25r52aaei.execute-api.us-east-1.amazonaws.com/beta',
    AWSApiObjectPath: 'private/taiho-platform-files/',
    AWSApiAccessToken: 'm5ephdLa0r8G10RCUeyBV3rxtVxlK7Us4SfiBz6r',

	/***************************************************************************
	* CMS API Config
	***************************************************************************/

	CMSApiEndpoint: '',
	CMSApiAccessToken: '',

	/***************************************************************************
	 * SAML Assertion
	 ***************************************************************************/

	OKTA: {
		saml: {
			path: '/login/okta-saml',
			entryPoint: 'https://dev-429449.oktapreview.com/app/apothecom_taihodataexchangecenterstaging_1/exksu737n17U0SDKt0h7/sso/saml',
			issuer: 'http://www.okta.com/exksu737n17U0SDKt0h7',
			callbackUrl: '',
			protocol: 'https://',
			cert: 'MIIDpDCCAoygAwIBAgIGAV2Tx75sMA0GCSqGSIb3DQEBCwUAMIGSMQswCQYDVQQGEwJVUzETMBEGA1UECAwKQ2FsaWZvcm5pYTEWMBQGA1UEBwwNU2FuIEZyYW5jaXNjbzENMAsGA1UECgwET2t0YTEUMBIGA1UECwwLU1NPUHJvdmlkZXIxEzARBgNVBAMMCmRldi00Mjk0NDkxHDAaBgkqhkiG9w0BCQEWDWluZm9Ab2t0YS5jb20wHhcNMTcwNzMwMTM1NDI3WhcNMjcwNzMwMTM1NTI3WjCBkjELMAkGA1UEBhMCVVMxEzARBgNVBAgMCkNhbGlmb3JuaWExFjAUBgNVBAcMDVNhbiBGcmFuY2lzY28xDTALBgNVBAoMBE9rdGExFDASBgNVBAsMC1NTT1Byb3ZpZGVyMRMwEQYDVQQDDApkZXYtNDI5NDQ5MRwwGgYJKoZIhvcNAQkBFg1pbmZvQG9rdGEuY29tMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA1SxbVBF3VWnDtnBbylxSd+0hIhXGmJrNfvV+FQXlR2uQqu2wqy4xLB12lF+fO3HLki8nb5xXqGvhfXn5trSk7ENt+qOWjdiYNJm7/mwhdyPZxtzJxJF5cizw+59FYLpxSVLfsGSMeFLgBKAWTrp0D1Z9lXMDUMhGbgX+/R5CmzpCea9pTM2UGtX2T/SJa4h/ohyqM06H5OREcziN5OhHbmqLxDFxzvZ3ytPE23thoQ4G9gSXG2b2XrYVLk9u9/7yhscOgsvxnNgqNIYATD4cN0keXoicm2DdpVEsz+aoP4ZlLbYto/WhdlMMgRG+RwuB4BlIxBusL6BGPqWGz03fcwIDAQABMA0GCSqGSIb3DQEBCwUAA4IBAQAYGBJ+5z5E+p6bvqiW/VIPQ68BFmTnyRnSAi80WXIX7xUS5/fHZ76rR13qtuhQ5V9D1TYYwXgOmrgHirPe3y1xr1wWQOUBec05rPMFsM4QBbUcBgl20EM//1jrG1sSMawG/N1BdJNQepdV0uSt3JX/jSnuVDbw6HJtRwbwlmm5q1ImmJYHdyDDX9WoF1UMwV5Hc2fFCQOA4RPZuf+Hz8hMtSFG5VhiKiUeLu+SHky7GU2iFKRTgBPONIbJmchiHDCsa80QNapHYchNjWqV32iInjYfT+ecgrE4BW0EQY0mxM8UwboMej1gYF7wGvVz4xPhUE0RvYV81Ii3DLSgOqWf'
		}
	},

	TPC: {
		saml: {
		  path: '/login/tpc',
		  entryPoint: 'https://login.microsoftonline.com/34ddb339-7fd0-4f00-9041-c2e47fbbc9f4/saml2',
		  issuer: 'https://taihodataexchangecenterstaging.com/',
		  callbackUrl: 'https://taihodataexchangecenterstaging.com/login/tpc',
		  protocol: 'https://',
		  cert: 'MIIC8DCCAdigAwIBAgIQZZyPYl/eyptHvBV9UvbPrzANBgkqhkiG9w0BAQsFADA0MTIwMAYDVQQDEylNaWNyb3NvZnQgQXp1cmUgRmVkZXJhdGVkIFNTTyBDZXJ0aWZpY2F0ZTAeFw0yMDExMTAwMjExNDFaFw0yMzExMTAwMjExNDFaMDQxMjAwBgNVBAMTKU1pY3Jvc29mdCBBenVyZSBGZWRlcmF0ZWQgU1NPIENlcnRpZmljYXRlMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA+tAjxAHDWPH0wdJ/vqaSHZfmxG/YJDUHrN2jReLoRroHoWoaHVBbJHvduP0zR0pdSA4fx/GD9+WdJq/onVEMmntjbwjHKUqjzpDIhZ7/juRvNBaizryNgiwz7yAb6T2ufdSKi5kOCC1iHrrwGbGWNcbr33J6OcAEzcaAIfjMYTn1Zk6mg0RcuT/Ahc1qRFmlgYmIJh7Aws0OK1Wc0vwOBq+AaekaTCPV5/9M/1GIrhcWsJqm5NIKTfga3Zm2PJWC7uLcUIlrJFpaI2bwBkH2rwzTMxUhLloGqV6rIKpFfGBtlXQwT7ASbYul/ZLSZO3l5QD/+c+39254b6B8LWJnCQIDAQABMA0GCSqGSIb3DQEBCwUAA4IBAQCNlhrOax8N2eASG5GNFds0nrpameHG745DVgKtfr/DRtGuQDutfQHyeVQq9YFtp9jgeq/Y1P1Wr1Gcj9x5mq8oQj7nxu3/Q6R2zzliFIgACTn5ooyIuWJwAyOakGKlDmsUTqKg79rEzc3Yq4GYKeJDNUer1Vqi9bj/qCD3bKkDsxvj5grTevIfjZCq3TPxXmoK/DMu12AaYlEM4R9pb8U/PzlRivS4WnLm4Cgl594SLUMkZrloGdeQXDcv1Ei4vSva9EwBXkWJT2G8QxvNyScGpyhuOmKwPmZBgh97SouwTdKYHZ5oeSiCgaYIY/HnPU2KiJCLafrlhANP3/yktLHC'
		}
	  },

	Astex: {
		saml: {
		  path: '/login/astex',
		  entryPoint: 'https://login.microsoftonline.com/952cf131-fad4-4a29-876e-01cf059a407d/saml2',
		  issuer: 'https://taihodataexchangecenterstaging.com/',
		  callbackUrl: 'https://taihodataexchangecenterstaging.com/login/astex',
		  protocol: 'https://',
		  cert: 'MIIC8DCCAdigAwIBAgIQZEl1Dl5Y2IhAm0n7S/x2CjANBgkqhkiG9w0BAQsFADA0MTIwMAYDVQQDEylNaWNyb3NvZnQgQXp1cmUgRmVkZXJhdGVkIFNTTyBDZXJ0aWZpY2F0ZTAeFw0yMDExMTEyMTAzMDlaFw0yMzExMTEyMTAzMDlaMDQxMjAwBgNVBAMTKU1pY3Jvc29mdCBBenVyZSBGZWRlcmF0ZWQgU1NPIENlcnRpZmljYXRlMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA4ih9sh5gMJXSQxcHOa8oirYjI7lSK/z4PKTpIO2QGYgeb23ERsiSaPFNG7PFbehAWKS5m6KM4o8/DWTOp9AherOyOxTutHW5nuOtE6LVQ216hLAfQyp3AgMGPbxloe3pVCXFP6BPR0M/Tvmw+CX6T0Ua94ispMUtUOrOK6CVmgnytJW/kjUfFofV1tEses5G1XJgAMQK4tQ5ZL0wvcvNdTy9pbgFLQWyCpT1xquuNYe/n90OxW8PwYFp25QM3b56pg0wTuQEXN6RjDLX90iCdVRZqsnuG4Jl8ILNR3mx5Vkkw7jCgpbMM42AMzL01mQtOVNksX0NPRJgfI01qBcbBQIDAQABMA0GCSqGSIb3DQEBCwUAA4IBAQBEc7u/lKQ2CBJ9jCG6OGRGhE5TFSNjaG0wetT8VfERwRVyDBwxTm5pGnXxW2top9xl8WJk53WNsIYdSbK6o+GRm+lO52f76ycbZLM1p4Kc9we6rXxO8snzS4HQ2P9NidEpLHly6SF/eBi9Hpz2fPLL3rvMFuv81WOMAtYL14AvkT6POOiAZ40i0VuZWECYW7k8LEW/nzc8muy5dCGKqe+BcXEPfMFfS4GGQOr6R9aTmlavB+BYCr7n6xMllYyLeXbNNlxoOUG3s3CsRRvYBSPYc6b7n9+1+L6UaNru0GqxF6giKGGIAXWiFnbkVxoLFfRgSvBikyqgD8cG3F43UggB'
		}
	  }
	

};
