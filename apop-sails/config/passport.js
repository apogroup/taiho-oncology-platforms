//--------------------------------
// Require PASSPORT
//--------------------------------

const { setMaxListeners } = require('process');

var passport 		= require('passport'),
	LocalStrategy 	= require('passport-local').Strategy,
	SamlStrategy	= require('passport-saml').Strategy,
	MultiSamlStrategy = require('passport-saml/multiSamlStrategy'),
	https			= require('https');

//--------------------------------
// SAML SSO Strategy API
//--------------------------------
let OKTA = {
	saml: {
		path: '/login/sso',
		entryPoint: '',
		issuer: '',
		cert: ''
	}
};

switch(process.env.NODE_ENV) {
	case 'development':
		OKTA = require('./env/development').OKTA;
		TPC = require('./env/development').TPC;
		Astex = require('./env/development').Astex;
		break;
	case 'staging':
		OKTA = require('./env/staging').OKTA;
		TPC = require('./env/staging').TPC;
		Astex = require('./env/staging').Astex;
		break;
	case 'production':
		OKTA = require('./env/production').OKTA;
		TPC = require('./env/production').TPC;
		Astex = require('./env/production').Astex;
		break;
	default:
		OKTA = require('./env/development').OKTA;
		TPC = require('./env/development').TPC;
		Astex = require('./env/development').Astex;
}

passport.use('okta-saml', new SamlStrategy({
		path: OKTA.saml.path,
		entryPoint: OKTA.saml.entryPoint,
	    issuer: OKTA.saml.issuer,
	    callbackUrl: OKTA.saml.callbackUrl,
   		protocol: OKTA.saml.protocol,
		cert: OKTA.saml.cert,
		authnContext: 'urn:oasis:names:tc:SAML:2.0:ac:classes:PasswordProtectedTransportSSO'
	},
	function (assertion, done) {
		sails.log.info("assertion: ", assertion);
		return done(null, assertion);
	})
);

passport.use('tpc', new SamlStrategy({
		path: TPC.saml.path,
		entryPoint: TPC.saml.entryPoint,
	    issuer: TPC.saml.issuer,
	    callbackUrl: TPC.saml.callbackUrl,
   		protocol: TPC.saml.protocol,
		cert: TPC.saml.cert,
		disableRequestedAuthnContext: true
	},
	function (assertion, done) {
		return done(null, assertion);
	})
);

passport.use('astex', new SamlStrategy({
		path: Astex.saml.path,
		entryPoint: Astex.saml.entryPoint,
	    issuer: Astex.saml.issuer,
	    callbackUrl: Astex.saml.callbackUrl,
   		protocol: Astex.saml.protocol,
		cert: Astex.saml.cert,
		authnContext: 'urn:oasis:names:tc:SAML:2.0:ac:classes:PasswordProtectedTransport'
	},
	function (assertion, done) {
		return done(null, assertion);
	})
);

// passport.use(new MultiSamlStrategy(
// 	{
// 		passReqToCallback: true,
// 		getSamlOptions: function(request, done) {
// 			return done(null, conf);
// 		}
// 	},
// 	function(req, profile, done) {
// 		const user = profile;
// 		return done(null, user);
// 	}
// ))

