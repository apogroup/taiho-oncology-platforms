/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes map URLs to views and controllers.
 *
 * If Sails receives a URL that doesn't match any of the routes below,
 * it will check for matching files (images, scripts, stylesheets, etc.)
 * in your assets directory.  e.g. `http://localhost:1337/images/foo.jpg`
 * might match an image file: `/assets/images/foo.jpg`
 *
 * Finally, if those don't match either, the default 404 handler is triggered.
 * See `api/responses/notFound.js` to adjust your app's 404 logic.
 *
 * Note: Sails doesn't ACTUALLY serve stuff from `assets`-- the default Gruntfile in Sails copies
 * flat files from `assets` to `.tmp/public`.  This allows you to do things like compile LESS or
 * CoffeeScript for the front-end.
 *
 * For more information on configuring custom routes, check out:
 * http://sailsjs.org/#!/documentation/concepts/Routes/RouteTargetSyntax.html
 */

module.exports.routes = {

	/***************************************************************************
	*                                                                          *
	* Make the view located at `views/homepage.ejs` (or `views/homepage.jade`, *
	* etc. depending on your default view engine) your home page.              *
	*                                                                          *
	* (Alternatively, remove this and add an `index.html` file in your         *
	* `assets` directory)                                                      *
	*                                                                          *
	***************************************************************************/

	//-------------------------------------
	// Bootstrap Single Page Angular App
	//-------------------------------------

	'/': 'BootstrapController.index',

	//-------------------------------------
	// PDFJS Viewer
	//-------------------------------------

	'/pdf': 'Bootstrap/pdfjs-viewer',

	//-------------------------------------
	// Login/Logout Auth
	//-------------------------------------

	'post /login/:config': 'AuthController.auth',

	'get /login/:config': 'AuthController.login',

	'/logout': 'AuthController.logout',

	//Splash

	'/login-sso': 'BootstrapController.login_sso',

	//-------------------------------------
	// Data API
	//-------------------------------------

	'get /user': 'UserController.getUser',

	'get /platform-alerts': 'AlertController.getAlerts',

	'get /platform-alerts-status': 'AlertStatusController.getAlertStatus',

	'post /platform-alerts-status/read': 'AlertStatusController.saveReadAlertStatus',

	'post /platform-alerts-status/alerted': 'AlertStatusController.saveAlertedAlertStatus',

	'get /favorites': 'FavoriteController.getFavorites',

	'post /favorite/save': 'FavoriteController.saveFavorite',

	'post /favorite/archive': 'FavoriteController.archiveFavorite',

	'post /askplatform/comment/save': 'AskPlatform.saveAskPlatformComment',

	'get /askplatform/survey': 'AskPlatform.getSubmittedSurveys',

	'post /askplatform/survey/save': 'AskPlatform.saveAskPlatformSurvey',

	'post /metric/save': 'MetricController.saveMetric',

	//-------------------------------------
	// Content API (Policy Protected Files)
	//-------------------------------------

	'get /content/documents/*': 'ContentFileController.s3',

	// 'get /content/figures/*': 'ContentFileController.s3',

	'get /content/references/*': 'ContentFileController.s3',

	'get /content/*': 'ContentFileController.serve',

	//-------------------------------------
	// Content API (Policy Protected Files)
	//-------------------------------------

	// 'get /cms/*': 'CMSController.fetch'

};
