'use strict';

//---------------------------------------------/
//  PLATFORM CONFIGURATIONS
// 
//  Used on both development, and staging/production
//
//  Define config variables for distribution build
//
//---------------------------------------------/

module.exports = {

    title: 'Taiho Data Exchange Center',
    themeColor: '#3c3c3c',

    compound: 'Taiho',
    indication: 'Data Exchange Center'

};