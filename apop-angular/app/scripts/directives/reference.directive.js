'use strict';

//-------------------------------
// <sup> tag directive
//-------------------------------

angular.module('apopApp')
	.directive('reference', function ($compile) {
		return {
			restrict: 'A',
			replace: true,
			scope: false,
			template: function (element, attrs) {

				try {

					var id = attrs.reference;

					var markup = [
						'<sup>',
						'<div ng-repeat="reference in (references | filter:{ id: \'' + id + '\'} ) track by reference.id" class="apop-inline-ref-container">',
						'<button class="apop-inline-ref-button" ng-bind-html="reference.UniqueID" ng-click="$event.stopPropagation(); openModal(\'reference-viewer\', reference.id)"></button>',
						'<div class="apop-inline-ref-citation">',
						'<div class="apop-inline-ref-citation-wrapper">',
						'<div class="icon-container">',
						'<i class="icon ion-information-circled"></i>',
						'</div>',
						'<p class="ref-inline-short-citation" ng-bind-html="reference.ShortCitation"></p>',
						'</div>',
						'</div>',
						'</div>',
						'</sup>'
					].join('');

					return markup;

				} catch (e) {

					console.error(e);

					return '<div></div>';
				}

			}
		}

	});