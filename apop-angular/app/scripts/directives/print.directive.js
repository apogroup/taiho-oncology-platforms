'use strict';

//-------------------------------
// <sup> tag directive
//-------------------------------

angular.module('apopApp')
.directive('print', function($compile){
	return {
		restrict: 'AEC',
		replace: false,
		scope: true,
		priority: 1000,
		template: function(element, $scope) {
			
			var chapters = ["1A", "1B", "2A", "2B", "3", "4", "5", "6", "7"];

			var template;

			template =  '<h1 class="apop-print-title">Scientific Platform</h1>';
			// template += '<h2 class="apop-print-sub-title">All Statements and Substatements</h2>';
			template += '<div class="apop-print-chapter" ng-repeat="tls in cssprint.Chapters track by $index">';
			template += 	'<h4 class="apop-print-chapter-label">CHAPTER <span bind-html-compile="cssprint.sidebar[$index].number"></span>:</h4>';
			template += 	'<h3 class="apop-print-chapter-title" bind-html-compile="tls.Title"></h3>';
			template += 	'<div ng-repeat="statement in tls.Statements">';
			template +=			'<div class="apop-print-statement-container" ng-if="!statement.Title">';
			template += 			'<div class="apop-print-statement-content">';
			template +=					'<p>No statements in this chapter.</p>';
			template +=				'</div>';
			template +=			'</div>';
			template += 		'<div class="apop-print-statement-container" ng-if="statement.Title">';
			template += 			'<div class="apop-print-statement-header">';
			template += 				'<h4>Core Scientific Statement {{statement.href}}</h4>';
			template += 				'<p bind-html-compile="statement.Title"></p>';
			template += 			'</div>';
			template += 			'<div class="apop-print-statement-content">';
			template += 				'<div ng-repeat="substatement in statement.Substatements">';
			template +=						'<div class="accordion-content apop-print-evidence" ng-if="substatement.evidence">';
			// template += 						'<h4>Statement Evidence</h4>';
			template += 						'<div class="accordion-content-body apop-print-evidence-content" bind-html-compile="substatement.evidence"></div>';
			template += 					'</div>';
			template += 					'<div class="apop-print-references" ng-if="substatement.references">';
			template += 						'<h4>References</h4>';
			template += 						'<ul class="apop-print-references-list">';
			template += 							'<li class="apop-print-references-list-item" ng-repeat="UniqueID in substatement.references | orderBy: UniqueID">';
			template +=									'<p>{{UniqueID}}. </p>';
			template +=									'<p ng-repeat="reference in references | filter:{\'UniqueID\': UniqueID}:true" bind-html-compile="reference.ShortCitation"></p>';
			template +=								'</li>';
			template += 						'</ul>';
			template += 					'</div>';
			template += 				'</div>';
			template += 			'</div>';
			template += 		'</div>';
			template += 	'</div>';
			template += '</div>';

			return template;
		}
	}


});