'use strict';

//-------------------------------
// <sup> tag directive
//-------------------------------

angular.module('apopApp')
	.directive('figure', function ($rootScope) {
		return {
			restrict: 'A',
			replace: true,
			scope: false,
			template: function (element, attrs) {

				try {

					var id = attrs.figure;

					var markup = [
						'<div ng-repeat="figure in (figures | filter:{ id: \'' + id + '\'}) track by figure.id" class="apop-figure-container">',
						'<div class="apop-figure-image">',
						'<img ng-src="/content/figure/{{figure.id}}">',
						'</div>',
						'<button ng-click="openModal(\'sp-figure-viewer\', figure.id)" class="apop-figure-button">',
						'<h4>Click to view figure full size</h4>',
						'</button>',
						'</div>'
					].join('');

					return markup;

				} catch (e) {

					console.error(e);

					return '<div></div>';
				}

			}
		}

	});