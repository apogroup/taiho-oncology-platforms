'use strict';

/**
 * @ngdoc overview
 * @name apopApp
 * @description
 * # apopApp
 *
 * Main module of the application.
 */
angular
	.module('apopApp', [
		'ionic',
		'ncy-angular-breadcrumb',
		'vAccordion',
		'angular-bind-html-compile',
		'ngAnimate',
		'ngStorage',
	])
	.config(['$stateProvider', '$urlRouterProvider', '$breadcrumbProvider', 'LocalStorageServiceProvider', 'MetricServiceProvider', function ($stateProvider, $urlRouterProvider, $breadcrumbProvider, LocalStorageServiceProvider, MetricServiceProvider) {

		/////////////////////////////////
		// CONFIGURE STATES & ROUTES
		/////////////////////////////////

		$urlRouterProvider.otherwise('/');

		$stateProvider

			.state('home', {
				url: '/',
				views: {
					'dashboard': {
						templateUrl: 'views/dashboard.html',
						controller: function ($scope, $stateParams) {
							// !LocalStorageServiceProvider.$get().get('prompted_disclaimer') ? setTimeout(function () { $scope.openModal('confirm-dashboard'); }, 250) : console.log('Disclaimer Confirmed');
							MetricServiceProvider.$get().generateMetric({type: 'page-view'});
						},
						ncyBreadcrumb: {
							label: 'Home'
						}
					}
				},
				ncyBreadcrumb: {
					label: 'Home'
				}
			})
			.state('document', {
				url: '/document/:docid',
				templateUrl: 'views/dashboard.html',
				controller: function ($state, $scope, $stateParams) {
					$scope.openModal('document-viewer', $stateParams.docid);
					// This funtionality will bounce the user to the dashboard url if they navigate directly to a document, necessary to avoid breadcrumbing issues
					$state.go('home', {}, { location: 'replace' });
				},
				ncyBreadcrumb: {
					label: 'Home'
				}
			})
			.state('reference', {
				url: '/reference/:refid',
				templateUrl: 'views/dashboard.html',
				controller: function ($state, $scope, $stateParams) {
					$scope.openModal('reference-viewer', $stateParams.refid);
					// This funtionality will bounce the user to the dashboard url if they navigate directly to a reference, necessary to avoid breadcrumbing issues
					$state.go('home', {}, { location: 'replace' });
				},
				ncyBreadcrumb: {
					label: 'Home'
				}
			})
			.state('help', {
				url: '/help',
				templateUrl: 'views/dashboard.html',
				controller: function ($state, $scope, $stateParams) {
					$scope.openModal('ask-platform', $stateParams.refid);
					// This funtionality will bounce the user to the dashboard url if they navigate directly to a reference, necessary to avoid breadcrumbing issues
					$state.go('home', {}, { location: 'replace' });
				},
				ncyBreadcrumb: {
					label: 'Home'
				}
			})
			.state('core-scientific-statements', {
				url: '/core-scientific-statements',
				templateUrl: 'views/core-scientific-statements/core-scientific-statements.html',
				controller: function ($scope, $stateParams) {
					MetricServiceProvider.$get().generateMetric({type: 'page-view'});

					$scope.print = function (contentDiv) {
						$scope.printFlag = true;
						var printContents = "";

						printContents = document.getElementById(contentDiv).innerHTML;
						// var printContents = '<div print=""></div>';

						var printWindow = window.open("", "_blank");

						// Write to the document so the styles will be applied

						printWindow.document.write('<html><head><title>Core Scientific Statements</title><link rel=\"stylesheet\" type=\"text/css\" href=\"./styles/apop.print.css\" /></head><body><div class=\"apop-global-print\">' + printContents + '</div></body></html>');

						$scope.printFlag = false;

						// Set a timeout so that the page will load before we continue
						setTimeout(function () {
							printWindow.print();
						}, 500);

						// // Hook up the closing of the print window
						// // This has to be done after the window is visible
						// // If close is called directly after print, close will fire before print in some cases causing the print window to load with no data
						// printWindow.onfocus = function () {
						//     setTimeout(function() {
						//	printWindow.close();
						//     }, 500);
						// }
					}
				},
				ncyBreadcrumb: {
					label: 'Scientific and Value Narratives',
					parent: 'home'
				}
			})
			.state('top-level-statements', {
				url: '/core-scientific-statements/:chapter',
				templateUrl: function (stateParams) {
					// return 'views/core-scientific-statements/'+stateParams.cssid+'/top-level-statements.html';
					return 'views/core-scientific-statements/top-level-statements.html';

				},
				controller: function ($scope, $stateParams) {
					$scope.route = {};
					$scope.route.chapter = $stateParams.chapter;
					$scope.label_CSS_CHAPTER = function () {
						try {
							var chapter = _.find($scope.chapters, { id: $stateParams.chapter });
							return chapter.Title;
						} catch (e) {
							// console.error(e);
							return 'Top Level Statement';
						}
					};
					MetricServiceProvider.$get().generateMetric({ type: 'page-view' });
				},
				ncyBreadcrumb: {
					label: '{{label_CSS_CHAPTER()}}',
					parent: function ($scope) {
						return $scope.from || 'core-scientific-statements';
					}
				}
			})
			.state('statement', {
				url: '/core-scientific-statements/:chapter/:statement',
				templateUrl: function (stateParams) {
					return 'views/core-scientific-statements/statement-submessages.html';
				},
				controller: function ($scope, $stateParams) {

					$scope.route = {};
					$scope.route.chapter = $stateParams.chapter;
					$scope.route.statement = $stateParams.statement;

					$scope.label_CSS_CHAPTER = function () {
						try {
							var chapter = _.find($scope.chapters, { id: $stateParams.chapter });
							return chapter.Title;
						} catch (e) {
							// console.error(e);
							return 'Top Level Statement';
						}
					};

					$scope.label_CSS_STATEMENT = function () {
						try {
							var chapter = _.find($scope.chapters, { id: $stateParams.chapter });
							var statement = _.find(chapter.Children, { id: $stateParams.statement });
							return statement.Descriptor;
						} catch (e) {
							// console.error(e);
							return 'Substatements';
						}

					};

					MetricServiceProvider.$get().generateMetric({ type: 'page-view' });
				},
				ncyBreadcrumb: {
					label: '{{label_CSS_STATEMENT()}}',
					parent: function ($scope) {
						return $scope.from || 'top-level-statements';
					}
				}
			})
			.state('substatement', {
				url: '/core-scientific-statements/:chapter/:statement/:substatement',
				templateUrl: function (stateParams) {
					return 'views/core-scientific-statements/statement-submessages.html';
				},
				controller: function ($scope, $stateParams) {

					$scope.route = {};
					$scope.route.chapter = $stateParams.chapter;
					$scope.route.statement = $stateParams.statement;
					$scope.route.substatement = $stateParams.substatement;

					$scope.label_CSS_CHAPTER = function () {
						try {
							var chapter = _.find($scope.chapters, { id: $stateParams.chapter });
							return chapter.Title
						} catch (e) {
							// console.error(e);
							return 'Top Level Statement';
						}
					};

					$scope.label_CSS_STATEMENT = function () {
						try {
							var chapter = _.find($scope.chapters, { id: $stateParams.chapter });
							var statement = _.find(chapter.Children, { id: $stateParams.statement });
							return statement.Descriptor;
						} catch (e) {
							// console.error(e);
							return 'Substatements';
						}
					};

					MetricServiceProvider.$get().generateMetric({ type: 'page-view' });
				},
				ncyBreadcrumb: {
					label: '{{label_CSS_STATEMENT()}}',
					parent: function ($scope) {
						return $scope.from || 'top-level-statements';
					}
				}
			})

			.state('scientific-lexicon', {
				url: '/scientific-lexicon',
				templateUrl: 'views/scientific-lexicon/scientific-lexicon.html',
				controller: function ($state, $scope) {
					MetricServiceProvider.$get().generateMetric({type: 'page-view'});
				},
				ncyBreadcrumb: {
					label: 'Scientific Lexicon',
					parent: function ($scope) {
						return $scope.from || 'home';
					}
				}
			})

			.state('lexicon', {
				url: '/scientific-lexicon/lexicon',
				templateUrl: 'views/scientific-lexicon/lexicon.html',
				controller: function ($scope, $stateParams) {
					$scope.lexicon = _.find($scope.scientificlexicon.lexicons, { id: $stateParams.lexicon });
					$scope.label_LEXICON = function() {
						return $scope.lexicon.title || 'Lexicon';
					};
					//MetricServiceProvider.$get().generateMetric({type: 'page-view'});
				},
				ncyBreadcrumb: {
					label: 'Lexicon',
					parent: function ($scope) {
						return $scope.from || 'scientific-lexicon';
					}
				}
			})

			.state('abbreviations', {
				url: '/scientific-lexicon/abbreviations',
				templateUrl: 'views/scientific-lexicon/abbreviations.html',
				controller: function ($scope, $stateParams) {
					MetricServiceProvider.$get().generateMetric({type: 'page-view'});
				},
				ncyBreadcrumb: {
					label: 'Abbreviations',
					parent: function ($scope) {
						return $scope.from || 'scientific-lexicon';
					}
				}
			})

			.state('glossary', {
				url: '/scientific-lexicon/glossary',
				templateUrl: 'views/scientific-lexicon/glossary.html',
				controller: function ($scope, $stateParams) {
					$scope.glossary = _.find($scope.scientificlexicon.lexicons, { id: $stateParams.glossary });
					$scope.label_LEXICON = function() {
						return $scope.lexicon.title || 'Glossary';
					};
					//MetricServiceProvider.$get().generateMetric({type: 'page-view'});
				},
				ncyBreadcrumb: {
					label: 'Glossary',
					parent: function ($scope) {
						return $scope.from || 'scientific-lexicon';
					}
				}
			})

			.state('facts-and-evidence', {
				url: '/facts-and-evidence',
				templateUrl: 'views/facts-and-evidence/facts-and-evidence.html',
				controller: function ($scope) {
					MetricServiceProvider.$get().generateMetric({type: 'page-view'});
					$scope.clearTagFilters();
				},
				ncyBreadcrumb: {
					label: 'Facts and Evidence',
					parent: function ($scope) {
						return $scope.from || 'home';
					}
				}
			})
			.state('facts-and-evidence-tag', {
				url: '/facts-and-evidence/:tagid',
				templateUrl: 'views/facts-and-evidence/facts-and-evidence.html',
				controller: function ($scope, $stateParams) {
					$scope.selectTag($stateParams.tagid);
					MetricServiceProvider.$get().generateMetric({type: 'page-view'});
				},
				ncyBreadcrumb: {
					label: 'Facts and Evidence',
					parent: function ($scope) {
						return $scope.from || 'home';
					}
				}
			})

			.state('congress-activities', {
				url: '/congress-activities',
				templateUrl: 'views/congress-activities/congress-activities.html',
				controller: function(){
					MetricServiceProvider.$get().generateMetric('page-view', null, null, null, null, null, null, null, null, null, null, null, null);
				},
				ncyBreadcrumb: {
					label: 'Congress Activities',
					parent: function ($scope) {
						return $scope.from || 'home';
					}
				}
			})
			.state('congress-activities-scroll', {
				url: '/congress-activities/:scroll',
				templateUrl: 'views/congress-activities/congress-activities.html',
				controller: function(){
					MetricServiceProvider.$get().generateMetric('page-view', null, null, null, null, null, null, null, null, null, null, null, null);
				},
				onEnter: function($location, $stateParams, $anchorScroll, $timeout){
					$timeout(function() {
						$location.hash($stateParams.scroll);
						$anchorScroll()
					}, 100)
				},
				ncyBreadcrumb: {
					label: 'Congress Activities',
					parent: function ($scope) {
						return $scope.from || 'home';
					}
				}
			})
			.state('congress-activities-congress', {
				url: '/congress-activities/congress/:congress',
				templateUrl: 'views/congress-activities/congress.html',
				controller: function($scope, $stateParams) {

					var congress = parseInt($stateParams.congress);
					$scope.congress = _.find($scope.congresses.activities, { id: congress });
					$scope.label_CONGRESS = function() {
						return $scope.congress.event || 'Congress';
					};

					// $scope.visibleTab = 0;

					// $scope.toggle_visible = function(tab, tabs) {

					// 	$scope.tabs = tabs;

					// 	if($scope.tabs.length < 1) {
					// 		return;
					// 	}

					// 	if($scope.visibleTab == tab){
					// 		return;
					// 	}

					// 	else{
					// 		$scope.visibleTab = tab;

					// 		for (var i = $scope.tabs.length - 1; i >= 0; i--) {
					// 			angular.element(document.querySelector('tab' + i)).removeClass("visible")
					// 		}
					// 		angular.element(document.querySelector('tab' + tab)).addClass("visible")
					// 		return;
					// 	}
					// }


					MetricServiceProvider.$get().generateMetric('page-view', null, null, null, null, null, null, null, null, null, null, null, null);
				},
				ncyBreadcrumb: {
					label: '{{label_CONGRESS()}}',
					parent: function ($scope) {
						return $scope.from || 'congress-activities';
					}
				}
			})

			// .state('competitive-landscape', {
			// 	url: '/competitive-landscape',
			// 	templateUrl: 'views/competitive-landscape/competitive-landscape.html',
			// 	controller: function(){
			// 		MetricServiceProvider.$get().generateMetric('page-view', null, null, null, null, null, null, null, null);
			// 	},
			// 	ncyBreadcrumb: {
			// 		label: 'Competitive Landscape',
			// 		parent: function ($scope) {
			// 			return $scope.from || 'home';
			// 		}
			// 	}
			// })
			// .state('competitive-dashboard', {
			// 	url: '/competitive-landscape/competitive-dashboard',
			// 	templateUrl: 'views/competitive-landscape/competitive-dashboard.html',
			// 	controller: function($scope, $window){
			// 		MetricServiceProvider.$get().generateMetric('page-view', null, null, null, null, null, null, null, null);

			// 		$scope.getImgURL = function (image) {
			// 		  return '../../content/competitive-landscape/' + image;
			// 		};
										
			// 		$scope.truncate = function(string, length, delimiter) {

			// 			var width = $window.innerWidth;

			// 			if(string.length > length) {
			// 				if(width < 1600) {
			// 					length = length - 30;

			// 					return string.substring(0, length) + delimiter;
			// 				}
			// 				return string.substring(0, length) + delimiter;
			// 			}
			// 			else {
			// 				return string;
			// 			}
			// 		};

			// 	},
			// 	ncyBreadcrumb: {
			// 		label: 'Competitive Landscape Dashboard',
			// 		parent: function ($scope) {
			// 			return $scope.from || 'competitive-landscape';
			// 		}
			// 	}
			// })
			// .state('competitive-detail', {
			// 	url: '/competitive-landscape/competitive-dashboard/:drug',
			// 	templateUrl: 'views/competitive-landscape/competitive-detail.html',
			// 	controller: function($scope, $stateParams) {

			// 		$scope.visibleTab = 0;

			// 		$scope.getImgURL = function (image) {
			// 		  return '../../content/competitive-landscape/' + image;
			// 		};

			// 		$scope.toggle_visible = function(tab) {
			// 			if($scope.tabs.length < 1) {
			// 				return;
			// 			}

			// 			if($scope.visibleTab == tab) {
			// 				return;
			// 			}
			// 			else {
			// 				$scope.visibleTab = tab;

			// 				for (var i = $scope.tabs.length - 1; i >= 0; i--) {
			// 					angular.element(document.querySelector('tab' + i)).removeClass("visible")
			// 				}
			// 				angular.element(document.querySelector('tab' + tab)).addClass("visible")
			// 				return;
			// 			}
			// 		};

			// 		$scope.label_COMPOUND = "Competitive Detail";

			// 		$scope.$watch(function(scope) { return scope.competitivedashboard },
		    //           function() {
			// 			var dashboard = $scope.competitivedashboard;

			// 			for(var category in dashboard) {
			// 			    $scope.compound = _.find(dashboard[category], { "route": $stateParams.drug });

			// 			    if ($scope.compound) {
			// 			    	$scope.tabs = Object.keys($scope.compound.content.tier2);
			// 			    	$scope.label_COMPOUND = $scope.compound.name;
			// 			    	return;
			// 			   	 }
			// 			    }
		    //           });

			// 		MetricServiceProvider.$get().generateMetric('page-view', null, null, null, null, null, null, null, null);
			// 	},
			// 	ncyBreadcrumb: {
			// 		label: '{{label_COMPOUND}}',
			// 		parent: function ($scope) {
			// 			return $scope.from || 'competitive-dashboard';
			// 		}
			// 	}
			// })			

			.state('educational-materials', {
				url: '/educational-materials',
				templateUrl: 'views/educational-materials/educational-materials.html',
				controller: function(){
					MetricServiceProvider.$get().generateMetric('page-view', null, null, null, null, null, null, null, null, null, null, null, null);
				},
				ncyBreadcrumb: {
					label: 'Educational Materials',
					parent: function ($scope) {
						return $scope.from || 'home';
					}
				}
			})
			// .state('materials-page', {
			// 	url: '/educational-materials/:map',
			// 	templateUrl: 'views/educational-materials/material-pages.html',
			// 	controller: function($scope, $stateParams){
			// 		$scope.map = $stateParams.map;
			// 		$scope.label_EDU_RES = function(){
			// 			var page = _.find($scope.educationalmaterials.pages, { route: $stateParams.map });
			// 			return page.title || 'Materials';
			// 		};
			// 		MetricServiceProvider.$get().generateMetric('page-view', null, null, null, null, null, null, null, null, null, null, null, null);					
			// 	},
			// 	ncyBreadcrumb: {
			// 		label: '{{label_EDU_RES()}}',
			// 		parent: function ($scope) {
			// 			return $scope.from || 'educational-materials';
			// 		}
			// 	}
			// })						

			

			.state('medical-plan', {
				url: '/medical-plan',
				templateUrl: 'views/medical-plan/medical-plan.html',
				controller: function () {
					//MetricServiceProvider.$get().generateMetric({type: 'page-view'});
				},
				ncyBreadcrumb: {
					label: 'Medical Plan',
					parent: function ($scope) {
						return $scope.from || 'home';
					}
				}
			})

			.state('press-releases', {
				url: '/press-releases',
				templateUrl: 'views/press-releases/press-releases.html',
				controller: function () {
					//MetricServiceProvider.$get().generateMetric({type: 'page-view'});
				},
				ncyBreadcrumb: {
					label: 'Press Releases',
					parent: function ($scope) {
						return $scope.from || 'home';
					}
				}
			})

			.state('competitive-landscape', {
				url: '/competitive-landscape',
				templateUrl: 'views/competitive-landscape/competitive-landscape.html',
				controller: function () {
					//MetricServiceProvider.$get().generateMetric({type: 'page-view'});
				},
				ncyBreadcrumb: {
					label: 'Competitive Landscape',
					parent: function ($scope) {
						return $scope.from || 'home';
					}
				}
			})

			.state('platform-alerts', {
				url: '/platform-alerts',
				templateUrl: 'views/platform-alerts/platform-alerts.html',
				controller: function () {
					//MetricServiceProvider.$get().generateMetric({type: 'page-view'});
				},
				ncyBreadcrumb: {
					label: 'Alerts',
					parent: function ($scope) {
						return $scope.from || 'home';
					}
				}
			});


		/////////////////////////////////
		// CONFIGURE NCYBREADCRUMB
		/////////////////////////////////

		$breadcrumbProvider.setOptions({
			templateUrl: 'views/global/breadcrumb-template.html'
		});

	}]);
