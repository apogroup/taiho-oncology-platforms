'use strict';

//-------------------------------
// APP .RUN > Load data
//-------------------------------

angular.module('apopApp')
	.run(['$window', '$rootScope', '$http', 'LocalStorageService', 'ContentService', 'AlertService', 'AskPlatformService', 'PreloaderService', '$ionicScrollDelegate', function ($window, $rootScope, $http, LocalStorageService, ContentService, AlertService, AskPlatformService, PreloaderService, $ionicScrollDelegate) {

		/////////////////////////////////
		// GLOBAL HTTP HEADERS (no cache)
		/////////////////////////////////
		$http.defaults.headers.common['Cache-Control'] = 'no-cache';

		/////////////////////////////////
		// BIND SCOPE FUNCTIONS
		/////////////////////////////////

		//-------------------------------
		// Return browser to DOM, <body>
		// (bowser)
		//-------------------------------

		$rootScope.bowser = function () {
			if (bowser.msie) { return 'msie' }
			if (bowser.firefox) { return 'msie' }
			return;
		}

		//-------------------------------
		// Return browser to DOM, Metrics
		// (browser-detect)
		//-------------------------------

		$rootScope.browser = browserDetect();

		$rootScope.domain = window.location.origin;

		//-------------------------------
		// Return user to DOM, Metrics
		//-------------------------------

		$rootScope.user = LocalStorageService.get('user');
		$http.get('/user').success(function (response) {
			if (_.isEmpty(response)) {
				throw 'No user returned';
			}
			$rootScope.user = response;
			LocalStorageService.set('user', $rootScope.user);
		}).error(function () {
      // $window.alert('Your session expired. Please continue to login.');
      setTimeout(function() {
        $rootScope.$broadcast('session-invalidated');
      }, 1000);
		});

		//-------------------------------
		// Populate data from Local Storage
		//-------------------------------

		_.forEach([
			'platform-alerts',
			'surveys',
			'references',
			'documents',
			'tags',
			'subtags',
			'lexicon',
			'glossary',
			'abbreviations',
			'figures',
			'chapters',
			'buckets'
		], function (data) {
			if (_.isEmpty($rootScope[data])) {
				$rootScope[data] = LocalStorageService.get(data);
			}
		});

		//-------------------------------
		// Lexicon Term Component $scope
		// Management of tooltip toggles
		//-------------------------------

		$rootScope.popovers = Object.create(null);
		$rootScope.activePopoverId = null;

		function closePopover() {
			if (this.activePopoverId) {
				try {
					this.popovers[this.activePopoverId].$ctrl.tooltip.hide();
					$rootScope.$apply();
				} catch (error) {
					console.error("Couldn't hide active tooltip");
					console.error(error);
				}
			}
		}
		$rootScope.closePopover = closePopover.bind($rootScope);

		document.addEventListener("click", $rootScope.closePopover, false);
		document.addEventListener("scroll", $rootScope.closePopover, false);

		//-------------------------------
		// Determine online mode
		//-------------------------------

		$rootScope.online = navigator.onLine;
		$window.addEventListener("offline", function () {
			$rootScope.$apply(function () {
				$rootScope.online = false;
			});
		}, false);

		$window.addEventListener("online", function () {
			$rootScope.$apply(function () {
				$rootScope.online = true;
			});
		}, false);

		//-------------------------------
		// Preload manifest, documents, & API data
		//-------------------------------
		$rootScope.preload = PreloaderService;

		// console.log($rootScope);

		///////////////////////////////////////////////
		// LOAD DATA FROM DATABASE & BIND TO SCOPE
		///////////////////////////////////////////////

		// //-------------------------------
		// // Platform Alerts (alerts)
		// //-------------------------------

		$rootScope.alerts = [];
		AlertService.getPlatformAlerts()
			.then(function(data) { 
				$rootScope.alerts = data;
				LocalStorageService.set('platform-alerts', data);
				// console.log("Database Alerts", data);
				// LocalStorageService.set('alerts', data);
				return;
			})
			.catch(function(e){
				$rootScope.alerts = [];
				$rootScope.alerts = LocalStorageService.get('platform-alerts');
				// console.log("Alerts: FAILED", $rootScope.alerts);
				return;
			});


		//-------------------------------
		// Submitted Surveys
		//-------------------------------

		$rootScope.surveys = [];
		AskPlatformService.getSubmittedSurveys()
			.then(function(data) { 
				$rootScope.surveys = data;
				LocalStorageService.set('surveys', data);
				// console.log("Database Surveys", data);
				// LocalStorageService.set('surveys', data);
				return;
			})
			.catch(function(e){
				$rootScope.surveys = [];
				$rootScope.surveys = LocalStorageService.get('surveys');;
				// console.log("Surveys: FAILED", $rootScope.surveys);
				return;
			});
	

		//-------------------------------
		// References Local Database
		//-------------------------------

		$rootScope.references = [];
		$rootScope.references_db = [];
		ContentService.getLocalizedContent('_references')
			.then(function(data){
				$rootScope.references = data;
				// console.log("JSON References", $rootScope.references);
				// console.table($rootScope.references);


				// Reference Index

				ContentService.getLocalizedContent('_references_index')
					.then(function(data){
						$rootScope.references_index = data;
						// console.log("JSON References Index Data", data);
						// console.log("JSON References Index", $rootScope.references_index);
						$rootScope.references = _($rootScope.references).concat($rootScope.references_index).groupBy('id').map(_.spread(_.assign)).value();
						// console.log("Paired References-Indexes", $rootScope.references);

						ContentService.getReferences()
							.then(function(data) { 
								$rootScope.references_db = data;
								// console.log("Database References", data);
								$rootScope.references = _($rootScope.references).concat($rootScope.references_db).groupBy('id').map(_.spread(_.assign)).value();
								// console.log("Paired JSON-DB References", $rootScope.references);
								return;
							})
							.catch(function(e){
								// console.log(e);
								return;
							});

						// return;
					})
					.catch(function(e){
						$rootScope.references_index = [];
						// console.log("References Index: FAILED", $rootScope.references_index);
						return;
					});

				//// Skip Ref Index
				ContentService.getReferences()
					.then(function(data) { 
						$rootScope.references_db = data;
						// LocalStorageService.set('references_db', data);
						// console.log("Database References", data);
						$rootScope.references = _($rootScope.references).concat($rootScope.references_db).groupBy('id').map(_.spread(_.assign)).value();
						// console.log("Paired JSON-DB References", $rootScope.references);
						return;
					})
					.catch(function(e){
						// $rootScope.references_db = LocalStorageService.get('references_db');
						$rootScope.references_db = [];
						$rootScope.references = _($rootScope.references).concat($rootScope.references_db).groupBy('id').map(_.spread(_.assign)).value();
						// console.log(e);
						return;
					});

				// return;
			})
			.catch(function(e){
				$rootScope.references = [];
				// console.log("References: FAILED", $rootScope.references);
				return;
			});


		///////////////////////////////////////////////
		// LOAD DATA FROM LOCAL JSON & BIND TO SCOPE
		///////////////////////////////////////////////

		//-------------------------------
		// CORE. Buckets
		//-------------------------------

		$rootScope.buckets = [];
		$rootScope.buckets = ContentService.getLocalizedContent('_buckets')
			.then(function(data){
				$rootScope.buckets = data;
				console.log("JSON Documents", $rootScope.buckets);
				return;
			})
			.catch(function(e){
				$rootScope.buckets = [];
				// console.log("Failed to retrieve any buckets", $rootScope.buckets);
				return;
			});

		//-------------------------------
		// CORE. Documents
		//-------------------------------

		$rootScope.documents = [];
		$rootScope.documents = ContentService.getLocalizedContent('_documents')
			.then(function(data){
				$rootScope.documents = data;
				// console.log("JSON Documents", $rootScope.documents);

				// Document Index

				ContentService.getLocalizedContent('_documents_index')
					.then(function(data){
						$rootScope.documents_index = data;
						// console.log("JSON Documents Index Data", data);
						// console.log("JSON Documents Index", $rootScope.documents_index);
						$rootScope.documents = _($rootScope.documents).concat($rootScope.documents_index).groupBy('id').map(_.spread(_.assign)).value();
						// console.log("Paired Documents-Indexes", $rootScope.documents);

						// ContentService.getDocuments()
						// 	.then(function(data) { 
						// 		$rootScope.documents_db = data;
						// 		// console.log("Database Documents", data);
						// 		$rootScope.documents = _($rootScope.documents).concat($rootScope.documents_db).groupBy('id').map(_.spread(_.assign)).value();
						// 		// console.log("Paired JSON-DB Documents", $rootScope.documents);
						// 		return;
						// 	})
						// 	.catch(function(e){
						// 		// console.log(e);
						// 		return;
						// 	});

						// return;
					})
					.catch(function(e){
						$rootScope.documents_index = [];
						// console.log("Documents Index: FAILED", $rootScope.documents_index);
						return;
					});


				// return;
			})
			.catch(function(e){
				$rootScope.documents = [];
				// console.log("Failed to retrieve any documents", $rootScope.documents);
				return;
			});

		//-------------------------------
		// CORE. Tags & Subtags
		//-------------------------------

		$rootScope.tags = [];
		ContentService.getLocalizedContent('_tags')
			.then(function(data){
				$rootScope.tags = data;
				// console.log("JSON Tags", $rootScope.tags);
				return;
			})
			.catch(function(e){
				$rootScope.tags = [];
				// console.log("Failed to retrieve any tags", $rootScope.tags);
				return;
			});

		$rootScope.subtags = [];
		ContentService.getLocalizedContent('_subtags')
			.then(function(data){
				$rootScope.subtags = data;
				// console.log("JSON Subtags", $rootScope.subtags);				
				return;
			})
			.catch(function(e){
				$rootScope.subtags = [];
				// console.log("Failed to retrieve any subtags", $rootScope.subtags);
				return;
			});


		//-------------------------------
		// 0. Global Localization
		//-------------------------------

		$rootScope.globallocal = [];
		ContentService.getLocalizedContent('_global')
			.then(function(data) { 
				$rootScope.globallocal = data;
				// console.log("Core Scientific Statements", $rootScope.css);
				return;
			})
			.catch(function(e){
				$rootScope.globallocal = [];
				// console.log("Failed to retrieve any subtags", $rootScope.css);
				return;
			});

		//-------------------------------
		// 3. Facts and Evidence
		//-------------------------------

		$rootScope.factsevidence = [];
		ContentService.getLocalizedContent('_facts_and_evidence')
			.then(function(data) { 
				$rootScope.factsevidence = data;
				// console.log("F&E", $rootScope.factsevidence);
				return;
			})
			.catch(function(e){
				$rootScope.factsevidence = [];
				// console.log("Failed to retrieve any lexicons", $rootScope.factsevidence);
				return;
			});


		//-------------------------------
		// 4. Congress Activites
		//-------------------------------

		$rootScope.congresses = [];
		$rootScope.activities = [];
		ContentService.getLocalizedContent('_congress_activities')
			.then(function(data) { 
				$rootScope.congresses = data;
				$rootScope.activities = $rootScope.congresses.activities;
				// console.log("Congress Activities", $rootScope.congresses);
				return;
			})
			.catch(function(e){
				$rootScope.congresses = [];
				// console.log("Failed to retrieve any subtags", $rootScope.congresses);
				return;
			});

		//-------------------------------
		// 6. Educational Materials
		//-------------------------------

		$rootScope.educationalmaterials = [];
		ContentService.getLocalizedContent('_educational_materials')
			.then(function(data) { 
				$rootScope.educationalmaterials = data;
				$rootScope.educationalmaterials.resourcesLength = 0
				// for (var i = 0; i < $rootScope.educationalmaterials.categories.length; i++) {
				// 	$rootScope.educationalmaterials.resourcesLength += $rootScope.educationalmaterials.categories[i].resources.length;
				// 	// console.log("Length", $rootScope.educationalmaterials.resourcesLength);

				// }
				// console.log("educational Resources", $rootScope.educationalmaterials);
				return;
			})
			.catch(function(e){
				$rootScope.educationalmaterials = [];
				// console.log("Failed to retrieve any subtags", $rootScope.congresses);
				return;
			});

		//-------------------------------
		// 7. Platform Alerts
		//-------------------------------

		$rootScope.platformalerts = [];
		ContentService.getLocalizedContent('_platform_alerts')
			.then(function(data) { 
				$rootScope.platformalerts = data;
				// console.log("Congress Activities", $rootScope.congresses);
				return;
			})
			.catch(function(e){
				$rootScope.platformalerts = [];
				// console.log("Failed to retrieve any subtags", $rootScope.congresses);
				return;
			});

		//-------------------------------
		// 7. Ask Platform (FAQ)
		//-------------------------------

		$rootScope.askplatform = [];
		ContentService.getLocalizedContent('_ask_platform')
			.then(function(data) { 
				$rootScope.askplatform = data;
				// console.log("Congress Activities", $rootScope.congresses);
				return;
			})
			.catch(function(e){
				$rootScope.askplatform = [];
				// console.log("Failed to retrieve any subtags", $rootScope.congresses);
				return;
			});




	}]);
