'use strict';

/**
 * @ngdoc function
 * @name apopApp.controller:PreloaderCtrl
 * @description
 * # PreloaderCtrl
 * Controller of the apopApp
 */
angular.module('apopApp')
  .component('lexiconTerm', {
    template: '<span class="apop-lexicon-term" ng-mouseenter="$ctrl.tooltip.show($event)" ng-mouseleave="$ctrl.tooltip.hide($event)"><ng-transclude></ng-transclude></span>',
    bindings: {
      id: '<',
    },
    transclude: true,
    controller: ['$element', '$scope', '$rootScope', '$timeout', function ($element, $scope, $rootScope, $timeout) {

      var self = this;

      // Have to use scope because hash clash with two components with the same term?
      $rootScope.popovers[$scope.$id] = $scope;

      /***************************************************************************
       * Lifecycle
       ***************************************************************************/
      this.$onInit = function () {
        // console.log(`Initializing: ${$scope.$id}`);
        $timeout(function () {
          $element.ready(function () {
            $scope.$apply(function () {
              try {
                var element = $element[0];
                var reference = element.querySelector('.apop-lexicon-term');
                var popover = document.querySelector(['.apop-tooltip[data-popover-id="', $scope.$id, '"]'].join(''));

                reference.addEventListener('touchend', self.tooltip.toggle.bind(self.tooltip));

                self.popover = new Popper(reference, popover, {
                  placement: 'auto-end',
                  eventsEnabled: false,
                  modifiers: {
                    computeStyle: {
                      gpuAcceleration: false
                    },
                    offset: {
                      enabled: true,
                      offset: '200, 0',
                    },
                  },
                  onUpdate: function (data) {
                    if (self.tooltip.isShown && data.hide) {
                      self.tooltip.hide();
                      $scope.$apply();
                    }
                  }
                });

              } catch (error) {
                console.error('Failed to create live lexicon popover');
                console.error(error);
              }

              self.$onDestroy = function () {
                // console.log(`Destroying: ${$scope.$id}`);
                if ($scope.$id in $rootScope.popovers) {
                  delete $rootScope.popovers[$scope.$id];
                }
                reference.removeEventListener('touchend', self.tooltip.toggle.bind(self.tooltip));
                self.popover.destroy();
              }

            });
          });
        });

      }

      this.tooltip = Object.defineProperties({
        /***************************************************************************
         * State
         ***************************************************************************/
        state: 'hidden',

        /***************************************************************************
         * Actions
         ***************************************************************************/
        hide: function () {
          if (!self.popover) return;

          $rootScope.activePopoverId = null;

          this.state = 'hidden';
          self.popover.disableEventListeners();

        },

        show: function ($event) {
          if (!self.popover) return;

          self.popover.scheduleUpdate();

          if ($event) {
            $event.preventDefault();
            $event.stopPropagation();
            switch ($event.type) {
              case 'touchend':
                this.state = 'active';
                break;
              case 'mouseover':
                this.state = 'visible';
                break;
              default:
                break;
            }
          }

          try {
            if ($rootScope.activePopoverId && $scope.$id !== $rootScope.activePopoverId) {
              $rootScope.popovers[$rootScope.activePopoverId].$ctrl.tooltip.hide();
            }
          } catch (error) {
            console.error('Could not hide sibling tooltip');
            console.error(error);
          }

          $rootScope.activePopoverId = $scope.$id;

          self.popover.enableEventListeners();

        },

        toggle: function ($event) {
          switch (this.state) {
            case 'hidden':
              this.show($event);
              break;
            case 'visible':
            case 'active':
              this.hide();
              break;
            default:
              break;
          }

          $scope.$apply();

        }
      }, {
        /***************************************************************************
         * Getters
         ***************************************************************************/
        isShown: {
          get: function () {
            return _.includes(['visible', 'active'], this.state);
          }
        },

      });

      /***************************************************************************
       * Properties
       ***************************************************************************/
      this.term = _.find($rootScope.abbreviations, { id: this.id });

      // this.applyStyle = function (data, options) {
      //   this.tooltip.styles = data.styles;
      // };

    }]
  });