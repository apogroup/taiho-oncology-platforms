'use strict';

/**
 * @ngdoc function
 * @name apopApp.service:HttpRequestScheduleService
 * @description
 * # HttpRequestScheduleService
 * Service of the apopApp
 */
angular.module('apopApp')
	.service('HttpRequestScheduleService', HttpRequestScheduleService);

	HttpRequestScheduleService.$inject = ['$rootScope', 'LocalStorageService'];

	function HttpRequestScheduleService($rootScope, LocalStorageService) {

		var service = {
			schedule: schedule,
			processRequests: processRequests
		}

		return service;


		function schedule(httpRequest) {

			var httpRequestQueue = LocalStorageService.get('httpRequestQueue');

			console.log(httpRequest);

			httpRequestQueue.push(httpRequest);

			console.log(httpRequestQueue);

			LocalStorageService.set('httpRequestQueue', httpRequestQueue);

		}

		function processRequests() {

			var httpRequestQueue = LocalStorageService.get('httpRequestQueue');

			httpRequestQueue.map(function(item, index) {

				var currentRequest = httpRequestQueue.splice(0, 1)[0];

				$http(currentRequest)
					.success(function () {

					})
					.error(function () {
						httpRequestQueue.push(currentRequest);
					});

			});

		}

	}