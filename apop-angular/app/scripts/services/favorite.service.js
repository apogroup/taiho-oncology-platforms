'use strict';

/**
 * @ngdoc function
 * @name apopApp.service:FavoriteService
 * @description
 * # FavoriteService
 * Service of the apopApp
 */
angular.module('apopApp')
	.service('FavoriteService', FavoriteService);

	FavoriteService.$inject = ['$http', '$q'];

	function FavoriteService($http, $q) {

		var service = {
			getFavorites: getFavorites,
			saveFavorite: saveFavorite,
			archiveFavorite: archiveFavorite
		}

		return service;

		function getFavorites() {
			var defer = $q.defer();
			$http.get('/favorites')
				.success(function(response){
					defer.resolve(response);
				})
				.error( function(err) {
					defer.reject(err);
				});
			return defer.promise;
		}

		function saveFavorite(favorite) {
			var defer = $q.defer();
			$http.post('/favorite/save', favorite)
				.success(function(response){
					// console.log(response);
					defer.resolve(response);
				})
				.error( function(err) {
					defer.reject(err);
					console.log($q);
				});
			return defer.promise;
		}

		function archiveFavorite(favorite) {
			var defer = $q.defer();
			$http.post('/favorite/archive', favorite)
				.success(function(response){
					// console.log(response);
					defer.resolve(response);
				})
				.error( function(err) {
					defer.reject(err);
				});
			return defer.promise;
		}

	}
