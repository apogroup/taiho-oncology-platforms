'use strict';

/**
 * @ngdoc function
 * @name apopApp.service:PreloaderService
 * @description
 * # PreloaderService
 * Service of the apopApp
 */
angular.module('apopApp')
	.service('PreloaderService', PreloaderService);

PreloaderService.$inject = ['PreloaderFactory'];

function PreloaderService(PreloaderFactory) {

	this.documents = PreloaderFactory.create('documents');
	// this.content = PreloaderFactory.create('content');

	Object.defineProperties(this, {
		// manifestForce: {
		// 	get: function () {
		// 		var willForce;
		// 		try {
		// 			willForce = LocalStorageService.wget('forcedCachePreload');
		// 			willForce = _.defaultTo(willForce, 'true');
		// 			willForce = JSON.parse(willForce);
		// 		} catch (e) {
		// 			console.error(e);
		// 			willForce = true;
		// 		}

		// 		return willForce;
		// 	},
		// 	set: function (force) {
		// 		try {
		// 			if (!_.isBoolean(force)) {
		// 				throw new Error('Value must be a Boolean');
		// 			}
		// 			return LocalStorageService.wset('forcedCachePreload', false);
		// 		} catch (e) {
		// 			console.error(e);
		// 			return false;
		// 		}
		// 	}
		// },
		preloader: {
			get: function () {

				// if (this.manifestForce || this.documents.force || !this.documents.loaded) {
				if (this.documents.force || !this.documents.loaded) {
					// console.log('Loading documents');
					return 'documents';
				}

				// if (this.content.force || !this.content.loaded) {
				// 	// console.log('Loading content');
				// 	return 'content';
				// }
			}
		}
	});

}
