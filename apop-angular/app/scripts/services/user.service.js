'use strict';

/**
 * @ngdoc function
 * @name apopApp.service:UserService
 * @description
 * # UserService
 * Service of the apopApp
 */
angular.module('apopApp')
	.service('UserService', UserService);

	UserService.$inject = ['$http', '$q'];

	function UserService($http, $q) {

		var service = {
			getUser: getUser,
		}

		return service;

		function getUser() {
			var defer = $q.defer();
			$http.get('/user')
				.success(function(response){
					defer.resolve(response);
				})
				.error( function(err) {
					defer.reject(err);
				});
			return defer.promise;
		}



	}
