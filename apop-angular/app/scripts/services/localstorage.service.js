'use strict';

/**
 * @ngdoc function
 * @name apopApp.service:LocalStorageService
 * @description
 * # LocalStorageService
 * Service of the apopApp
 */
angular.module('apopApp')
	.service('LocalStorageService', LocalStorageService);

	LocalStorageService.$inject = ['$rootScope', '$localStorage', '$window'];

	function LocalStorageService($rootScope, $localStorage, $window) {

		var service = {
			set: set,
			get: get,
			wget: wget,
			wset: wset
		}

		return service;

		function set(key, value) {
			$localStorage[key] = value;
		}

		function get(key) {
			if(!$localStorage[key]) return null;
			return $localStorage[key];
		}

		function wget(key) {
			return $window.localStorage[key];
		}

		function wset(key, value) {
			return $window.localStorage[key] = value;
		}

	}