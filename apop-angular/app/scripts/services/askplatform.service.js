'use strict';

/**
 * @ngdoc function
 * @name apopApp.service:AskPlatformService
 * @description
 * # AskPlatformService
 * Service of the apopApp
 */
angular.module('apopApp')
	.service('AskPlatformService', AskPlatformService);

	AskPlatformService.$inject = ['$http', '$q'];

	function AskPlatformService($http, $q) {

		var service = {
			saveComment: saveComment,
			saveSurvey: saveSurvey,
			getSubmittedSurveys: getSubmittedSurveys
		}

		return service;

		function saveComment(comment) {
			console.log(comment);
			var defer = $q.defer();
			$http.post('/askplatform/comment/save', comment)
				.success(function(response){
					// console.log(response);
					defer.resolve(response);
				})
				.error( function(err) {
					defer.reject(err);
				});
			return defer.promise;
		}

		function getSubmittedSurveys() {
			var defer = $q.defer();
			$http.get('/askplatform/survey')
				.success(function(response){
					defer.resolve(response);
				})
				.error( function(err) {
					defer.reject(err);
				});
			return defer.promise;
		}

		function saveSurvey(survey) {
			var defer = $q.defer();
			$http.post('/askplatform/survey/save', survey)
				.success(function(response){
					// console.log(response);
					defer.resolve(response);
				})
				.error( function(err) {
					defer.reject(err);
				});
			return defer.promise;
		}

	}
