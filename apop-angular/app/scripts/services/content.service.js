'use strict';

/**
 * @ngdoc function
 * @name apopApp.service:ContentService
 * @description
 * # ContentService
 * Service of the apopApp
 */
angular.module('apopApp')
	.service('ContentService', ContentService);

	ContentService.$inject = ['$http', '$q'];

	function ContentService($http, $q) {

		var service = {
			getReferences: getReferences,
			getPlatformAlerts: getPlatformAlerts,
			getLocalizedContent: getLocalizedContent,
			getCMSContent: getCMSContent
		}

		return service;

		function getReferences() {
			var defer = $q.defer();
			$http.get('/references', { headers: { 
				'Cache-Control': 'no-cache',
				'Pragma': 'no-cache',
				'Expires': 'Sat, 01 Jan 2000 00:00:00 GMT'
			} })
				.success(function(response){
					defer.resolve(response);
				})
				.error( function(err) {
					defer.reject(err);
				});
			return defer.promise;
		}

		function getPlatformAlerts() {
			var defer = $q.defer();
			$http.get('/platform-alerts')
				.success(function(response){
					defer.resolve(response);
				})
				.error( function(err) {
					defer.reject(err);
				});
			return defer.promise;
		}

		function getPlatformAlertsStatus() {
			var defer = $q.defer();
			$http.get('/platform-alerts-status')
				.success(function(response){
					defer.resolve(response);
				})
				.error( function(err) {
					defer.reject(err);
				});
			return defer.promise;
		}

		function getLocalizedContent(contentType) {
			var defer = $q.defer();
			$http.get('content/'+contentType+'.json')
				.success(function(response) {
					defer.resolve(response);
				})
				.error( function(err) {
					defer.reject(err);
				});
			return defer.promise;
		}

		function getCMSContent(url) {
			var defer = $q.defer();
			$http.get('/cms/' + url)
				.success(function (response) {
					defer.resolve(response);
				})
				.error(function (err) {
					defer.reject(err);
				});
			
			return defer.promise;
		}

	}
