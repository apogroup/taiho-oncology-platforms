'use strict';

/**
 * @ngdoc function
 * @name apopApp.service:MetricService
 * @description
 * # MetricService
 * Service of the apopApp
 */

angular.module('apopApp')
	.service('MetricService', MetricService);

	MetricService.$inject = ['$rootScope', '$location', '$http', '$q', '$window', 'HttpRequestScheduleService'];

	function MetricService($rootScope, $location, $http, $q, $window, HttpRequestScheduleService) {

		var service = {
			generateMetric: generateMetric,
			saveMetric: saveMetric,
		}

		return service;

		function generateMetric(data) {
			var browser = $rootScope.browser; // { name: 'node', version: '9.9.0', versionNumber: 9.9, mobile: false, os: 'win32' }
				browser = 'Name: ' + browser.name + ', Version: ' + browser.version + ', Mobile: ' + browser.mobile + ', OS: ' + browser.os;

			var metric = {
				userclient: $rootScope.user.username,
				timestamp: moment().format("YYYY-MM-DD HH:mm:ss"),
				version: '0.0.0',
        useragent: browser,
        page: data.page || $location.$$url || null,
        type: data.type || null,
        duration: data.duration || null,
				category: data.category || null,
				reference: data.reference || null,
				document: data.doc || null,
				resource: data.resource || null,
				tag: data.tag || null,
				table: data.table || null,
				figure: data.figure || null,
				search: data.search || null,
				submessage: data.substatement || null
			};
			// console.log(metric);
			saveMetric(metric);
		}

		function saveMetric(metric) {
			var defer = $q.defer();
			$http.post('/metric/save', metric)
				.success(function(response){
					// console.log(response);
					defer.resolve(response);
				})
				.error( function(err) {
					defer.reject(err);
          // console.log($q);
          // $window.alert('Your session expired. Please continue to login.');
          $rootScope.$broadcast('session-invalidated');
				});
			return defer.promise;
		}

	}
