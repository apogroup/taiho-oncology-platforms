'use strict';

/**
 * @ngdoc function
 * @name apopApp.service:AlertService
 * @description
 * # AlertService
 * Service of the apopApp
 */
angular.module('apopApp')
	.service('AlertService', AlertService);

	AlertService.$inject = ['$http', '$q'];

	function AlertService($http, $q) {

		var service = {
			getPlatformAlerts: getPlatformAlerts,
			markReadAlertStatus: markReadAlertStatus,
			markAlertedAlertStatus: markAlertedAlertStatus
		}

		return service;

		function getPlatformAlerts() {
			var defer = $q.defer();
			$http.get('/platform-alerts')
				.success(function(response){
					defer.resolve(response);
				})
				.error( function(err) {
					defer.reject(err);
				});
			return defer.promise;
		}

		function markReadAlertStatus(alert) {
			var alert = {
				alert: alert.id
			};
			var defer = $q.defer();
			$http.post('/platform-alerts-status/read', alert)
				.success(function(response){
					// console.log(response);
					defer.resolve(response);
				})
				.error( function(err) {
					defer.reject(err);
				});
			return defer.promise;
		}

		function markAlertedAlertStatus(alert) {
			var alert = {
				alert: alert.id
			};
			var defer = $q.defer();
			$http.post('/platform-alerts-status/alerted', alert)
				.success(function(response){
					// console.log(response);
					defer.resolve(response);
				})
				.error( function(err) {
					defer.reject(err);
				});
			return defer.promise;
		}

	}
