'use strict';

/**
 * @ngdoc function
 * @name apopApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the apopApp
 */
angular.module('apopApp')
	.controller('MainCtrl', MainController);

MainController.$inject = ['$filter', '$scope', '$state', '$ionicScrollDelegate', 'AlertFactory', 'AskPlatformFactory', 'FavoriteFactory', 'ModalFactory', 'SearchFactory', 'TagFactory', 'ScrollFactory', 'MetricService', 'ManifestFactory', 'CacheFactory', 'LocalStorageService'];

function MainController($filter, $scope, $state, $ionicScrollDelegate, AlertFactory, AskPlatformFactory, FavoriteFactory, ModalFactory, SearchFactory, TagFactory, ScrollFactory, MetricService, ManifestFactory, CacheFactory, LocalStorageService) {

	///////////////////////////////////////////////
	// LOAD DATA FROM DATABASE & BIND TO SCOPE
	///////////////////////////////////////////////

	//-------------------------------
	// Bind to $scope
	//-------------------------------

	$scope.$state = $state;
	$scope.stateLabel;
	$scope.$on('$stateChangeSuccess', function () {
		setTimeout(function () {
			$scope.stateLabel = $state.current.ncyBreadcrumbLabel;

			if ($ionicScrollDelegate.$getByHandle('mainScroll')) { $ionicScrollDelegate.$getByHandle('mainScroll').scrollTop(); }
		}, 100);
	});

	$scope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams, options) {
		if (fromState.name != "") {
			$scope.page_end = moment();
			$scope.page_duration = moment.utc($scope.page_end.diff($scope.page_start)).format("HH:mm:ss");
			MetricService.generateMetric({ type: 'page-view-exit', duration: $scope.page_duration, page: fromState.ncyBreadcrumbLink });
		}

		$scope.page_start = moment.now();
		$scope.page_end = null;
		$scope.page_duration = null;

		var scrollContainer = document.getElementById('ion-scroll-element');
		scrollContainer.scrollTop = 0;
	});

	$scope.stateof = {};
	$scope.stateof.refPDF = 'clean';

	$scope.search = {};
	$scope.search.query = "";

	$scope.alphabet = {};
	$scope.alphabet.character = "ALL";

	$scope.prompted_disclaimer;

	$scope.user_guide = "5d40685826251512ac0bf035";


	//-------------------------------
	// Build Modals
	//-------------------------------

	$scope.modals = [];

	ModalFactory.build($scope, 'ask-platform');
	ModalFactory.build($scope, 'confirm-dashboard');
	ModalFactory.build($scope, 'favorites');
	ModalFactory.build($scope, 'document-viewer');
	ModalFactory.build($scope, 'reference-viewer');
	ModalFactory.build($scope, 'share-reference', 'message');
	ModalFactory.build($scope, 'share-document', 'message');
	ModalFactory.build($scope, 'sp-figure-viewer');
	ModalFactory.build($scope, 'sp-table-viewer');
	ModalFactory.build($scope, 'platform-alerts');
	ModalFactory.build($scope, 'platform-survey');
	ModalFactory.build($scope, 'manifest-update');
	ModalFactory.build($scope, 'manifest-forced');
	ModalFactory.build($scope, 'content-cache-update');
	ModalFactory.build($scope, 'session-expired');

	//-------------------------------
	// Date / Modeling
	//-------------------------------

	$scope.today = new Date();
	$scope.createDate = function (d) {
		return new Date(d);
	}

	// ** Break out controller fx in services into factories

	//-------------------------------
	// Ref/Tag Sorting
	//-------------------------------

	TagFactory.init($scope);

	//-------------------------------
	// Ref/Tag Sorting
	//-------------------------------

	FavoriteFactory.init($scope);

	//-------------------------------
	// Search
	//-------------------------------

	SearchFactory.init($scope);


	//-------------------------------
	// Ask Platform
	//-------------------------------

	AskPlatformFactory.init($scope);
	$scope.askplatform_responses = {};
	setTimeout(function () { $scope.promptSurvey(); }, 1000);

	//-------------------------------
	// Alerts Status
	//-------------------------------

	AlertFactory.init($scope);
	setTimeout(function () { $scope.promptAlerts(); }, 1000);

	//-------------------------------
	// Manifest Update
	//-------------------------------

	ManifestFactory.init($scope);

	//-------------------------------
	// Content Cache Update
	//-------------------------------

	CacheFactory.init($scope);

	//-------------------------------
	// Scroll
	//-------------------------------

	ScrollFactory.init($scope);

	//-------------------------------
	// Ionic Scroll
	//-------------------------------

	$scope.scrollMainToTop = function () {
		$ionicScrollDelegate.$getByHandle('mainScroll').scrollTop();
	};

	$scope.scrollMainResize = function () {
		setTimeout(function () {
			$ionicScrollDelegate.$getByHandle('mainScroll').resize();
		}, 500);
	};

	$scope.scrollSearchToTop = function () {
		$ionicScrollDelegate.$getByHandle('searchScroll').scrollTop();
	};

	$scope.scrollSearchResize = function () {
		setTimeout(function () { $ionicScrollDelegate.$getByHandle('searchScroll').resize(); }, 500);
	};

	$scope.scrollLinksFavsToTop = function () {
		$ionicScrollDelegate.$getByHandle('linksScroll').scrollTop();
		$ionicScrollDelegate.$getByHandle('favsScroll').scrollTop();
	};

	$scope.scrollFavsResize = function () {
		$ionicScrollDelegate.$getByHandle('favsScroll').resize();
	}

	//-------------------------------
	// Submessage
	//-------------------------------

	$scope.substatementSelected = function (substatement) {
		MetricService.generateMetric({ type: 'substatement-view', substatement: substatement});
	}

	//-------------------------------
	// CSS Export
	//-------------------------------

	$scope.printStatementsView = function (document) {
		//MetricService.generateMetric({ type: 'print-statements-view', doc: document });
	}

	//-------------------------------
	// Core Resource
	//-------------------------------

	$scope.coreResourceSelected = function (category, resource) {
		//MetricService.generateMetric({ type: 'core-resource-view', category: category, resource: resource });
  }

  	$scope.promptDisclaimer = function (id){
		$scope.openModal('confirm-dashboard', id);	  
	}

	//------------------------------------------
	// Competitive Landscape -- Latest Material
	//------------------------------------------

	$scope.isLatestDocument = function(documents, id){
		// find latest documents -- within the past year
		// var d = new Date(new Date().getFullYear(), 0, 1);
		// var sortedDocuments = documents.filter(function (document) {
		// 	var date = new Date(document.publication_date).getTime();
		// 	return date > d;
		// }).sort();
		// var latestDocument = _.find(sortedDocuments, { id: id }) || null; 

		// find the latest document
		var sortedDocuments = $filter('orderBy')(documents, 'publication_date', true);
		var latestDocument = _.head(sortedDocuments);

		if(latestDocument && latestDocument.id === id){
			return true;
		} else {
			return false;
		}

	}


  //-------------------------------
  // Session Expiry
  //-------------------------------
  $scope.logoutExpiredSession = function () {
    var logoutURL = window.location.protocol + '//' + window.location.hostname + '/logout';
    window.location.assign(logoutURL);
  }

  $scope.$on('session-invalidated', function (event, data) {
    $scope.openModal('session-expired');
  });

}

