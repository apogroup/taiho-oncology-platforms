'use strict';

/**
 * @ngdoc function
 * @name apopApp.controller:PreloaderCtrl
 * @description
 * # PreloaderCtrl
 * Controller of the apopApp
 */
angular.module('apopApp')
	.controller('PreloaderCtrlContent', PreloaderControllerContent);

PreloaderControllerContent.$inject = ['$scope', '$state', '$timeout', '$stateParams', '$http', 'LocalStorageService', '$rootScope'];

function PreloaderControllerContent($scope, $state, $timeout, $stateParams, $http, LocalStorageService, $rootScope) {

	//-------------------------------
	// Bind to $scope
	//-------------------------------

	$scope.$state = $state;

	$scope.preloader = Object.defineProperties({
		message: '',
		title: 'Loading content',
		progress: 0,
		fileProgress: 0,
		complete: false
	}, {
			progressBar: {
				get: function () {
					var percentage = [this.progress, '%'].join('');

					return {
						classes: {
							complete: this.complete
						},
						styles: {
							width: percentage
						},
						percentage: percentage
					};
				}
			},
			fileBar: {
				get: function () {
					return {
						classes: {
							'apop-loader-indeterminate': !Boolean(this.progress) || !Boolean(this.fileProgress)
						}
					}
				}
			}
		});

	var queue = new createjs.LoadQueue();

	queue.on('fileload', handleFileLoad);
	queue.on('progress', handleProgress);
	queue.on('error', handleError);
	queue.on('complete', handleComplete);

	// $http.get('content.manifest.json')
	// 	.success(function (manifest) {
	// 		queue.loadManifest(manifest);
	// 	})
	// 	.catch(function (err) {
	// 		console.error("Content Manifest load failed: ", err);
	// 		throw err;
	// 	});

	function handleFileLoad(event) {
		// console.log('File loaded', event);
		var request = event.item;
		try {
			var id = request.id;
			var response;
			switch (request.type) {
				case 'fileload':
					response = event.result;
					break;
				case 'json':
					response = event.result;
					break;
				default:
					console.warning('No matching type');
					return;
			}

			$rootScope[id] = response;
			LocalStorageService.set(id, response);

		} catch (error) {
			console.error(error, event);
		}
	}

	function handleProgress(event) {
		var progress = Math.round(event.loaded * 100);

		$timeout(function () {
			$scope.preloader.progress = progress;
		});
	}

	function handleError(event) {
		// console.error('File Error', event);
		var item = event.data;
		$scope.preloader.message = '[' + item.id + ' errored] ';
	}

	function handleComplete(event) {
		// console.log('Complete', event);
		$scope.preloader.message = '[Complete - All loaded]';
		$scope.preloader.complete = true;

		$http.get('cms/cache/timestamp')
			.success(function (response) {
				// console.log('API server timestamp', response);
				$scope.preload.content.timestamp = response.timestamp;
				$scope.preload.content.loaded = true;
				$state.go($state.current, $stateParams, { reload: true, inherit: false });
			})
			.catch(function (err) {
				// console.error("Failed to resolve timestamp from content API: ", err);
				throw err;
			});

		// $scope.preload.content.timestamp = moment().format();

	}

	$scope.escape = function () {
		$scope.preload.documents.loaded = true;
		$scope.preload.documents.force = false;
		// $state.go($state.current, $stateParams, { reload: true, inherit: false });
	}

	$scope.pause = function () {
		queue.setPaused(true);
	}

}

// http://codepen.io/yvesvanbroekhoven/pen/Efkar