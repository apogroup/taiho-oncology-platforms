'use strict';

/**
 * @ngdoc function
 * @name apopApp.controller:PreloaderCtrl
 * @description
 * # PreloaderCtrl
 * Controller of the apopApp
 */
angular.module('apopApp')
	.controller('PreloaderCtrlDoc', PreloaderControllerDocuments);

PreloaderControllerDocuments.$inject = ['$scope', '$state', '$timeout', '$stateParams', '$http'];

function PreloaderControllerDocuments($scope, $state, $timeout, $stateParams, $http) {

	//-------------------------------
	// Bind to $scope
	//-------------------------------

	$scope.$state = $state;

	$scope.preloader = Object.defineProperties({
		message: '',
		title: 'Loading files',
		progress: 0,
		fileProgress: 0,
		complete: false
	}, {
			progressBar: {
				get: function () {
					var percentage = [this.progress, '%'].join('');

					return {
						classes: {
							complete: this.complete
						},
						styles: {
							width: percentage
						},
						percentage: percentage
					};
				}
			},
			fileBar: {
				get: function () {
					var percentage = [this.fileProgress, '%'].join('');

					return {
						styles: {
							width: percentage
						},
					}
				}
			}
		});

	var queue = new createjs.LoadQueue();

	queue.on('fileload', handleFileLoad);
	queue.on('fileprogress', handleFileProgress);
	queue.on('progress', handleProgress);
	queue.on('error', handleError);
	queue.on('complete', handleComplete);

	$http.get('preload.manifest.json')
		.success(function (manifest) {
			queue.loadManifest(manifest.filesManifest.files);
		})
		.catch(function (error) {
			console.log("Manifest App Cache load failed: ", err);
		});

	function handleFileLoad(event) {
		// console.log('File loaded', event);
	}

	function handleFileProgress(event) {
		var fileprogress = Math.round(event.loaded * 100);
		// console.log('File progress', event.loaded);
		$timeout(function () {
			$scope.preloader.message = '[' + event.item.id + ' loading] ';
			// $scope.preloader.message.fileprogress = 'File Progress: ' + fileprogress + '%';
			$scope.preloader.fileProgress = fileprogress;
		});

	}

	function handleProgress(event) {
		var progress = Math.round(event.loaded * 100);
		// console.log('Progress', progress);

		$timeout(function () {
			$scope.preloader.progress = progress;
		});
	}

	function handleError(event) {
		// console.log('File Error', event);
		var item = event.data;
		$scope.preloader.message = '[' + item.id + ' errored] ';
	}

	function handleComplete(event) {
		// console.log('Complete', event);
		$scope.preloader.message = '[Complete - All loaded]';
		$scope.preloader.complete = true;

		$scope.preload.documents.timestamp = moment().format();
		$scope.preload.documents.loaded = true;
		// $scope.preload.manifestForce = false;

		$state.go($state.current, $stateParams, { reload: true, inherit: false });
	}

	$scope.escape = function () {
		$scope.preload.documents.loaded = true;
		$scope.preload.documents.force = false;
		// $state.go($state.current, $stateParams, {reload: true, inherit: false});
	}

	$scope.pause = function () {
		queue.setPaused(true);
	}

	// http://codepen.io/yvesvanbroekhoven/pen/Efkar

}
