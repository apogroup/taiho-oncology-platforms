'use strict';

/**
 * @ngdoc function
 * @name apopApp.service:PreloaderFactory
 * @description
 * # PreloaderFactory
 * Factory of the apopApp
 */
angular.module('apopApp')
	.service('PreloaderFactory', PreloaderFactory);

PreloaderFactory.$inject = ['LocalStorageService'];

function PreloaderFactory(LocalStorageService) {

	return {
		create: create
	};

	function create(name) {

		return Object.create(null, {

			get: {
				value: function (key) {
					return LocalStorageService.get([name, key].join('-'));
				}
			},

			set: {
				value: function (key, value) {
					return LocalStorageService.set([name, key].join('-'), value);
				}
			},


			//-------------------------------
			// Check or set if elements have been preloaded
			//-------------------------------
			loaded: {
				get: function () {
					try {
						var isPreloaded = this.get('preloaded');
						isPreloaded = JSON.parse(isPreloaded);
						// console.log('GET', name, isPreloaded);
						return _.defaultTo(isPreloaded, false);
					} catch (e) {
						console.error(e);
						return false;
					}
				},
				set: function (loaded) {
					// console.log(`Trying to set ${name}-preloaded to ${loaded}`);
					try {
						if (!_.isBoolean(loaded)) {
							throw new Error('Value must be a Boolean');
						}
						// console.log('SET', `${name}-preloaded`, loaded);
						this.set('preloaded', loaded);
					} catch (e) {
						console.error(e);
						return false;
					}
				}
			},

			//-------------------------------
			// When were elements last preloaded
			//-------------------------------
			timestamp: {
				get: function () {
					var timestamp;
					try {
						timestamp = this.get('preloaded-timestamp');
						timestamp = _.defaultTo(timestamp, undefined);
					} catch (e) {
						console.error(e);
						return timestamp = undefined;
					}

					// Moment will return current time when given undefined
					return moment(timestamp);
				},
				set: function (timestamp) {
					try {
						var datetime = moment(timestamp);
						if (!datetime.isValid()) {
							throw new Error('Value must be a valid datetime');
						}
						this.set('preloaded-timestamp', datetime.format());
						this.set('preloaded-timestamp-unix', timestamp);
					} catch (e) {
						console.error(e);
						return moment().format();
					}
				}
			},

			//-------------------------------
			// Manually force client to preload elements
			//-------------------------------
			force: {
				get: function () {
					var forceTimestamp = "2017-04-21T12:00:00-04:00";
					return this.timestamp.isBefore(forceTimestamp);
				}
			}
		});
	}

}
