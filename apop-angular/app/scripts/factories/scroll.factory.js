'use strict';

/**
 * @ngdoc function
 * @name apopApp.factory:ScrollFactory
 * @description
 * # ScrollFactory
 * factory of the apopApp
 */
angular.module('apopApp')
	.factory('ScrollFactory', ScrollFactory);

	ScrollFactory.$inject = ['$rootScope'];

	function ScrollFactory($rootScope) {

		var factory = {
			init: init
		}

		return factory;

		function init($scope) {

			$scope.scrollPageReset = function() {
				var scrollContainer = document.getElementById('ion-scroll-element');
				scrollContainer.scrollTop = 0;
			}
		
		}

	}