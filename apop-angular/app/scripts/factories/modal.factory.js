'use strict';

/**
 * @ngdoc function
 * @name apopApp.factory:ModalFactory
 * @description
 * # ModalFactory
 * factory of the apopApp
 */
angular.module('apopApp')
	.factory('ModalFactory', ModalFactory);

ModalFactory.$inject = ['$rootScope', '$ionicModal', 'LocalStorageService', 'MetricService'];

function ModalFactory($rootScope, $ionicModal, LocalStorageService, MetricService) {

	var factory = {
		build: build,
	}

	return factory;

	function build($scope, type, template) {

		if (!template) template = type;

		var templateUrl = ['views/global/modals/modal-', type, '.html'].join('');

		var promise = $ionicModal.fromTemplateUrl(templateUrl, {
			scope: $scope,
			animation: 'slide-in-up',
		})
			.then(function (modal) {
				var modalClass = ['apop', template, 'modal-backdrop'].join('-');
				modal.el.classList.add(modalClass);
				modal.type = type;
				$scope.modals.push(modal);
				return modal;
			});

		//-------------------------------
		// Modal Open/Close Fn
		//-------------------------------

		$scope.reference = {};
		// $scope.reference.Filename = null;

		$scope.pdfOptions = {
			fallbackLink: [
				'<div class="apop-fallback-message">',
					'<h2>This device does not support inline PDFs.</h2>',
					'<h3>Please download the PDF from the &apos;Details&apos; sidebar accessible from the bottom left corner of this window.</h3>',
				'</div>'
			]
		};

		// $scope.pdfOptions = {
		// 	fallbackLink: false,
		// 	forcePDFJS: true,
		// 	PDFJS_URL: '/pdf'
		// };


		$scope.document;
		$scope.document_disclaimer;

		$scope.document_modal_start;
		$scope.document_modal_end;
		$scope.document_modal_duration;

		$scope.reference_modal_start;
		$scope.reference_modal_end;
		$scope.reference_modal_duration;

		$scope.openModal = function (type, id) {

			_($scope.modals)
				.filter({ type: type })
				.forEach(function (modal) {
					modal.show();
				});

			try {

				switch (type) {

					case 'confirm-dashboard':
						$scope.document = _.find($scope.documents, { id: id }); 
						switch($scope.document.type){
							case 'internal':
								$scope.document_disclaimer = "This resource is for internal use only and may not be shared or distributed outside of Taiho. ";
								break;
								case 'proactive': 
								$scope.document_disclaimer = "This resource is for internal use only and may not be shared or distributed outside of Taiho. This resource has been approved for proactive external use by authorized Medical Affairs employees only.";
								break;
								case 'reactive':
									$scope.document_disclaimer = "This resource is for internal use only and may not be shared or distributed outside of Taiho. This resource has been approved for reactive external use by authorized Medical Affairs employees only."
									break;
								default:
									$scope.document_disclaimer = "This resource is for internal use only and may not be shared or distributed outside of Taiho.";
									break;										
						}
						break;
					case 'document-viewer':
						var document = _.find($scope.documents, { id: id });
						// var documentUrl = ['content', 'document', document.FilePath.id].join('/');
						var documentUrl = ['content', 'documents', document.filename+'.pdf'].join('/');
						PDFObject.embed(documentUrl, '#document', $scope.pdfOptions);
						$scope.document = document;

						MetricService.generateMetric({ type: 'document-view', doc: $scope.document.Title });
						$scope.document_modal_start = moment.now();

						break;

					case 'reference-viewer':
						/******************************************************************************************************************************
						if the user is opening the reference viewer, and the favorites modal is open, this will close the favorites modal
						before the reference viewer is opened
						*******************************************************************************************************************************/
						_($scope.modals)
							.filter($scope.modals, { type: 'favorites' })
							.forEach(function (modal) {
								modal.hide();
							});

						var reference = _.find($scope.references, { id: id });
						// var referenceUrl = ['content', 'references', reference.FilePath.id].join('/');
						var referenceUrl = ['content', 'references', reference.filename+'.pdf'].join('/');

						PDFObject.embed(referenceUrl, '#reference', $scope.pdfOptions)

						$scope.reference = reference;

						// MetricService.generateMetric({ type: 'reference-view', reference: $scope.reference.UniqueID });
						MetricService.generateMetric({ type: 'reference-view', reference: $scope.reference.filename });
						$scope.reference_modal_start = moment.now()
						break;

					case 'sp-figure-viewer':
						$scope.figure = _.find($scope.figures, { id: id });
						MetricService.generateMetric({ type: 'figure-view', figure: $scope.figure.key });
						break;

					case 'sp-table-viewer':
						$scope.table = _.find($scope.tables, { id: id });

						//MetricService.generateMetric({ type: 'table-view', table: $scope.table.title });
						break;

					case 'share-document':
						$scope.share_document = id;

						MetricService.generateMetric({ type: 'share-document-view', doc: $scope.document.Title });
						break;

					case 'share-reference':
						$scope.share_reference = id;

						// MetricService.generateMetric({ type: 'share-reference-view', reference: $scope.reference.UniqueID });
						MetricService.generateMetric({ type: 'share-reference-view', reference: $scope.reference.filename });
						break;

					case 'platform-alerts':
						MetricService.generateMetric({ type: 'platform-alerts' });
						break;

					case 'platform-survey':
						MetricService.generateMetric({ type: 'platform-survey' });
						break;

					case 'ask-platform':
						MetricService.generateMetric({ type: 'ask-platform-view' });
						break;

					case 'favorites':
						MetricService.generateMetric({ type: 'favorites-view' });
						break;

					case 'manifest-update':
						MetricService.generateMetric({ type: 'manifest-update' });
						break;

					case 'manifest-forced':
						MetricService.generateMetric({ type: 'manifest-forced' });
						break;

					case 'content-cache-update':
						MetricService.generateMetric({ type: 'manifest-forced' });
          break;

          case 'session-expired':
						// MetricService.generateMetric({ type: 'session-expired' });
						break;

					default:
						throw new Error('Not a valid modal type');
						break;
				}

			} catch (e) {

				console.error(e);
				$scope.closeModal(type);

			}

		};

		$scope.closeModal = function (type) {

			_($scope.modals)
				.filter({ type: type })
				.forEach(function (modal) {

					modal.hide();
					document.body.classList.remove('modal-open');

					switch (modal.type) {
						case 'confirm-dashboard':
							LocalStorageService.set('prompted_disclaimer', true);
							$scope.openModal('document-viewer', $scope.document.id);
							break;
						case 'document-viewer':
							LocalStorageService.set('prompted_disclaimer', false);
							$scope.document_modal_end = moment();
							$scope.document_modal_duration = moment.utc($scope.document_modal_end.diff($scope.document_modal_start)).format('HH:mm:ss');
							MetricService.generateMetric({ type: 'document-view-exit', duration: $scope.document_modal_duration, doc: $scope.document.Title});
							$scope.resetDurations();
							break;

						case 'reference-viewer':
							$scope.reference_modal_end = moment();
							$scope.reference_modal_duration = moment.utc($scope.reference_modal_end.diff($scope.reference_modal_start)).format('HH:mm:ss');
							// MetricService.generateMetric({ type: 'reference-view-exit', duration: $scope.reference_modal_duration, reference: $scope.reference.UniqueID });
							MetricService.generateMetric({ type: 'reference-view-exit', duration: $scope.reference_modal_duration, reference: $scope.reference.filename });
							$scope.resetDurations();
							break;

						default:
							break;
					}

				});
		};

		$scope.resetDurations = function () {
			$scope.document_modal_start = null;
			$scope.document_modal_end = null;
			$scope.document_modal_duration = null;
			$scope.reference_modal_start = null;
			$scope.reference_modal_end = null;
			$scope.reference_modal_duration = null;
		}

		$scope.closeSidebar = function () {
			var referenceSidebar = document.querySelector('.apop-reference-modal-sidebar');
			if (referenceSidebar) referenceSidebar.classList.remove('apop-modal-sidebar-open');

			var documentSidebar = document.querySelector('.apop-document-modal-sidebar');
			if (documentSidebar) documentSidebar.classList.remove('apop-modal-sidebar-open');
		};

		$scope.toggleModalSidebar = function () {
			var referenceSidebar = document.querySelector('.apop-reference-modal-sidebar');
			if (referenceSidebar) referenceSidebar.classList.toggle('apop-modal-sidebar-open');

			var documentSidebar = document.querySelector('.apop-document-modal-sidebar');
			if (documentSidebar) documentSidebar.classList.toggle('apop-modal-sidebar-open');
		};

		$scope.toggleRefPDFEmbed = function () {

			var current_toggle = document.querySelector('.current-label');

			if (current_toggle.classList.contains('apop-toggle-label-clean')) {

				$scope.stateof.refPDF = 'annotated';

				current_toggle.classList.remove('current-label');

				var marked_toggle = document.querySelector('.apop-toggle-label-marked');
				marked_toggle.classList.add('current-label');

				var referenceUrl = ['content', 'reference', $scope.reference.AnnotatedFilePath.id].join('/');
				PDFObject.embed(referenceUrl, '#reference', $scope.pdfOptions);

				// MetricService.generateMetric({ type: 'reference-view-annotated', reference: $scope.reference.UniqueID });
				MetricService.generateMetric({ type: 'reference-view-annotated', reference: $scope.reference.filename });
			}

			if (current_toggle.classList.contains('apop-toggle-label-marked')) {

				$scope.stateof.refPDF = 'clean';

				current_toggle.classList.remove('current-label');

				var clean_toggle = document.querySelector('.apop-toggle-label-clean');
				clean_toggle.classList.add('current-label');

				var referenceUrl = ['content', 'reference', $scope.reference.FilePath.id].join('/');
				PDFObject.embed(referenceUrl, '#reference', $scope.pdfOptions);
			}

		}

		$scope.downloadReference = function (type) {
			if (type === 'annotated') {
				// MetricService.generateMetric({ type: 'reference-annotated-downloaded', reference: $scope.reference.UniqueID });
				MetricService.generateMetric({ type: 'reference-annotated-downloaded', reference: $scope.reference.filename });
			} else if (type === 'clean') {
				// MetricService.generateMetric({ type: 'reference-clean-downloaded', reference: $scope.reference.UniqueID });
				MetricService.generateMetric({ type: 'reference-clean-downloaded', reference: $scope.reference.filename });
			}
		}

		$scope.downloadDocument = function (id) {
			MetricService.generateMetric({ type: 'document-downloaded', doc: $scope.document.Title });
			// if (id === $scope.document.FilePath.id){
			// 	MetricService.generateMetric({ type: 'document-downloaded', doc: $scope.document.Title });
			// } else if (id === $scope.document.SourceFilePath.id){
			// 	MetricService.generateMetric({ type: 'document-source-downloaded', doc: $scope.document.Title });
			// }
		}

		//-------------------------------
		// Modal Helper Functions
		//-------------------------------

		// Cleanup the modal when we're done with it!
		$scope.$on('$destroy', function () {
			$scope.modals.forEach(function (modal) {
				modal.remove();
			});
		});

		// Execute action on hide modal
		$scope.$on('modal.hidden', function (event, modal) {
			switch (modal.type) {
				case 'reference-viewer':

					$scope.closeSidebar();

					var current_toggle = document.querySelector('.current-label');
					if (current_toggle) current_toggle.classList.remove('current-label');

					var clean_toggle = document.querySelector('.apop-toggle-label-clean');
					if (clean_toggle) clean_toggle.classList.add('current-label');

					var toggle_btn = document.querySelector('.apop-toggle-input');
					if (toggle_btn) toggle_btn.checked = false;

					$scope.stateof.refPDF = 'clean';

					break;

				case 'document-viewer':
					$scope.closeSidebar();
					break;

				case 'platform-alerts':
					// $scope.markAllAlertedAlertStatus();
					break;

				default:
					break;

			}
		});

		// Execute action on remove modal
		$scope.$on('modal.removed', function () {
			// Execute action
		});

		return promise;
	}

}
