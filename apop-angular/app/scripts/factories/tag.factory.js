'use strict';

/**
 * @ngdoc function
 * @name apopApp.factory:TagFactory
 * @description
 * # TagFactory
 * factory of the apopApp
 */
angular.module('apopApp')
	.factory('TagFactory', TagFactory);

TagFactory.$inject = ['MetricService'];

function TagFactory(MetricService) {

	var factory = {
		init: init
	}

	return factory;

	function init($scope) {

		$scope.referenceTags = _.map($scope.tags, function (tag) {

			var subtags = _.map(tag.Subtags, function (subtag) {
				return _.assign({
					checked: false
				}, subtag);
			});

			return _.assign({ checked: false }, tag, {
				Subtags: subtags
			});

		});

		Object.defineProperties($scope, {

			activeTags: {
				get: function () {
					var tags = new Map();

					_.forEach($scope.referenceTags, function (tag) {
						if (tag.checked) {
							var tagId = tag.id;
							tags.set(tagId, new Map());

							_.forEach(tag.Subtags, function (subtag) {
								if (subtag.checked) {
									var tag = tags.get(tagId);
									tag.set(subtag.id, true);
								}
							});
						}
					});

					return tags;
				}
			},

			filteredReferences: {
				get: function () {

					if (_.isEmpty($scope.activeTags)) {

						return $scope.references;

					} else {

						return _.filter($scope.references, function (reference) {
							return _.some(reference.Tags, function (tag) {
								var id = tag.id;
								if ($scope.activeTags.has(id)) {
									var activeTag = $scope.activeTags.get(id);

									if (_.isEmpty(activeTag)) {
										return true;
									} else {
										return _.some(reference.Subtags, function (subtag) {
											return activeTag.has(subtag.id);
										});
									}

								}
							});
						});

					}
				}
			}
		});

		$scope.onTagChange = function (tag) {
			if (tag.checked) {
				$scope.tagSelected(tag);
			} else {
				_.forEach(tag.Subtags, function (subtag) {
					subtag.checked = false;
				});
			}

			$scope.scrollMainResize();

		};

		$scope.onSubtagChange = function (subtag) {
			if (subtag.checked) {
				$scope.subtagSelected(subtag);
			}

			$scope.scrollMainResize();
		};

		$scope.selectTag = function (tagId) {

			_.forEach($scope.referenceTags, function (tag) {

				if (tag.id === tagId) {

					tag.checked = true;
					$scope.tagSelected(tag);

				} else {

					tag.checked = false;

					_.forEach(tag.Subtags, function (subtag) {
						if (subtag.id === tagId) {
							tag.checked = subtag.checked = true;
							$scope.subtagSelected(subtag);
						} else {
							subtag.checked = false;
						}
					});

				}
			});

		}

		$scope.tagSelected = function (tag) {
			if (tag.checked) {
				MetricService.generateMetric({ type: 'tag-selected', tag: tag.Tag });
			}
		};

		$scope.subtagSelected = function (subtag) {
			if (subtag.checked) {
				MetricService.generateMetric({ type: 'subtag-selected', tag: subtag.Subtag });
			}
		};

		$scope.clearTagFilters = function () {
			_.forEach($scope.referenceTags, function (tag) {
				tag.checked = false;
				_.forEach(tag.Subtags, function (subtag) {
					subtag.checked = false;
				});
			});
		}


	}

}
