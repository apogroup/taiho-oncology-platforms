'use strict';

/**
 * @ngdoc function
 * @name apopApp.factory:SearchFactory
 * @description
 * # SearchFactory
 * factory of the apopApp
 */
angular.module('apopApp')
	.factory('SearchFactory', SearchFactory);

SearchFactory.$inject = ['$filter', '$timeout', 'MetricService', 'ContentService'];

function SearchFactory($filter, $timeout, MetricService, ContentService) {

	var factory = {
		init: init
	}

	return factory;

	function init($scope) {

		//-------------------------------
		// Search States
		//-------------------------------

		$scope.search.status = false;
		var timer;

		//-------------------------------
		// DOM Hide Show Search Drawer
		//-------------------------------

		$scope.displaySearchResults = function () {
			var results = document.querySelector('.apop-search-results');
			results.classList.add('apop-display-results');
		};

		$scope.hideSearchResults = function () {
			var results = document.querySelector('.apop-search-results');
			results.classList.remove('apop-display-results');
		};

		//-------------------------------
		// Filter Functions
		//-------------------------------

		$scope.filtered = {};

		$scope.updateSearchFilters = function () {

			if (timer)
				$timeout.cancel(timer);

			$scope.search.running = true;

			timer = $timeout(function () {

				var searchEndpoint = ['search?query=', $scope.search.query, '&', 'operator=', 'OR'].join('');

				ContentService.getCMSContent(searchEndpoint)
					.then(function (results) {

						var results = _.chain(results);

						// TAGS & SUBTAGS
						// $scope.filtered.tags = results.get('tags', [])
						// 	.map(function (tag) {
						// 		return Object.assign(tag, _.find($scope.tags, { id: tag.id }));
						// 	})
						// 	.map(function (tag) {
						// 		tag.Subtags = results.get('subtags', [])
						// 			.intersectionBy(tag.Subtags, 'id')
						// 			.map(function (subtag) {
						// 				return Object.assign(subtag, _.find(tag.Subtags, { id: subtag.id }));
						// 			})
						// 			.value();
						// 		return tag;
						// 	}).value();

						// SCIENTIFIC LEXICON
						// $scope.filtered.lexicon = results.get('lexicon', []).map(function(term){
						// 	return Object.assign(term, _.find($scope.lexicon, { id: term.id }));
						// }).value();

						// $scope.filtered.glossary = results.get('glossary', []).map(function(term){
						// 	return Object.assign(term, _.find($scope.glossary, { id: term.id }));
						// }).value();

						// $scope.filtered.abbreviations = results.get('abbreviations', [])
						// 	.map(function (abbreviation) {
						// 		return Object.assign(abbreviation, _.find($scope.abbreviations, { id: abbreviation.id }));
						// 	}).value();

						// CORE SCIENTIFIC STATEMENTS
						// $scope.filtered.chapters = results.get('chapters', [])
						// 	.map(function (chapter) {
						// 		return Object.assign(chapter, _.find($scope.chapters, { id: chapter.id }));
						// 	})
						// 	.map(function (chapter) {
						// 		chapter.Children = results.get('statements', [])
						// 			.intersectionBy(chapter.Children, 'id')
						// 			.map(function (statement) {
						// 				return Object.assign(statement, _.find(chapter.Children, { id: statement.id }));
						// 			})
						// 			.map(function (statement) {
						// 				statement.Children = results.get('substatements', [])
						// 					.intersectionBy(statement.Children, 'id')
						// 					.map(function (substatement) {
						// 						return Object.assign(substatement, _.find(statement.Children, { id: substatement.id }));
						// 					})
						// 					.value();
						// 				return statement;
						// 			})
						// 			.value();
						// 		return chapter;
						// 	})
						// 	.value();

						// REFERENCES
						$scope.filtered.references = results.get('references', []).map(function (reference) {
							return Object.assign(reference, _.find($scope.references, { id: reference.id }));
						}).value();

						// DOCUMENTS
						$scope.filtered.documents = results.get('documents', []).map(function (document) {
							return Object.assign(document, _.find($scope.documents, { id: document.id }));
						}).value();

					}, function () {

						// TAGS & SUBTAGS
						// $scope.filtered.tags = $filter('search')($scope.tags, $scope.search.query, 'OR');
						// $scope.filtered.tags = _.map($scope.filtered.tags, function (tag) {
						// 	return Object.assign(tag, {
						// 		Subtags: $filter('search')(tag.Subtags, $scope.search.query, 'OR')
						// 	});
						// });

						// SCIENTIFIC LEXICON

						// $scope.filtered.lexicon_terms = $filter('search')($scope.scientificlexicon.lexicon.terms, $scope.search.query, 'OR');
						// $scope.filtered.lexicon_terms = $filter('score')($scope.filtered.lexicon_terms, $scope.search.query, 'OR');
						// // console.log("Filtered Preferred Terms:", $scope.filtered.lexicon_terms);
						// // $scope.filtered.lexicon_terms = $filter('orderBy')($scope.filtered.lexicon_terms, 'score', true); //collection, expression, reverse

						// $scope.filtered.glossary_terms = $filter('search')($scope.scientificlexicon.glossary.terms, $scope.search.query, 'OR');
						// $scope.filtered.glossary_terms = $filter('score')($scope.filtered.glossary_terms, $scope.search.query, 'OR');
						// // console.log("Filtered Glossary Terms:", $scope.filtered.glossary_terms);
						// // $scope.filtered.glossary_terms = $filter('orderBy')($scope.filtered.glossary_terms, 'score', true);

						// $scope.filtered.abbreviations = $filter('search')($scope.abbreviations, $scope.search.query, 'OR');
						// $scope.filtered.abbreviations = $filter('score')($scope.filtered.abbreviations, $scope.search.query, 'OR');
						// console.log("Filtered Abbreviations:", $scope.filtered.abbreviations);
						// $scope.filtered.abbreviations = $filter('orderBy')($scope.filtered.abbreviations, 'score', true);

						// CORE SCIENTIFIC STATEMENTS
						// $scope.filtered.chapters = $filter('search')($scope.chapters, $scope.search.query, 'OR');
						// $scope.filtered.chapters = $filter('score')($scope.filtered.chapters, $scope.search.query, 'OR');
						// $scope.filtered.chapters = $filter('orderBy')($scope.filtered.chapters, 'score', true);
						// $scope.filtered.chapters = _.map($scope.filtered.chapters, function (chapter) {
						// 	return Object.assign(chapter, {
						// 		Children: (function () {
						// 			var statements = chapter.Children;

						// 			statements = $filter('search')(statements, $scope.search.query, 'OR');
						// 			statements = $filter('score')(statements, $scope.search.query, 'OR');
						// 			statements = $filter('orderBy')(statements, 'score', true);

						// 			statements = _.map(statements, function (statement) {
						// 				return Object.assign(statement, {
						// 					Children: (function () {

						// 						var substatements = statement.Children;

						// 						substatements = $filter('search')(substatements, $scope.search.query, 'OR');
						// 						substatements = $filter('score')(substatements, $scope.search.query, 'OR');
						// 						substatements = $filter('treeLevelContains')(substatements, $scope.search.query);
						// 						substatements = $filter('orderBy')(substatements, 'score', true);

						// 						return substatements;

						// 					}())
						// 				});
						// 			});

						// 			return statements;
						// 		}())
						// 	});
						// });

						// REFERENCES
						$scope.filtered.references = $filter('search')($scope.references, $scope.search.query, 'OR');
						$scope.filtered.references = $filter('score')($scope.filtered.references, $scope.search.query, 'OR');
						$scope.filtered.references = $filter('scoreRefContext')($scope.filtered.references, $scope.search.query, 'OR');
						$scope.filtered.references = $filter('orderBy')($scope.filtered.references, 'score', true);

						// DOCUMENTS
						$scope.filtered.documents = $filter('search')($scope.documents, $scope.search.query, 'OR');
						$scope.filtered.documents = $filter('score')($scope.filtered.documents, $scope.search.query, 'OR');
						$scope.filtered.documents = $filter('scoreDocContext')($scope.filtered.documents, $scope.search.query, 'OR');
						$scope.filtered.documents = $filter('orderBy')($scope.filtered.documents, 'score', true);

						// CONGRESS ACTIVITIES
						
						$scope.filtered.congress_activities = $filter('search')($scope.activities, $scope.search.query, 'OR');
						$scope.filtered.congress_activities = $filter('score')($scope.filtered.congress_activities, $scope.search.query, 'OR');
						$scope.filtered.congress_activities = $filter('orderBy')($scope.filtered.congress_activities, 'score', true);

					}).finally(function () {

						$scope.search.status = true;
						$scope.search.running = false;

						MetricService.generateMetric({ type: 'search-term', search: $scope.search.query });
					});

			}, 250);

		}

		$scope.clearSearchResults = function () {
			$scope.filtered = {};
			$scope.search.status = false;
			$timeout.cancel(timer);
		}

		$scope.startSearch = function (keyEvent) {
			if (keyEvent.which === 13) {
				$scope.displaySearchResults();
				$scope.updateSearchFilters();
			}
		}

		//-------------------------------
		// Helper Functions
		//-------------------------------

		$scope.submessageResultCount = function (tlsIndex) {

			// var result = $scope.filtered.top_level_statements.length;
			// var result = tlsIndex;

			// console.log("OUTPUT-" + tlsIndex + ": " + JSON.stringify($scope.css.top_level_statements[tlsIndex].statements) + "\n\n\n");

			var selectedTls = $scope.chapters[tlsIndex].Children;

			// console.log("SEARCH RESULTS-" + tlsIndex + ": " + JSON.stringify($filter('search')(selectedTls, $scope.search.query, 'OR')) + "\n\n\n");

			var result = $filter('search')(selectedTls, $scope.search.query, 'OR').length;

			// for(var i = 0; i < selectedTls.length; i++) {

			// 	console.log("SEARCH RESULTS-" + tlsIndex + ": " + $filter('search')(selectedTls, $scope.search.query, 'OR').length + "\n\n\n");
			// }

			// console.log("--------------------------------\n");

			return result;
		};

		$scope.buildSectionRoute = function (filteredResources) {

			filteredResources.forEach(function (resource) {
				resource.tags.forEach(function (tag) {
					$scope.tags.forEach(function (globalTag) {
						if (tag == globalTag.id) {
							resource.page = globalTag.tag;

							if (globalTag.bucket_type == 1) {
								resource.page_route = "#/congress-activities/congress/" + globalTag.route;
							}
							else if (globalTag.bucket_type == 2) {
								resource.page_route = "#/educational-resources/" + globalTag.route;
							}

						}
					})
				})
			})

			// console.log("Filtered Resources: ", filteredResources);
		}

	}


}
