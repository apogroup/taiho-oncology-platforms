'use strict';

/**
 * @ngdoc function
 * @name apopApp.factory:ManifestFactory
 * @description
 * # ManifestFactory
 * factory of the apopApp
 */
angular.module('apopApp')
    .factory('ManifestFactory', ManifestFactory);

ManifestFactory.$inject = [];

function ManifestFactory() {

    var factory = {
        init: init
    }

    return factory;

    function init($scope) {

        $scope.manifestLogoutRefresh = function () {
            var logoutURL = window.location.protocol + '//' + window.location.hostname + '/logout';
            window.location.assign(logoutURL);
        }

        //-------------------------------
        // Evaluate Cache Status
        //-------------------------------

        var cacheStatus = window.localStorage.getItem('appCacheStatus');

        if(cacheStatus === '4') {
            setTimeout(function(){
                $scope.openModal('manifest-update')
            }, 1000);
        };

        //-------------------------------
        // Evaluate Cache Forced
        //-------------------------------

        var forcedCache = window.localStorage.getItem('forcedCachePrompt');

        if (forcedCache === 'true') {
            setTimeout(function () {
                console.log('Manifest Forced Reload: Prompt User');
                $scope.openModal('manifest-forced');
                window.localStorage.setItem('forcedCachePrompt', false);
            }, 1000);
        };

    }

}
