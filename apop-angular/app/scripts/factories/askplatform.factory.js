'use strict';

/**
 * @ngdoc function
 * @name apopApp.factory:AskPlatformFactory
 * @description
 * # AskPlatformFactory
 * factory of the apopApp
 */
angular.module('apopApp')
	.factory('AskPlatformFactory', AskPlatformFactory);

	AskPlatformFactory.$inject = ['$ionicSlideBoxDelegate', 'AskPlatformService'];

	function AskPlatformFactory($ionicSlideBoxDelegate, AskPlatformService) {

		var factory = {
			init: init
		}

		return factory;

		function init($scope) {

			$scope.promptSurvey = function() {
				if(!$scope.surveys.length) $scope.openModal('platform-survey');
			}

			$scope.surveyNext = function() {
				$ionicSlideBoxDelegate.next();
			}

			$scope.surveyPrevious = function() {
				$ionicSlideBoxDelegate.previous();
			}

			$scope.saveAskPlatformComment = function(comment) {
				// console.log(comment);
				AskPlatformService.saveComment(comment)
				.then(function() {
					$scope.askplatform_responses.comments.comment = null;
				});
			};

			$scope.successAskPlatformComment = function() {
				// $scope.askplatform_responses.comments.comment = null;
				var message = document.querySelector('.help-container-submit-message');
				message.classList.add('show-message');
				setTimeout(function(){
					message.classList.remove('show-message');
				}, 5000);
			}

			$scope.saveAskPlatformSurvey = function(survey) {
				// console.log(survey);
				if(survey.survey_q3){
					survey.survey_q3 = JSON.stringify(survey.survey_q3);
				}
				AskPlatformService.saveSurvey(survey)
				.then(function() {
					AskPlatformService.getSubmittedSurveys()
					.then(function(data) { 
						$scope.surveys = data;
						// console.log("Database Surveys", $scope.surveys);
						return $scope.surveys;
					});
				});
			};

		}

	}
