'use strict';

/**
 * @ngdoc function
 * @name apopApp.factory:AlertFactory
 * @description
 * # AlertFactory
 * factory of the apopApp
 */
angular.module('apopApp')
	.factory('AlertFactory', AlertFactory);

	AlertFactory.$inject = ['$filter', 'AlertService'];

	function AlertFactory($filter, AlertService) {

		var factory = {
			init: init
		}

		return factory;

		function init($scope) {

			$scope.promptAlerts = function(){
				$scope.pending = $filter('unalerted')($scope.alerts);
				if($scope.pending.length > 0) $scope.openModal('platform-alerts');
			}

			$scope.markReadAlertStatus = function(alert) {
				AlertService.markReadAlertStatus(alert)
				.then(function() {
					AlertService.getPlatformAlerts()
					.then(function(data) { 
						$scope.alerts = data;
						console.log($scope.alerts);
						return $scope.alerts;
					});
				});
			};

			$scope.markAlertedAlertStatus = function(alert) {
				AlertService.markAlertedAlertStatus(alert)
				.then(function() {
					AlertService.getPlatformAlerts()
					.then(function(data) { 
						$scope.alerts = data;
						console.log($scope.alerts);
						return $scope.alerts;
					});
				});
			};

			$scope.markAllAlertedAlertStatus = function() {

				var alertsLength = $scope.alerts.length;

				$scope.alerts.map(function(alert, i){
					console.log(alert);

					AlertService.markAlertedAlertStatus(alert)
					.then(function() {
					});

					if(alertsLength === i+1) {
						AlertService.getPlatformAlerts()
						.then(function(data) { 
							$scope.alerts = data;
							console.log($scope.alerts);
							return $scope.alerts;
						});
					}

				});

			};

		}


	}
