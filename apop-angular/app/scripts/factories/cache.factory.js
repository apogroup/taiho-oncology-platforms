'use strict';

/**
 * @ngdoc function
 * @name apopApp.factory:CacheFactory
 * @description
 * # CacheFactory
 * factory of the apopApp
 */
angular.module('apopApp')
    .factory('CacheFactory', CacheFactory);

CacheFactory.$inject = ['PreloaderService', '$http'];

function CacheFactory(PreloaderService, $http) {

    var factory = {
        init: init
    }

    return factory;

    function init($scope) {

        $scope.contentCacheRefresh = function () {
            PreloaderService.content.loaded = false;
        }

        //-------------------------------
        // Evaluate Cache Status
        //-------------------------------

        // setTimeout(function () {

        //     var contentPreloadedTimestamp = Date.parse(PreloaderService.content.timestamp);
        //     // console.log('Content Preloaded Timestamp', contentPreloadedTimestamp);

        //     var reqUrl = 'cms/cache/renew?timestamp=' + contentPreloadedTimestamp;

        //     $http.get(reqUrl)
        //         .success(function (response) {
        //             // console.log('API server cache renew bool: ', response);

        //             if (response === true) {
        //                 setTimeout(function(){
        //                     $scope.openModal('content-cache-update');
        //                 }, 1000);
        //             };

        //             return;
        //         })
        //         .catch(function (err) {
        //             // console.error("Failed to resolve cache renew from content API: ", err);
        //             throw err;
        //         });

        // }, 1000);

    }

}
