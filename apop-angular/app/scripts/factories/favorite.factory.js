'use strict';

/**
 * @ngdoc function
 * @name apopApp.factory:FavoriteFactory
 * @description
 * # FavoriteFactory
 * Factory of the apopApp
 */
angular.module('apopApp')
	.factory('FavoriteFactory', FavoriteFactory);

FavoriteFactory.$inject = ['FavoriteService', 'ContentService', 'LocalStorageService'];

function FavoriteFactory(FavoriteService, ContentService, LocalStorageService) {

	var factory = {
		init: init
	}

	return factory;

	function init($scope) {

		$scope.saveFavorite = function (reference) {
			console.log("Saving Favorite", reference);

			var favorite = {
				reference: reference.id,
			};

			FavoriteService.saveFavorite(favorite)
				.then(function (favorite) {

					console.log('Response: ', favorite);

					var user = $scope.user;

					console.log('User', $scope.user);

					var userFavorites = user.favorites || [];
					user.favorites = _.unionBy(userFavorites, [favorite], 'reference');
					LocalStorageService.set('favorites', user.favorites);

					console.log('User Favorites', user.favorites);

					ContentService.getCMSContent('references')
						.then(function (data) {
							// console.log("Favorites Data", data);
							$scope.references = data;
							LocalStorageService.set('references', data);
						})
						.catch(function (error) {
							console.log("Save Favorite Error: ", error);
						});
				});

		};

		$scope.archiveFavorite = function (reference) {
			console.log("Archiving Favorite", reference);

			var favorite = {
				reference: reference.id
			};

			FavoriteService.archiveFavorite(favorite)
				.then(function (favorite) {

					console.log('Response: ', favorite);

					var user = $scope.user;

					console.log('User', $scope.user);

					var userFavorites = user.favorites || [];
					user.favorites = userFavorites.filter(function (savedFavorite) {
						return savedFavorite.reference !== favorite.reference;
					});
					LocalStorageService.set('favorites', user.favorites);

					console.log('User Favorites', user.favorites);

					ContentService.getCMSContent('references')
						.then(function (data) {
							// console.log("Favorites Data", data);
							$scope.references = data;
							LocalStorageService.set('references', data);
						})
						.catch(function (error) {
							console.log("Save Favorite Error: ", error);
						});
				});

		};

	}

}
