'use strict';

//-------------------------------
// Reference is favorited
//-------------------------------

angular.module('apopApp')
	.filter('orderByIdArray', function () {

		return function (array, orderArray) {
            
            var sortObj = orderArray.reduce(function(acc, value, index) {
              acc[value] = index;
              return acc;
            }, {});

            return _.sortBy(array, function(obj) {
              return sortObj[obj.id];
            });

		};

	});