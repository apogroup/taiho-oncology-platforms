'use strict';

//-------------------------------
// Reference Filter By Tag
//-------------------------------
// Return an array of references that contain
// any of the selected tags in their Tags property.
// If no matches, return all references.
//-------------------------------

angular.module('apopApp')
	.filter('empty', function ($filter) {
		return function (input, path) {

			try {
				// No path given
				if (_.isNil(path)) {
					return _.isEmpty(input);
				}

				var prop = _.property(path);

				return _.has(input, path) && _.isEmpty(prop(input));
			}
			catch (e) {
				console.error(e);
				return true;
			}
		}
	});

angular.module('apopApp')
	.filter('notEmpty', function ($filter) {
		return _.negate($filter('empty'));
	});

angular.module('apopApp')
	.filter('has', function ($filter) {
		return _.has;
	});

angular.module('apopApp')
	.filter('size', function ($filter) {
		return _.size;
	});

angular.module('apopApp')
	.filter('gt', function ($filter) {
		return _.gt;
	});

angular.module('apopApp')
	.filter('intersectionBy', function ($filter) {
		return _.intersectionBy;
	});

angular.module('apopApp')
	.filter('log', function ($filter) {
		return function (value) {
			console.log(value);
			return value;
		}
	});

angular.module('apopApp')
	.filter('tail', function ($filter) {
		return _.tail;
	});