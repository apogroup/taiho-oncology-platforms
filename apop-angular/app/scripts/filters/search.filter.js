'use strict';

//-------------------------------
// Search
//-------------------------------
(function () {

	var stopWords = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "able", "about", "above", "abroad", "according", "accordingly", "across", "actually", "adj", "after", "afterwards", "again", "against", "ago", "ahead", "ain't", "all", "allow", "allows", "almost", "alone", "along", "alongside", "already", "also", "although", "always", "am", "amid", "amidst", "among", "amongst", "an", "and", "another", "any", "anybody", "anyhow", "anyone", "anything", "anyway", "anyways", "anywhere", "apart", "appear", "appreciate", "appropriate", "are", "aren't", "around", "as", "a's", "aside", "ask", "asking", "associated", "at", "available", "away", "awfully", "back", "backward", "backwards", "be", "became", "because", "become", "becomes", "becoming", "been", "before", "beforehand", "begin", "behind", "being", "believe", "below", "beside", "besides", "best", "better", "between", "beyond", "both", "brief", "but", "by", "came", "can", "cannot", "cant", "can't", "caption", "cause", "causes", "certain", "certainly", "changes", "clearly", "c'mon", "co", "co.", "com", "come", "comes", "concerning", "consequently", "consider", "considering", "contain", "containing", "contains", "corresponding", "could", "couldn't", "course", "c's", "currently", "dare", "daren't", "definitely", "described", "despite", "did", "didn't", "different", "directly", "do", "does", "doesn't", "doing", "done", "don't", "down", "downwards", "during", "each", "edu", "eg", "eight", "eighty", "either", "else", "elsewhere", "end", "ending", "enough", "entirely", "especially", "et", "etc", "even", "ever", "evermore", "every", "everybody", "everyone", "everything", "everywhere", "ex", "exactly", "example", "except", "fairly", "far", "farther", "few", "fewer", "fifth", "first", "five", "followed", "following", "follows", "for", "forever", "former", "formerly", "forth", "forward", "found", "four", "from", "further", "furthermore", "get", "gets", "getting", "given", "gives", "go", "goes", "going", "gone", "got", "gotten", "greetings", "had", "hadn't", "half", "happens", "hardly", "has", "hasn't", "have", "haven't", "having", "he", "he'd", "he'll", "hello", "help", "hence", "her", "here", "hereafter", "hereby", "herein", "here's", "hereupon", "hers", "herself", "he's", "hi", "him", "himself", "his", "hither", "hopefully", "how", "howbeit", "however", "hundred", "i'd", "ie", "if", "ignored", "i'll", "i'm", "immediate", "in", "inasmuch", "inc", "inc.", "indeed", "indicate", "indicated", "indicates", "inner", "inside", "insofar", "instead", "into", "inward", "is", "isn't", "it", "it'd", "it'll", "its", "it's", "itself", "i've", "just", "k", "keep", "keeps", "kept", "know", "known", "knows", "last", "lately", "later", "latter", "latterly", "least", "less", "lest", "let", "let's", "like", "liked", "likely", "likewise", "little", "look", "looking", "looks", "low", "lower", "ltd", "made", "mainly", "make", "makes", "many", "may", "maybe", "mayn't", "me", "mean", "meantime", "meanwhile", "merely", "might", "mightn't", "mine", "minus", "miss", "more", "moreover", "most", "mostly", "mr", "mrs", "much", "must", "mustn't", "my", "myself", "name", "namely", "nd", "near", "nearly", "necessary", "need", "needn't", "needs", "neither", "never", "neverf", "neverless", "nevertheless", "new", "next", "nine", "ninety", "no", "nobody", "non", "none", "nonetheless", "noone", "no-one", "nor", "normally", "not", "nothing", "notwithstanding", "novel", "now", "nowhere", "obviously", "of", "off", "often", "oh", "ok", "okay", "old", "on", "once", "one", "ones", "one's", "only", "onto", "opposite", "or", "other", "others", "otherwise", "ought", "oughtn't", "our", "ours", "ourselves", "out", "outside", "over", "overall", "own", "particular", "particularly", "past", "per", "perhaps", "placed", "please", "plus", "possible", "presumably", "probably", "provided", "provides", "que", "quite", "qv", "rather", "rd", "re", "really", "reasonably", "recent", "recently", "regarding", "regardless", "regards", "relatively", "respectively", "right", "round", "said", "same", "saw", "say", "saying", "says", "second", "secondly", "see", "seeing", "seem", "seemed", "seeming", "seems", "seen", "self", "selves", "sensible", "sent", "serious", "seriously", "seven", "several", "shall", "shan't", "she", "she'd", "she'll", "she's", "should", "shouldn't", "since", "six", "so", "some", "somebody", "someday", "somehow", "someone", "something", "sometime", "sometimes", "somewhat", "somewhere", "soon", "sorry", "specified", "specify", "specifying", "still", "sub", "such", "sup", "sure", "take", "taken", "taking", "tell", "tends", "th", "than", "thank", "thanks", "thanx", "that", "that'll", "thats", "that's", "that've", "the", "their", "theirs", "them", "themselves", "then", "thence", "there", "thereafter", "thereby", "there'd", "therefore", "therein", "there'll", "there're", "theres", "there's", "thereupon", "there've", "these", "they", "they'd", "they'll", "they're", "they've", "thing", "things", "think", "third", "thirty", "this", "thorough", "thoroughly", "those", "though", "three", "through", "throughout", "thru", "thus", "till", "to", "together", "too", "took", "toward", "towards", "tried", "tries", "truly", "try", "trying", "t's", "twice", "two", "un", "under", "underneath", "undoing", "unfortunately", "unless", "unlike", "unlikely", "until", "unto", "up", "upon", "upwards", "us", "use", "used", "useful", "uses", "using", "usually", "v", "value", "various", "versus", "very", "via", "viz", "vs", "want", "wants", "was", "wasn't", "way", "we", "we'd", "welcome", "well", "we'll", "went", "were", "we're", "weren't", "we've", "what", "whatever", "what'll", "what's", "what've", "when", "whence", "whenever", "where", "whereafter", "whereas", "whereby", "wherein", "where's", "whereupon", "wherever", "whether", "which", "whichever", "while", "whilst", "whither", "who", "who'd", "whoever", "whole", "who'll", "whom", "whomever", "who's", "whose", "why", "will", "willing", "wish", "with", "within", "without", "wonder", "won't", "would", "wouldn't", "yes", "yet", "you", "you'd", "you'll", "your", "you're", "yours", "yourself", "yourselves", "you've", "zero"];
	var parseTerms = function (query) {
		return _.chain(query)
			.split(/\s+/)
			.difference(stopWords)
			.value();
	}
	var isNotArray = _.negate(_.isArray);

	angular.module('apopApp')
		.filter("search", function ($filter) {

			return function (elements, query, operator) {

				try {

					if (isNotArray(elements)) {
						throw new Error('Input must be an array');
					}

					var operator = _.defaultTo(operator, 'AND');
					var query = _.defaultTo(query, '').toLowerCase();
					var elements = _.defaultTo(elements, []);

					var terms = parseTerms(query);

					// console.log({ elements, query, terms, operator });

					switch (operator) {

						case 'AND':
							return $filter('filter')(elements, terms);

						case 'OR':
							return _(terms)
								.flatMap(function (term) {
									return $filter('filter')(elements, term);
								})
								.uniq()
								.value();

						default:
							return [];

					}

				} catch (e) {

					console.error(e);
					return [];

				}

			};
		});

	angular.module('apopApp')
		.filter("treeLevelContains", function ($filter) {

			return function (elements, query) {

				try {

					if (isNotArray(elements)) {
						throw new Error('Input must be an array');
					}

					var query = _.defaultTo(query, '').toLowerCase();
					var elements = _.defaultTo(elements, []);

					var terms = parseTerms(query);

					// console.log({ elements, query, terms });

					return $filter('filter')(elements, function (element) {

						var elementText;
						try {
							elementText = JSON.stringify(_.omit(element, ['AnnotatedFilePath', 'SourceFilePath', 'ThumbnailPath']));
						} catch (e) {
							console.error(e);
							return false;
						}

						var score = _.reduce(terms, function (score, term) {

							var queryScore;
							try {
								var query = new RegExp(query, 'gi');
								queryScore = elementText.match(query).length;
							} catch (e) {
								console.error(e);
								queryScore = 0;
							}

							var termScore;
							try {
								var term = new RegExp(term, 'gi');
								termScore = elementText.match(term).length;
							} catch (e) {
								console.error(e);
								termScore = 0;
							}

							return score + queryScore + termScore;
						}, 0);

						return score > 0;
					});

				} catch (e) {

					console.error(e);
					return false;

				}

			};
		});

	angular.module('apopApp')
		.filter("score", function ($filter) {
			return function (elements, query, operator) {

				try {

					if (isNotArray(elements)) {
						throw new Error('Input must be an array');
					}

					var operator = _.defaultTo(operator, 'AND');
					var query = _.defaultTo(query, '').toLowerCase();
					var elements = _.defaultTo(elements, []);

					var terms = parseTerms(query);

					// console.log('Score', { elements, query, terms, operator });

					switch (operator) {

						case 'AND':
							return _.map(elements, function (element) {

								var score;
								try {
									var query = new RegExp(query, 'gi');
									score = JSON.stringify(_.omit(element, ['AnnotatedFilePath', 'SourceFilePath', 'ThumbnailPath'])).match(query).length;
								} catch (e) {
									console.error(e);
									score = 0;
								}

								var elementScore = _.defaultTo(element.score, 0);

								return _.assign(element, {
									score: score + elementScore
								});
							});

						case 'OR':
							return _.reduce(terms, function (elements, term) {

								return _.map(elements, function (element) {

									var score;
									var elementText;
									try {
										elementText = JSON.stringify(_.omit(element, ['AnnotatedFilePath', 'SourceFilePath', 'ThumbnailPath']));
									} catch (e) {
										console.error(e);
									}

									var queryScore;
									try {
										var query = new RegExp(query, 'gi');
										queryScore = elementText.match(query).length * 2; //*2 weight for exact query match
									} catch (e) {
										console.error(e);
										queryScore = 0;
									}

									var termScore;
									try {
										var term = new RegExp(term, 'gi');
										termScore = elementText.match(term).length;
									} catch (e) {
										console.error(e);
										termScore = 0;
									}

									var score = queryScore + termScore;

									var elementScore = _.defaultTo(element.score, 0);

									return _.assign(element, {
										score: score + elementScore
									});

								});

							}, elements);

						default:
							return [];
					}

				} catch (e) {

					console.error(e);
					return [];

				}

			};
		});

	angular.module('apopApp')
		.filter("scoreRefContext", function ($filter) {
			return function (elements, query, operator) {

				try {

					if (isNotArray(elements)) {
						throw new Error('Input must be an array');
					}

					var operator = _.defaultTo(operator, 'AND');
					var query = _.defaultTo(query, '').toLowerCase();
					var elements = _.defaultTo(elements, []);

					var terms = parseTerms(query);

					// console.log('Score Reference Context', { elements, query, terms, operator });

					switch (operator) {

						case 'AND':
							return _.map(elements, function (element) {

								var score;
								try {

									var query = new RegExp(query, 'gi');

									var scoreDisplayName;
									try {
										scoreDisplayName = JSON.stringify(element.DisplayName).match(query).length;
									} catch (e) {
										console.error(e);
										scoreDisplayName = 0;
									}

									var scoreShortCitation;
									try {
										scoreShortCitation = JSON.stringify(element.ShortCitation).match(query).length;
									} catch (e) {
										console.error(e);
										scoreShortCitation = 0;
									}

									// var scoreTitle;
									// try {
									// 	scoreTitle = JSON.stringify(element.Title).match(query).length;
									// } catch (e) {
									// 	console.error(e);
									// 	scoreTitle = 0;
									// }

									score = Math.max(scoreDisplayName + scoreShortCitation, 1);
									// score = Math.max(scoreDisplayName + scoreShortCitation + scoreTitle, 1);

								} catch (e) {

									console.error(e);
									score = 0;

								}

								var score = Math.max(score, 1);
								var elementScore = _.defaultTo(element.score, 1);

								return _.assign(element, {
									score: score * elementScore
								});

							});

						case 'OR':
							return _.reduce(terms, function (elements, term) {

								return _.map(elements, function (element) {

									var score;
									try {

										var query = new RegExp(query, 'gi');

										var scoreQueryDisplayName;
										try {
											scoreQueryDisplayName = JSON.stringify(element.DisplayName).match(query).length * 2;
										} catch (e) {
											console.error(e);
											scoreQueryDisplayName = 0;
										}

										var scoreQueryShortCitation;
										try {
											scoreQueryShortCitation = JSON.stringify(element.ShortCitation).match(query).length * 2;
										} catch (e) {
											console.error(e);
											scoreQueryShortCitation = 0;
										}

										// var scoreQueryTitle;
										// try {
										// 	scoreQueryTitle = JSON.stringify(element.Title).match(query).length * 2;
										// } catch (e) {
										// 	console.error(e);
										// 	scoreQueryTitle = 0;
										// }

										var term = new RegExp(term, 'gi');

										var scoreTermDisplayName;
										try {
											scoreTermDisplayName = JSON.stringify(element.DisplayName).match(term).length;
										} catch (e) {
											console.error(e);
											scoreTermDisplayName = 0;
										}

										var scoreTermShortCitation;
										try {
											scoreTermShortCitation = JSON.stringify(element.ShortCitation).match(term).length;
										} catch (e) {
											console.error(e);
											scoreTermShortCitation = 0;
										}

										score = _.sum([scoreQueryDisplayName, scoreQueryShortCitation, scoreTermDisplayName, scoreTermShortCitation]);
										// score = _.sum([scoreQueryDisplayName, scoreQueryShortCitation, scoreQueryTitle, scoreTermDisplayName, scoreTermShortCitation]);

									} catch (e) {
										console.error(e);
										score = 1;
									}

									score = Math.max(score, 1);
									var elementScore = _.defaultTo(element.score, 1);

									return _.assign(element, {
										score: score * elementScore
									});

								});

							}, elements)

						default:
							return [];
					}

				} catch (e) {
					console.error(e);
					return [];
				}

			};
		});

	angular.module('apopApp')
		.filter("scoreDocContext", function ($filter) {
			return function (elements, query, operator) {

				try {

					if (isNotArray(elements)) {
						throw new Error('Input must be an array');
					}

					var operator = _.defaultTo(operator, 'AND');
					var query = _.defaultTo(query, '').toLowerCase();
					var elements = _.defaultTo(elements, []);

					var terms = parseTerms(query);

					// console.log('Score Document Context', { elements, query, terms, operator });

					switch (operator) {

						case 'AND':
							return _.map(elements, function (element) {

								var score;
								try {

									var query = new RegExp(query, 'gi');
									score = JSON.stringify(element.Title).match(query).length;

								} catch (e) {

									console.error(e);
									score = 1;

								}

								var score = Math.max(score, 1);
								var elementScore = _.defaultTo(element.score, 1);

								return _.assign(element, {
									score: score * elementScore
								});

							});

						case 'OR':
							return _.reduce(terms, function (elements, term) {

								return _.map(elements, function (element) {

									var score;
									try {

										var query = new RegExp(query, 'gi');

										var scoreQueryTitle;
										try {
											scoreQueryTitle = JSON.stringify(element.Title).match(query).length * 2;
										} catch (e) {
											console.error(e);
											scoreQueryTitle = 0;
										}

										var term = new RegExp(term, 'gi');

										var scoreTermTitle;
										try {
											scoreTermTitle = JSON.stringify(element.Title).match(term).length;
										} catch (e) {
											console.error(e);
											scoreTermTitle = 0;
										}

										score = scoreQueryTitle + scoreTermTitle;

									} catch (e) {
										console.error(e);
										score = 1;
									}

									score = Math.max(score, 1);
									var elementScore = _.defaultTo(element.score, 1);

									return _.assign(element, {
										score: score * elementScore
									});

								});

							}, elements)

						default:
							return [];
					}

				} catch (e) {
					console.error(e);
					return [];
				}

			};
		});

})();