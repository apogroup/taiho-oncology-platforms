'use strict';

//-------------------------------
// Reference Filter By Tag
//-------------------------------
// Return an array of references that contain
// any of the selected tags in their Tags property.
// If no matches, return all references.
//-------------------------------

angular.module('apopApp')
	.filter('byTag', function ($filter) {
		return function (references, tags) {

			try {

				var checkedTags = _.chain(tags).filter('checked');

				return _.chain(references).reject(function (reference) {
					return checkedTags.intersectionBy(reference.Tags, 'id').isEmpty().value();
				}).thru(function (r) {
					return _.isEmpty(r) ? references : r;
				}).value();

			} catch (e) {

				console.error(e);
				return references;

			}

		};

	});

//-------------------------------
// Reference Filter By Subtag
//-------------------------------
// Return an array of references that contain
// any of the selected subtags in their Subtags property.
// If no matches, return all references.
//-------------------------------

angular.module('apopApp')
	.filter('bySubTag', function ($filter) {
		return function (references, subtags) {

			try {

				var checkedSubtags = _(subtags).filter('checked');

				return _.chain(references).reject(function (reference) {
					return checkedSubtags.intersectionBy(reference.Subtags, 'id').isEmpty();
				}).thru(function (r) {
					return _.isEmpty(r) ? references : r;
				}).value();

			} catch (e) {

				console.error(e);
				return references;

			}


		};

	});

//-------------------------------
// Reference Count by Tag
//-------------------------------

angular.module('apopApp')
	.filter('tagCount', function ($filter) {
		return function (references, tagID) {

			if (!Array.isArray(references)) return false;

			return $filter("filter")(references, function (reference) {

				return _.some(reference.Tags, { id: tagId });

			});

		};

	});

//-------------------------------
// Reference Count by Subtag
//-------------------------------

angular.module('apopApp')
	.filter('subtagCount', function ($filter) {
		return function (tags, subtagID) {

			if (!Array.isArray(tags)) return false;

			return $filter("filter")(tags, function (tag) {

				return _.some(tag.Subtags, { id: subtagID });

			});

		};

	});
