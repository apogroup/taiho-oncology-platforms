'use strict';

//-------------------------------
// Date Comparators
//-------------------------------

angular.module('apopApp')
.filter('isFuture', function($filter){

	return function(items, date){
		
		var d = date;

		if (!_.isArray(items)) return false;

		return $filter("filter")(items, function(item){

			var now = new Date();
			var date = new Date(item[d]);

			if(date > now) return true;
			return false;

		});

	};

});

angular.module('apopApp')
.filter('isPast', function($filter){

	return function(items, date){
		
		var d = date;

		if (!_.isArray(items)) return false;

		return $filter("filter")(items, function(item){

			var now = new Date();
			var date = new Date(item[d]);

			if(date < now) return true;
			return false;

		});

	};

});