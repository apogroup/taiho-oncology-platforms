'use strict';

//-------------------------------
// Reference is favorited
//-------------------------------

angular.module('apopApp')
	.filter('favorited', function ($filter) {

		return function (references, user) {

			if (!(_.isArray(user ? user.favorites : null) && _.isArray(references) && references.length)) return false;

			return $filter("filter")(references, function(reference){
				return $filter('isFavorited')(reference, user);
			});

		};

	});

angular.module('apopApp')
	.filter('isFavorited', function () {

		return function (reference, user) {

			if (!(_.isArray(user ? user.favorites : null) && reference && reference.id)) return false;

			return user.favorites.filter(function (favorite) {
				return favorite.favorited;
			})
				.map(function (favorite) {
					return favorite.reference;
				})
				.includes(reference.id);

		};

	});