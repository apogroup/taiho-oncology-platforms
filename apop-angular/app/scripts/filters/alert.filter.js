'use strict';

//-------------------------------
// Alerts
//-------------------------------

angular.module('apopApp')
.filter('unalerted', function($filter){

	return function(alerts){
		
		if (!_.isArray(alerts)) return false;

		return $filter("filter")(alerts, function(alert){

			if(!alert.alertstatus[0] || !alert.alertstatus[0].alerted) return true;
			return false;

		});

	};

});

angular.module('apopApp')
.filter('read', function($filter){

	return function(alerts){
		
		if (!_.isArray(alerts)) return false;

		return $filter("filter")(alerts, function(alert){

			if (alert.alertstatus[0] && alert.alertstatus[0].read) return true;
			return false;

		});

	};

});

angular.module('apopApp')
.filter('unread', function($filter){

	return function(alerts){
		
		if (!_.isArray(alerts)) return false;

		return $filter("filter")(alerts, function(alert){

			if (!alert.alertstatus[0] || !alert.alertstatus[0].read) return true;
			return false;

		});

	};

});