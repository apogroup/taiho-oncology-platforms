'use strict';

//-------------------------------
// sortByFirstCharacter
//-------------------------------

angular.module('apopApp')
.filter('sortByFirstCharacter', function($filter){

	return function(arr, character){

		if (!_.isArray(arr)) return false;
		if (!character) return false;

		character = character.toLowerCase();

		if (character === 'all') {
			return $filter("filter")(arr, function(obj){
				return true;
			});
		}

		if (character === '#'){
			var nums = /^[0-9]+$/;
			return $filter("filter")(arr, function(obj){
				if(obj.Term)
					return obj.Term.charAt(0).match(nums);
				else if (obj.ShortCitation)
					return obj.ShortCitation.charAt(0).match(nums);
			});
		}
		
		return $filter("filter")(arr, function(obj){
			var first_character = obj.Term ? obj.Term.charAt(0).toLowerCase() : obj.ShortCitation ? obj.ShortCitation.charAt(0).toLowerCase() : obj.Term.charAt(0).toLowerCase();
			return first_character === character;
		});	
		
	};

});