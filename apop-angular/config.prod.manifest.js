'use strict';

//---------------------------------------------/
//  PRODUCTION MANIFEST CONFIGURATION
// 
//  Manifest is only built in production deploy
//
//	Configure settings for manifesting
//
//  Define files to be manifested 
//	** Only from deploy 'dist', never from app
//
//---------------------------------------------/

module.exports = {

	//---------------------------------------------/
	//	Files to EXCLUDE
	//---------------------------------------------/

	manifestExclude: [

		////////////////////////////
		// IMAGES
		////////////////////////////

		// 'images/congress-grid/*.png',
		// 'images/congress-detail/*.png',

		////////////////////////////
		// CONTENT
		////////////////////////////

		'content/_references_index.json'

	],

	//---------------------------------------------/
	//	Files to INCLUDE
	//---------------------------------------------/

	manifestInclude: [

		////////////////////////////
		// ROOT
		////////////////////////////

		'*.html',
		'!index-offline.html',
		'!offline-cache.html',
		'*.json',
		
		////////////////////////////
		// SCRIPTS
		////////////////////////////

		'scripts-bower/*',
		'scripts/*',

		////////////////////////////
		// STYLES
		////////////////////////////

		'styles/*.css',
		'styles/fonts/*',

		////////////////////////////
		// VIEWS
		////////////////////////////

		'views/{,*/}{,*/}*.html',

		////////////////////////////
		// IMAGES
		////////////////////////////

		'images/*.png',
		'images/*.jpg',

		////////////////////////////
		// CONTENT
		////////////////////////////

		'content/{,*/}*.json',
		'content/{,*/}{,*/}*.png',
		'content/{,*/}{,*/}*.jpg',

		// 'content/external/oarsi_whitepaper/*'
		// 'content/documents/{,*/}*.pdf',
		// 'content/references/lam_2016.pdf'
	]

	
};