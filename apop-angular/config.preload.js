'use strict';

//---------------------------------------------/
//  PRELOAD MANIFEST CONFIGURATION
// 
//  Used on both development, and staging/production
//
//  Define files to be preloaded 
//
//---------------------------------------------/

module.exports = {

	//---------------------------------------------/
	//	Files to INCLUDE
	//---------------------------------------------/

	includeApp: [

		////////////////////////////
		// ROOT
		////////////////////////////

		'<%= platform.app %>/*.html',
		
		////////////////////////////
		// SCRIPTS
		////////////////////////////

		// Angular does not like when you try to double load

		////////////////////////////
		// STYLES
		////////////////////////////

		'<%= platform.app %>/styles/*.css',
		'<%= platform.app %>/styles/fonts/*',

		////////////////////////////
		// VIEWS
		////////////////////////////

		'<%= platform.app %>/views/{,*/}{,*/}*.html',

		////////////////////////////
		// IMAGES
		////////////////////////////

		'<%= platform.app %>/images/{,*/}{,*/}*.png',
		'<%= platform.app %>/images/{,*/}{,*/}*.jpg',

		////////////////////////////
		// CONTENT
		////////////////////////////

		'<%= platform.app %>/content/{,*/}*.json',
		'<%= platform.app %>/content/{,*/}{,*/}*.png',
		'<%= platform.app %>/content/{,*/}{,*/}*.jpg',

		// '<%= platform.dist %>/content/{,*/}*.pdf',
		// '<%= platform.dist %>/content/documents/{,*/}*.pdf',
		// '<%= platform.dist %>/content/references/lam_2016.pdf'
	],

	includeDist: [

		////////////////////////////
		// ROOT
		////////////////////////////

		'<%= platform.dist %>/*.html',
		
		////////////////////////////
		// SCRIPTS
		////////////////////////////

		// Angular does not like when you try to double load

		////////////////////////////
		// STYLES
		////////////////////////////

		'<%= platform.dist %>/styles/*.css',
		'<%= platform.dist %>/styles/fonts/*',

		////////////////////////////
		// VIEWS
		////////////////////////////

		'<%= platform.dist %>/views/{,*/}{,*/}*.html',

		////////////////////////////
		// IMAGES
		////////////////////////////

		'<%= platform.dist %>/images/{,*/}{,*/}*.png',
		'<%= platform.dist %>/images/{,*/}{,*/}*.jpg',

		////////////////////////////
		// CONTENT
		////////////////////////////

		'<%= platform.dist %>/content/{,*/}*.json',
		'<%= platform.dist %>/content/{,*/}{,*/}*.png',
		'<%= platform.dist %>/content/{,*/}{,*/}*.jpg',

		// '<%= platform.dist %>/content/external/oarsi_whitepaper/styles/fonts/*',
		// '<%= platform.dist %>/content/external/oarsi_whitepaper/styles/img/*.png',
		// '<%= platform.dist %>/content/external/oarsi_whitepaper/styles/img/app/*',
		// '<%= platform.dist %>/content/{,*/}*.pdf',
		// '<%= platform.dist %>/content/documents/{,*/}*.pdf',
		// '<%= platform.dist %>/content/references/lam_2016.pdf',

	]

	
};