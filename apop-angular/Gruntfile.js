// Generated on 2016-10-11 using generator-angular 0.15.1, revised 2017-08-16

'use strict';

// # Globbing
// for performance reasons we're only matching one level down:
// 'test/spec/{,*/}*.js'
// use this if you want to recursively match all subfolders:
// 'test/spec/**/*.js'

module.exports = function (grunt) {

////////////////////////////////////////////////////////////////////////////////////////////////////////////
//---------------------------------------------------------------------------------------------------------/
//
//    LOAD FILES
//
//---------------------------------------------------------------------------------------------------------/
////////////////////////////////////////////////////////////////////////////////////////////////////////////

	var manifestConfig = require('./config.prod.manifest.js');
	var preloadConfig = require('./config.preload.js');

////////////////////////////////////////////////////////////////////////////////////////////////////////////
//---------------------------------------------------------------------------------------------------------/
//
//    LOAD GRUNT UTILITIES
//
//---------------------------------------------------------------------------------------------------------/
////////////////////////////////////////////////////////////////////////////////////////////////////////////



	// Time how long tasks take. Can help when optimizing build times

	require('time-grunt')(grunt);

	// Automatically load required Grunt tasks

	require('jit-grunt')(grunt, {
		useminPrepare: 'grunt-usemin',
		preload_assets: 'grunt-preload-assets',
		replace: 'grunt-replace',
		rename: 'grunt-contrib-rename',
		manifest: 'grunt-manifest',
		shell: 'grunt-shell-spawn',
		compress: 'grunt-contrib-compress',
		prompt: 'grunt-prompt',
		choose: 'grunt-choose'
	});


////////////////////////////////////////////////////////////////////////////////////////////////////////////
//---------------------------------------------------------------------------------------------------------/
//
//    DELCARE CONFIGURATION VARIABLES
//
//---------------------------------------------------------------------------------------------------------/
////////////////////////////////////////////////////////////////////////////////////////////////////////////

	// Declare configurable paths (and variables) for the application

	var appConfig = {
		app: require('./bower.json').appPath || 'app',
		dist: 'dist'
	};

////////////////////////////////////////////////////////////////////////////////////////////////////////////
//---------------------------------------------------------------------------------------------------------/
//
//    CONFIGURATION
//
//---------------------------------------------------------------------------------------------------------/
////////////////////////////////////////////////////////////////////////////////////////////////////////////

	grunt.initConfig({

	////////////////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////////////////
	// // 
	// //	CONFIGURE
	// //
	////////////////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////////////////

	//---------------------------------------------/
	//	CONFIGURATION VARIABLES
	//---------------------------------------------/
	//
	//	Define global variables reference in tasks
	//
	//---------------------------------------------/
		
		// Assign package.json to pkg to access variables
		pkg: grunt.file.readJSON('package.json'),
		versionFileExtension: grunt.file.readJSON('package.json').version.replace(/([.])/g, '-'),
		cacheHash: require('crypto').createHash('sha1').update(new Date().toISOString()).digest('hex'),

		// Get Config
		configPlatform: require('./app-config/config.platform.js'),

		// Declare platform & Assign project configuration variables
		platform: appConfig,






	////////////////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////////////////
	// // 
	// //	TASK PRIMARILY FOR DEVELOPMENT
	// //	server: '.tmp'
	// //
	// //	Shared Task: connect(dist), clean(dist),
	// //	compass(dist)
	// //
	////////////////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////////////////

	//---------------------------------------------/
	//  TASK
	// 
	//  CONNECT: SPINS LOCALHOST GRUNT SERVER
	//
	//  Grunt server settings
	//
	//---------------------------------------------/

		connect: {
			options: {
				port: 9000,
				// Change this to '0.0.0.0' to access the server from outside.
				hostname: 'localhost',
				livereload: 35729
			},
			livereload: {
				options: {
					open: true,
					middleware: function (connect) {
						return [
							connect.static('.tmp'),
							connect().use(
								'/bower_components',
								connect.static('./bower_components')
							),
							connect().use(
								'/app/styles',
								connect.static('./app/styles')
							),
							connect.static(appConfig.app)
						];
					}
				}
			},
			// test: {
			// 	options: {
			// 		port: 9001,
			// 		middleware: function (connect) {
			// 			return [
			// 				connect.static('.tmp'),
			// 				connect.static('test'),
			// 				connect().use(
			// 					'/bower_components',
			// 					connect.static('./bower_components')
			// 				),
			// 				connect.static(appConfig.app)
			// 			];
			// 		}
			// 	}
			// },
			// dist: {
			// 	options: {
			// 		open: true,
			// 		base: '<%= platform.dist %>'
			// 	}
			// }
		},


	//---------------------------------------------/
	//  TASK -- not in use
	// 
	//  JSHINT: LINTS JS FOR ERRORS
	//
	//  Validates JS to make sure there are no
	//  obvious mistakes
	//
	//  References: .jshintrc
	//---------------------------------------------/

		// jshint: {
		// 	options: {
		// 		jshintrc: '.jshintrc',
		// 		reporter: require('jshint-stylish')
		// 	},
		// 	all: {
		// 		src: [
		// 			'Gruntfile.js',
		// 			'<%= platform.app %>/scripts/{,*/}*.js'
		// 		]
		// 	},
		// 	test: {
		// 		options: {
		// 			jshintrc: 'test/.jshintrc'
		// 		},
		// 		src: ['test/spec/{,*/}*.js']
		// 	}
		// },


	//---------------------------------------------/
	//  TASK
	// 
	//  WIREDEP: CONCAT BOWER COMPONENTS
	//
	//  Concatenate all bower components from ./
	//
	//---------------------------------------------/

		bower_concat: {
			all: {
				dest: '<%= platform.app %>/scripts-bower/bower-components.js',
				// cssDest: '<%= platform.app %>/scripts-bower/bower-components.css',
				exclude: [],
				dependencies: {},
				mainFiles: {
					'browser-detect': ['dist/browser-detect.umd.js'],
				},
				bowerOptions: {
					relative: false
				}
			}
		}, 


	//---------------------------------------------/
	//  TASK
	// 
	//  COMPASS: COMPILES SASS TO CSS
	//
	//  Compiles Sass to CSS and generate neccesary
	//  files if requested
	//
	//---------------------------------------------/

		compass: {
			options: {
				sassDir: '<%= platform.app %>/styles',
				// cssDir: '.tmp/styles',
				cssDir: '<%= platform.app %>/styles',
				generatedImagesDir: '.tmp/images/generated',
				imagesDir: '<%= platform.app %>/images',
				javascriptsDir: '<%= platform.app %>/scripts',
				fontsDir: '<%= platform.app %>/styles/fonts',
				importPath: './bower_components',
				httpImagesPath: '/images',
				httpGeneratedImagesPath: '/images/generated',
				httpFontsPath: '/styles/fonts',
				relativeAssets: false,
				assetCacheBuster: false,
				raw: 'Sass::Script::Number.precision = 10\n'
			},
			dist: {
				options: {
					generatedImagesDir: '<%= platform.dist %>/images/generated'
				}
			},
			server: {
				options: {
					sourcemap: true
				}
			}
		},


	//---------------------------------------------/
	//  TASK - not in use
	// 
	//  SASS: Compiles Sass
	//
	//  Compiles Sass (raw in development)
	//
	//---------------------------------------------/

		// sass: {
		// 	dist: {
		// 		files: {
		// 			'app/styles/apop.app.css': 'app/styles/apop.app.scss'
		// 		}
		// 	}
		// },


	//---------------------------------------------/
	//  TASK - not in use
	// 
	//  KARMA: DEFINES KARMA TEST
	//
	//  Load test settings
	//
	//---------------------------------------------/

		// karma: {
		// 	unit: {
		// 		configFile: 'test/karma.conf.js',
		// 		singleRun: true
		// 	}
		// },





	////////////////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////////////////
	// // 
	// //	TASK PRIMARILY FOR PRODUCTION
	// //	dist: 'dist'
	// //
	////////////////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////////////////


	//---------------------------------------------/
	//  TASKS (co-dependent) - not in use
	// 
	//  USEMINPREPARE: READS HTML FOR USEMIN BLOCKS
	//
	//  Reads HTML for usemin blocks to enable smart 
	//  builds that automatically concat, minify and 
	//  revision files. Creates configurations in 
	//  memory so additional tasks can operate on them
	//
	//  USEMIN: REWRITES HTML
	//
	//  Performs rewrites based on filerev and the 
	//  useminPrepare configuration
	//
	//---------------------------------------------/

		//**********************************************/
		//  By default, your `index.html`'s 
		//  <!-- Usemin block --> will take care of
		//  minification. 
		//**********************************************/

		// useminPrepare: {
		// 	html: '<%= platform.app %>/index.html',
		// 	options: {
		// 		dest: '<%= platform.dist %>',
		// 		flow: {
		// 			html: {
		// 				steps: {
		// 					js: ['concat', 'uglifyjs']
		// 				},
		// 				post: {}
		// 			}
		// 		}
		// 	}
		// },

		useminPrepare: {
			html: '<%= platform.app %>/index.html',
			options: {
				dest: '<%= platform.dist %>'
			}
		},

		usemin: {
			html: '<%= platform.dist %>/index.html',
			options: {}
		},


		

	//**********************************************/
	//  The following *-min tasks will produce 
	//  minified files in the dist folder
	//  By default, your `index.html`'s 
	//  <!-- Usemin block --> will take care of
	//  minification. These next options are 
	//  pre-configured if you do not wish
	//  to use the Usemin blocks.
	//**********************************************/

	//---------------------------------------------/
	//  TASK
	// 
	//  NGANNOTATE: ANNOTATES ANGULAR JS
	//
	//  Makes code safe for minification
	//
	//---------------------------------------------/

		// ng-annotate tries to make the code safe for minification automatically
		// by using the Angular long form for dependency injection.
		ngAnnotate: {
			dist: {
				files: [{
					expand: true,
					cwd: '.tmp/concat/scripts',
					src: '*.js',
					dest: '.tmp/concat/scripts'
				}]
			}
		},

	//---------------------------------------------/
	//  TASK
	// 
	//  CSSMIN: MINIFIES CSS
	//
	//  Minifies CSS -- post Sass compile
	//
	//---------------------------------------------/

		// cssmin: {
		// 	dist: {
		// 		files: {
		// 			'<%= platform.app %>/styles/apop.app.css': [
		// 				'<%= platform.dist %>/styles/apop.app.css'
		// 			]
		// 		}
		// 	}
		// },

	//---------------------------------------------/
	//  TASK
	// 
	//  CONCAT: CONCATENATES JS
	//
	//  Minifies JS -- post JS minification(uglify)
	//
	//---------------------------------------------/
		
		// concat: {
		// 	dist: {}
		// },


	//---------------------------------------------/
	//  TASK
	// 
	//  UGLIFY: MINIFIES JS
	//
	//  Minifies JavaScript files
	//
	//---------------------------------------------/

		// uglify: {
		// 	dist: {
		// 		files: {
		// 			'<%= platform.dist %>/scripts/scripts.js': [
		// 				'<%= platform.dist %>/scripts/scripts.js'
		// 			]
		// 		}
		// 	}
		// },


	//---------------------------------------------/
	//  TASK
	// 
	//  IMAGEMIN: MINIFIES / COMPRESSES IMAGES
	//
	//  Minifies Images
	//
	//---------------------------------------------/

		imagemin: {
			dist: {
				files: [{
					expand: true,
					cwd: '<%= platform.app %>/images',
					src: '{,*/}*.{png,jpg,jpeg,gif}',
					dest: '<%= platform.dist %>/images'
				}]
			}
		},


	//---------------------------------------------/
	//  TASK
	// 
	//  IMAGEMIN: MINIFIES / COMPRESSES IMAGES
	//
	//  Minifies Images
	//
	//---------------------------------------------/
		svgmin: {
			dist: {
				files: [{
					expand: true,
					cwd: '<%= platform.app %>/images',
					src: '{,*/}*.svg',
					dest: '<%= platform.dist %>/images'
				}]
			}
		},

	//---------------------------------------------/
	//  TASKS
	// 
	//  HTMLMIN: MINIFIES HTML
	//  NGANNOTATE: SAFELY MINIFIES ANGULAR HTML
	//
	//  Minifies HTML
	//
	//---------------------------------------------/

		htmlmin: {
			dist: {
				options: {
					collapseWhitespace: true,
					conservativeCollapse: true,
					collapseBooleanAttributes: true,
					removeCommentsFromCDATA: true
				},
				files: [{
					expand: true,
					cwd: '<%= platform.dist %>',
					src: ['*.html'],
					dest: '<%= platform.dist %>'
				}]
			}
		},

		// ng-annotate tries to make the code safe for minification automatically
		// by using the Angular long form for dependency injection.
		ngAnnotate: {
			dist: {
				files: [{
					expand: true,
					cwd: '.tmp/concat/scripts',
					src: '*.js',
					dest: '.tmp/concat/scripts'
				}]
			}
		},


	////////////////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////////////////
	// // 
	// //	TASK GLOBAL
	// //	
	// //
	////////////////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////////////////

	//---------------------------------------------/
	//  TASK
	// 
	//  CLEAN: DUMPS FOLDERS / FILES
	//
	//  Empties folders to start clean
	//
	//---------------------------------------------/

		clean: {
			
			//---------------------------------------------/
			// DEVELOPMENT (ANGULAR > SAILS)
			//---------------------------------------------/

			server: '.tmp',
			
			sass: '.sass-cache',
			
			// sailsBower: '../apop-sails/assets/scripts-bower',

			// sailsViews: '../apop-sails/assets/views',

			// sailsHTML: '../apop-sails/assets/*.html',

			// sailsStyles: '../apop-sails/assets/styles',

			// sailsScripts: '../apop-sails/assets/scripts',

			// sailsJSONContent: '../apop-sails/assets/content/*.json',

			//---------------------------------------------/
			// DEVELOPMENT & PRODUCTION
			//---------------------------------------------/

			sailsAssets: '../apop-sails/assets/{,*/}*',
			sailsContent: '../apop-sails/content/{,*/}*',


			//---------------------------------------------/
			// PRODUCTION (APP > DIST, DIST > SAILS)
			//---------------------------------------------/

			dist: {
				files: [{
					dot: true,
					src: [
						'.tmp',
						'<%= platform.dist %>/{,*/}*',
						'!<%= platform.dist %>/.git{,*/}*'
					]
				}]
			},

			options: {
				force: true
			}
		},

	//---------------------------------------------/
	//  TASK
	// 
	//  COPY: Copies files
	//
	//  Copies files
	//
	//---------------------------------------------/

		copy: {

			//---------------------------------------------/
			// DEVELOPMENT (ANGULAR > SAILS)
			//---------------------------------------------/

			appToSails: {
				files: [{
					expand: true,
					dot: true,
					cwd: '<%= platform.app %>',
					dest: '../apop-sails/assets',
					src: [
						'**',
						'!**/styles/*.scss',
						'!**/styles/*.css.map',
						'!**/styles/sass/**',
						'!**/content/**'
					]
				}]
			},

			appContentToSails: {
				files: [{
					expand: true,
					dot: true,
					cwd: '<%= platform.app %>/content',
					dest: '../apop-sails/content',
					src: [
						'**',
					]
				}]
			},

			appBowerToSails: {
				files: [{
					expand: true,
					dot: true,
					cwd: '<%= platform.app %>',
					dest: '../apop-sails/assets',
					src: [
						'scripts-bower/{,*/}*',
					]
				}]
			},

			appHTMLToSails: {
				files: [{
					expand: true,
					dot: true,
					cwd: '<%= platform.app %>',
					dest: '../apop-sails/assets',
					src: [
						'*.html',
						'views/{,*/}*',
						'scripts/**/*.html'
					]
				}]
			},

			appStylesToSails: {
				files: [{
					expand: true,
					dot: true,
					cwd: '<%= platform.app %>',
					dest: '../apop-sails/assets',
					src: [
						'styles/{,*/}*',
					]
				}]
			},

			appScriptsToSails: {
				files: [{
					expand: true,
					dot: true,
					cwd: '<%= platform.app %>',
					dest: '../apop-sails/assets',
					src: [
						'scripts/**/*.js',
					]
				}]
			},

			appJSONContentToSails: {
				files: [{
					expand: true,
					dot: true,
					cwd: '<%= platform.app %>/content',
					dest: '../apop-sails/content',
					src: [
						'*.json',
					]
				}]
			},


			//---------------------------------------------/
			// PRODUCTION (APP > DIST, DIST > SAILS)
			//---------------------------------------------/

			appToDist: {
				files: [{
					expand: true,
					dot: true,
					cwd: '<%= platform.app %>',
					dest: '<%= platform.dist %>',
					src: [
						'**/*',
						'!**/scripts/**',
						'!**/images/{,*/}*.{png,jpg,jpeg,gif,svg}',
						'!**/styles/*.scss',
						'!**/styles/*.css.map',
						'!**/styles/sass/**',
					]
				}]
			},

			distToSails: {
				files: [{
					expand: true,
					dot: true,
					cwd: '<%= platform.dist %>',
					dest: '../apop-sails/assets',
					src: [
						'**',
						'!**/content/**'
					]
				}]
			},

			distContentToSails: {
				files: [{
					expand: true,
					dot: true,
					cwd: '<%= platform.dist %>/content',
					dest: '../apop-sails/content',
					src: [
						'**'
					]
				}]
			}

		},


	//---------------------------------------------/
	//  TASK
	// 
	//  REPLACE: REPLACE MATCH IN FILES
	//
	//  Used to update version number
	// 	Used to append version number for cache busting
	//  Used to create index-offline for manifest
	//	Used to cleanup preload.manifest.json
	//
	//---------------------------------------------/

		replace: {

			versionNumber: {
				options: {
					patterns: [
						{
							match: new RegExp('%VERSION%', 'g'),
							replacement: '<%= pkg.version %>'
						},
						{
							match: new RegExp('0\.0\.0', 'g'),
							replacement: '<%= pkg.version %>'
						}
					]
				},
				files: [
					{
						expand: false,
						flatten: false,
						src: ['<%= platform.dist %>/login.html'],
						dest: '<%= platform.dist %>/login.html'
					},
					{
						expand: false,
						flatten: false,
						src: ['<%= platform.dist %>/scripts/scripts.js'],
						dest: '<%= platform.dist %>/scripts/scripts.js'
					}
				]
			},

			platformVariables: {
				options: {
					patterns: [
						{
							match: new RegExp('%PLATFORM_TITLE%', 'g'),
							replacement: '<%= configPlatform.title %>'
						},
						{
							match: new RegExp('%PLATFORM_THEME_COLOR%', 'g'),
							replacement: '<%= configPlatform.themeColor %>'
						},
						{
							match: new RegExp('%PLATFORM_COMPOUND%', 'g'),
							replacement: '<%= configPlatform.compound %>'
						},
						{
							match: new RegExp('%PLATFORM_INDICATION%', 'g'),
							replacement: '<%= configPlatform.indication %>'
						},
					]
				},
				files: [
					{
						expand: false,
						flatten: false,
						src: ['<%= platform.dist %>/index.html'],
						dest: '<%= platform.dist %>/index.html'
					},
					{
						expand: false,
						flatten: false,
						src: ['<%= platform.dist %>/login.html'],
						dest: '<%= platform.dist %>/login.html'
					},
					{
						expand: false,
						flatten: false,
						src: ['<%= platform.dist %>/views/global/header.html'],
						dest: '<%= platform.dist %>/views/global/header.html'
					},
					{
						expand: false,
						flatten: false,
						src: ['<%= platform.dist %>/views/global/preloader.html'],
						dest: '<%= platform.dist %>/views/global/preloader.html'
					}
				]
			},

			cacheBustedFiles: {
				options: {
					patterns: [
						{
							match: new RegExp('offline-cache([^.]*).*?.html', 'g'),
							replacement: 'offline-cache.<%= versionFileExtension %>.<%= cacheHash %>.html'

						},
						{
							match: new RegExp('content.manifest.json', 'g'),
							replacement: 'content.manifest.<%= versionFileExtension %>.<%= cacheHash %>.json'
						},
						{
							match: new RegExp('preload.manifest.json', 'g'),
							replacement: 'preload.manifest.<%= versionFileExtension %>.<%= cacheHash %>.json'
						},
						{
							match: new RegExp('scripts-bower\/bower-components.js', 'g'),
							replacement: 'scripts-bower/bower-components.<%= versionFileExtension %>.<%= cacheHash %>.js'
						},
						{
							match: new RegExp('scripts\/scripts.js', 'g'),
							replacement: 'scripts/scripts.<%= versionFileExtension %>.<%= cacheHash %>.js'
						},
						{
							match: new RegExp('styles\/apop.app.css', 'g'),
							replacement: 'styles/apop.app.<%= versionFileExtension %>.<%= cacheHash %>.css'
						},
						{
							match: new RegExp('styles\/apop.print.css', 'g'),
							replacement: 'styles/apop.print.<%= versionFileExtension %>.<%= cacheHash %>.css'
						}
					]
				},
				files: [
					{
						expand: false,
						flatten: false,
						src: ['<%= platform.dist %>/index.html',],
						dest: '<%= platform.dist %>/index.html'
					},
					{
						expand: false,
						flatten: false,
						src: ['<%= platform.dist %>/login.html',],
						dest: '<%= platform.dist %>/login.html'
					},
					{
						expand: false,
						flatten: false,
						src: ['<%= platform.dist %>/error.html',],
						dest: '<%= platform.dist %>/error.html'
					},
					{
						expand: false,
						flatten: false,
						src: ['<%= platform.dist %>/scripts/scripts.js'],
						dest: '<%= platform.dist %>/scripts/scripts.js'
					}
				]
			},

			distIndexOffline: {
				options: {
					patterns: [
						{
							match: new RegExp('<html>', 'g'),
							replacement: '<html manifest="manifest.appcache" type="text/cache-manifest">'
						},
						{
							match: new RegExp('<iframe(?=[^>]*?src="offline-cache([^.]*).*?.html").*?style="([^"]*).*?<\/iframe>', 'g'),
							replacement: '<!--OFFLINE-->'
						}

					]
				},
				files: [
					{
						expand: false,
						flatten: false, 
						src: ['<%= platform.dist %>/index.html'], 
						dest: '<%= platform.dist %>/index-offline.html'
					}
				]
			},

			appPreloadJSON: {
				options: {
					patterns: [
						{
							match: new RegExp('app/', 'g'),
							replacement: ''
						}
					]
				},
				files: [
					{
						expand: false,
						flatten: false, 
						src: ['<%= platform.app %>/preload.manifest.json'], 
						dest: '<%= platform.app %>/preload.manifest.json'
					}
				]
			},

			distPreloadJSON: {
				options: {
					patterns: [
						{
							match: new RegExp('dist/', 'g'),
							replacement: ''
						}
					]
				},
				files: [
					{
						expand: false,
						flatten: false, 
						src: ['<%= platform.dist %>/preload.manifest.json'], 
						dest: '<%= platform.dist %>/preload.manifest.json'
					}
				]
			},

			manifest:  {
				options: {
					patterns: [
						{
							match: new RegExp('/%20index-offline.html', 'g'),
							replacement: '/ index-offline.html'
						}
					]
				},
				files: [
					{
						expand: false,
						flatten: false, 
						src: ['<%= platform.dist %>/manifest.appcache'], 
						dest: '<%= platform.dist %>/manifest.appcache'
					}
				]
			},

		},

	//---------------------------------------------/
	//  TASK
	// 
	//  RENAME: RENAME FILES TO APPEND VERSION
	//
	// 	Used to append version number for cache busting
	//
	//---------------------------------------------/

		rename: {
			cacheBusting: {
				files: [
					{
						src: ['<%= platform.dist %>/offline-cache.html'],
						dest: '<%= platform.dist %>/offline-cache.<%= versionFileExtension %>.<%= cacheHash %>.html'
					},
					{
						src: ['<%= platform.dist %>/scripts-bower/bower-components.js'],
						dest: '<%= platform.dist %>/scripts-bower/bower-components.<%= versionFileExtension %>.<%= cacheHash %>.js'
					},
					{
						src: ['<%= platform.dist %>/scripts/scripts.js'],
						dest: '<%= platform.dist %>/scripts/scripts.<%= versionFileExtension %>.<%= cacheHash %>.js'
					},
					{
						src: ['<%= platform.dist %>/styles/apop.app.css'],
						dest: '<%= platform.dist %>/styles/apop.app.<%= versionFileExtension %>.<%= cacheHash %>.css'
					},
					{
						src: ['<%= platform.dist %>/styles/apop.print.css'],
						dest: '<%= platform.dist %>/styles/apop.print.<%= versionFileExtension %>.<%= cacheHash %>.css'
					}
				]
			},
			cacheBustingPreloadJSON: {
				files: [
					{
						src: ['<%= platform.dist %>/content.manifest.json'],
						dest: '<%= platform.dist %>/content.manifest.<%= versionFileExtension %>.<%= cacheHash %>.json'
					},
					{
						src: ['<%= platform.dist %>/preload.manifest.json'],
						dest: '<%= platform.dist %>/preload.manifest.<%= versionFileExtension %>.<%= cacheHash %>.json'
					}
				]
			}
		},

	//---------------------------------------------/
	//  TASK
	// 
	//  PRELOAD_ASSETS: GENERATES PRELOADJS MANIFEST
	//
	//  Generates preload (PreloadJS) manifest.json
	//	file from defined rules
	//
	//---------------------------------------------/

		preload_assets: {

			app: {
				options: {
					detectId: false,
					detectType: false,
				},
				files: {
					'<%= platform.app %>/preload.manifest.json': preloadConfig.includeApp
				}
			},

			dist: {
				options: {
					detectId: false,
					detectType: false,
				},
				files: {
					'<%= platform.dist %>/preload.manifest.json': preloadConfig.includeDist
				}
			},

		},

	//---------------------------------------------/
	//  TASK
	// 
	//  MANIFEST: GENERATES MANIFEST FILE
	//
	//  Generates manifest file from defined rules
	//
	//---------------------------------------------/

		// REF IMPORT: manfiestConfig, ./config/config.prod.manifest

		manifest: {
			generate: {
				options: {
					basePath: '<%= platform.dist %>/',
					cache: [],
					network: ['*'],
					fallback: ['/ index-offline.html'], // SPACE IS 
					exclude: manifestConfig.manifestExclude,
					preferOnline: true,
					headcomment: "<%= pkg.name %> v<%= pkg.version %>",
					verbose: true,
					timestamp: true,
					hash: true,
					master: ['index.html']
				},
				src: manifestConfig.manifestInclude,
				dest: '<%= platform.dist %>/manifest.appcache'
			}
		},

	
	//---------------------------------------------/
	//  TASK
	// 
	//  CONCURRENT: RUN TASKS IN PARRALLEL
	//
	//  Run some tasks in parallel to speed up the 
	//  build process
	//
	//---------------------------------------------/

		concurrent: {

			// COMPASS
			compass: [
				'compass:server',
			],

			options: {
				// logConcurrentOutput: true
			}
		},





	////////////////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////////////////
	// // 
	// //	COMPRESS ZIP
	// //
	////////////////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////////////////

	//---------------------------------------------/
	//  TASK
	// 
	//  COMPRESS: COMPRESS FILES/FOLDERS
	//
	//  Build deployment zips
	//
	//---------------------------------------------/

		compress: {
			sails: {
				options: {
					archive: '../deployment-zips/<%= pkg.name %>_v<%= pkg.version %>.zip'
				},
				files: [
					{
						expand: true,
						cwd: '../apop-sails/',
						src: ['**', '.ebextensions/*'],
						dest: ''
					}, // makes all src relative to cwd
				]
			}
		},





	////////////////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////////////////
	// // 
	// //	SHELL
	// //
	////////////////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////////////////

	//---------------------------------------------/
	//  TASK
	// 
	//  SHELL: LAUNCH A NEW SHELL (CMD,TERMINAL)
	//
	//  Launch a new shell with command
	//
	//---------------------------------------------/

		shell: {
			sails: {
				command: 'sails lift',
				options: {
					async: true,
						execOptions: {
	                	cwd: '../apop-sails/'
	           		}
				}
			},
			sailsDeploy: {
				command: 'sails lift',
				options: {
					async: false,
						execOptions: {
	                	cwd: '../apop-sails/'
	           		}
				}
			},
			options: {
				stdout: true,
				stderr: true,
				failOnError: true
			}
		},



	////////////////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////////////////
	// // 
	// //	CLI PROMPT GUI CONFIGURATION
	// //
	////////////////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////////////////

	//---------------------------------------------/
	//	CONFIGURE GRUNT PROMPT CLI GUI
	//---------------------------------------------/
	//
	//	Configure 'grunt-prompt' CLI
	//	https://github.com/dylang/grunt-prompt
	//
	//---------------------------------------------/

		// prompt: {
		// 	target: {
		// 		options: {
		// 			questions: [
		// 				{
		// 					config: 'run', // arbitrary name or config for any other grunt task
		// 					type: 'list', // list, checkbox, confirm, input, password
		// 					message: 'Please choose a task to lift the platform.', // Question to ask the user, function needs to return a string,
		// 					default: 'serve:sails', // default value if nothing is entered
		// 					choices: ['serve:angular','serve:sails','deploy:sails','build:deploy:zip','ready:deploy:zip'],
		// 					// validate: function(value), // return true if valid, error message if invalid. works only with type:input 
		// 					// filter:  function(value), // modify the answer
		// 					when: function(answers) // only ask this question when this function returns true
		// 				}
		// 			]
		// 		}
		// 	}
		// }

		// choose: {
		// 	target: {
		// 		options: {
		// 			message: "Please choose a task to serve or deploy the platform.",
		// 			multiple: true
		// 		},
		// 		choices: {
		// 			"serve:angular - for development -- raw Angular source, auto launches localhost Angular app in browser, and watches files with live reloads": 'serve:angular',
		// 			"serve:sails - for development, deploys raw Angular source to Sails app, spins up locahost:1337 Sails app server, watches files & auto reloads": "serve:sails",
		// 			"deploy:sails - for development, deploys raw Angular source to Sails app, compiles Angular source, manifest, and spins up staging/production code to localhost:1337 for testing": "deploy:sails",
		// 			"build:deploy:zip - for staging/production deployment of Sails app, completes deploy:sails and zips to ./deployment-zips": "build:deploy:zip",
		// 			"ready:deploy:zip - only if deploy sails is already completed, zips staging/production Sails app to ./deployment-zips": "ready:deploy:zip"
		// 		}
		// 	}
		// }

	});


////////////////////////////////////////////////////////////////////////////////////////////////////////////
//---------------------------------------------------------------------------------------------------------/
//
//    REGISTER WATCH TASK
//
//---------------------------------------------------------------------------------------------------------/
////////////////////////////////////////////////////////////////////////////////////////////////////////////

	//---------------------------------------------/
	//  TASK
	//---------------------------------------------/
	//  WATCH: WATCHES FILES FOR TASK AUTOMATION
	//---------------------------------------------/
	//
	//  Watches files for changes and runs tasks 
	//  based on the changed files
	//
	//	Applies to server (serve:angular)
	//
	//---------------------------------------------/

	grunt.registerTask('watch:angular', function() {

		// Configuration for watch:server tasks.
		var watchAngularConfig = {

			options: {
				interrupt: true
			},

			// GRUNTFILE (this file)
			// gruntfile: {
			// 	files: ['Gruntfile.js']
			// },

			// SCRIPTS BOWER
			bower: {
				files: [
					'bower.json'
					// 'bower_components/{,*/*/}*.js'
				],
				tasks: ['bower_concat'],
				options: {
					livereload: '<%= connect.options.livereload %>'
				}
			},

			// HTML
			html: {
				files: [
					'<%= platform.app %>/*.html',
					'<%= platform.app %>/{,*/}*.html',
					'<%= platform.app %>/scripts/**/*.html'
				],
				tasks: [],
				options: {
					livereload: '<%= connect.options.livereload %>'
				}
			},

			// SCRIPTS
			js: {
				files: ['<%= platform.app %>/scripts/**/*.js'],
				tasks: [],
				options: {
					livereload: '<%= connect.options.livereload %>'
				}
			},

			// SASS
			compass: {
				files: ['<%= platform.app %>/styles/{,*/*/}*.{scss,sass}'],
				tasks: ['concurrent:compass'],
				options: {
					livereload: '<%= connect.options.livereload %>'
				}
			},

			// CSS
			css: {
				files: ['<%= platform.app %>/styles/{,*/*/}*.{css}'],
				tasks: [],
				options: {
					livereload: '<%= connect.options.livereload %>'
				}
			},

			// JSON (content)
			json: {
				files: ['<%= platform.app %>/content/{,*/}*.json'],
				tasks: [],
				options: {
					livereload: '<%= connect.options.livereload %>'
				}
			},

			// LIVERELOAD
			livereload: {
				options: {
					livereload: '<%= connect.options.livereload %>'
				},
				files: [
					'<%= platform.app %>/{,*/}*.html',
					'<%= platform.app %>/views/**/*.html',
					'<%= platform.app %>/views/**/**/*.html',
					'.tmp/styles/{,*/}*.css',
					'<%= platform.app %>/images/{,*/}*.{png,jpg,jpeg,gif,webp,svg}',
					'<%= platform.app %>/scripts/**/*.@(js|html)',
					'<%= platform.app %>/content/{,*/}*.json'
				]
			}
			
		};

		grunt.config('watch', watchAngularConfig);
		grunt.task.run('watch');

	});


	//---------------------------------------------/
	//  TASK
	//---------------------------------------------/
	//  WATCH: WATCHES FILES FOR TASK AUTOMATION
	//---------------------------------------------/
	//
	//  Watches files for changes and runs tasks 
	//  based on the changed files
	//
	//	Applies to dist (serve:sails)
	//
	//---------------------------------------------/

	grunt.registerTask('watch:sails', function() {

		// Configuration for watch:server tasks.
		var watchSailsConfig = {

			options: {
				interrupt: true
			},

			// GRUNTFILE (this file)
			// gruntfile: {
			// 	files: ['Gruntfile.js']
			// },

			// SCRIPTS BOWER
			bower: {
				files: [
					'bower.json'
					// 'bower_components/{,*/*/}*.js'
				],
				tasks: ['bower_concat', 'copy:appBowerToSails'],
			},

			// HTML
			html: {
				files: [
					'<%= platform.app %>/{,*/}*.html',
					'<%= platform.app %>/views/{,*/}*.html',
					'<%= platform.app %>/scripts/**/*.html'
				],
				tasks: ['copy:appHTMLToSails'],
			},

			// SCRIPTS
			js: {
				files: ['<%= platform.app %>/scripts/**/*.js'],
				tasks: ['copy:appScriptsToSails'],
			},

			// SASS
			compass: {
				files: ['<%= platform.app %>/styles/{,*/*/}*.{scss,sass}'],
				tasks: ['concurrent:compass', 'copy:appStylesToSails'],
			},

			// CSS

			css: {
				files: ['<%= platform.app %>/styles/{,*/*/}*.{css}'],
				tasks: ['copy:appStylesToSails'],
			},

			// JSON (content)
			json: {
				files: ['<%= platform.app %>/content/{,*/}*.json'],
				tasks: ['copy:appJSONContentToSails'],
			},

			
		};

		grunt.config('watch', watchSailsConfig);
		grunt.task.run('watch');

	});

	


////////////////////////////////////////////////////////////////////////////////////////////////////////////
//---------------------------------------------------------------------------------------------------------/
//
//    REGISTER TASK
//
//---------------------------------------------------------------------------------------------------------/
////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	//---------------------------------------------/
	// TASK ERROR HANDLING
	//---------------------------------------------/

	grunt.registerTask('server', 'DEPRECATED TASK. Use the "serve" task instead', function (target) {
		grunt.log.warn('The `server` task has been deprecated. Use `grunt serve` to start a server.');
		// grunt.task.run(['serve:' + target]);
	});


	//---------------------------------------------/
	// DEFAULT
	//---------------------------------------------/

	grunt.registerTask('default', [

		// RUN PROMPT
		'choose'
		
	]);


	//---------------------------------------------/
	// BUILD (no compiling)
	//---------------------------------------------/

	grunt.registerTask('build:app', 'Build from angular, only compile SASS', function () {
		grunt.task.run([

			// CLEAN .TMP & .SASS-CACHE
			'clean:server',
			'clean:sass',

			// INJECT BOWER / CONCAT BOWER
			// 'wiredep',
			'bower_concat',

			// COMPILE SASS
			'concurrent:compass', // run compass:server concurrently in background

			// DEFINE PRELOAD MANIFEST
			'preload_assets:app',
			'replace:appPreloadJSON',

		]);
	});

	//---------------------------------------------/
	// SERVE DEVELOPMENT
	//---------------------------------------------/

	grunt.registerTask('serve:angular', 'Start a connect web server (localhost:9000)', function () {
		grunt.task.run([

			// BUILD LOCAL ANGULAR APP
			'build:app',

			// SPIN LOCALHOST SERVER
			'connect:livereload',

			// WATCH
			'watch:angular'

		]);
	});

	grunt.registerTask('serve:sails', 'Compile to & then start a connect web server (localhost:9000)', function () {
		grunt.task.run([


			// BUILD LOCAL ANGULAR APP
			'build:app',

			// CLEAN & COPY APP TO SAILS
			'clean:sailsAssets',
			'clean:sailsContent',
			'copy:appToSails',
			'copy:appContentToSails',
			
			// START SAILS
			'shell:sails',

			// WATCH
			'watch:sails'

		]);
	});

	//---------------------------------------------/
	// BUILD / COMPILE (PRODUCTION)
	//---------------------------------------------/

	grunt.registerTask('build:dist', 'Build from angular, only compile SASS, concatenate, minifiy, and uglify JS and replace script tags, ', function () {
		grunt.task.run([

			// BUILD ANGULAR LOCALLY
			'build:app',

			// CLEAN DIST
			'clean:dist',

			// BUILD DIST (copy)
			'copy:appToDist',

			// MIN IMAGES (png,jpg,jpeg,gif & svg)
			'imagemin',
			'svgmin',

			// CONCATENTATE, MINIFY & UGLIFY SCRIPT INJECTIONS (index.html), MINIFY CSS
			'useminPrepare',
			'concat:generated',
			'cssmin:generated',
			'ngAnnotate:dist',
			'uglify:generated',
			// 'filerev', // NOT USED
			'usemin',

			// Replace Version Number
			'replace:versionNumber',

			// Replace: Platform Variable Injection
			'replace:platformVariables',
			
			// Replace Version Number Cache Files
			'replace:cacheBustedFiles',

			// Prepare index-offline manifest fallback
			'replace:distIndexOffline',

			// Rename Files Cache Bust
			'rename:cacheBusting',

			// DEFINE PRELOAD MANIFEST
			'preload_assets:dist',
			'replace:distPreloadJSON',
			'rename:cacheBustingPreloadJSON',

			// BUILD DIST MANIFEST using config.prod.manifest
			'manifest',
			'replace:manifest' //remove %20 in fallback

		]);
	});

	//---------------------------------------------/
	// SERVE STAGING/PRODUCTION DEPLOY
	//---------------------------------------------/

	grunt.registerTask('deploy:sails', 'Compile to & then start a connect web server (localhost:9000)', function () {
		grunt.task.run([


			// BUILD LOCAL ANGULAR APP
			'build:dist',

			// CLEAN & COPY DIST TO SAILS, scrub SASS
			'clean:sailsAssets',
			'clean:sailsContent',
			'copy:distToSails',
			'copy:distContentToSails',
			
			// START SAILS
			'shell:sailsDeploy'

		]);
	});

	//---------------------------------------------/
	// BUILD STAGING/PRODUCTION DEPLOY ZIP
	//---------------------------------------------/

	grunt.registerTask('build:deploy:zip', 'Compile to deployment ZIP', function () {
		grunt.task.run([


			// BUILD LOCAL ANGULAR APP
			'build:dist',

			// CLEAN & COPY DIST TO SAILS, scrub SASS
			'clean:sailsAssets',
			'clean:sailsContent',
			'copy:distToSails',
			'copy:distContentToSails',
			
			// BUILD SAILS ZIP
			'compress:sails'

		]);
	});

	grunt.registerTask('ready:deploy:zip', 'Compile to deployment ZIP', function () {
		grunt.task.run([
			
			// BUILD SAILS ZIP
			'compress:sails'

		]);
	});


};
